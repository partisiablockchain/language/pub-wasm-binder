# Pub wasm binder

Binder for public WASM smart contracts.

This binder implements the needed logic for deploying WASM-based smart contracts to Partisia
Blockchain. WASM-based contracts are supported by invoking the `WasmInvoker` on the associated WASM
code.

It is assumed that the contract was written in Rust using the
[Partisia Blockchain Contract SDK](https://gitlab.com/partisiablockchain/language/contract-sdk/), and
compiled to WASM using the
[Cargo Partisia Contract CLI](https://gitlab.com/partisiablockchain/language/cargo-partisia-contract) tool.

This binder does not support REAL/ZK contract feature set.

## Further links

- [Javadoc Documentation for `main`](https://partisiablockchain.gitlab.io/language/pub-wasm-binder)
