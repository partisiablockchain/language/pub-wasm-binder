package com.partisiablockchain.binder.pubwasm;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CheckReturnValue;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import com.partisiablockchain.language.wasminvoker.Configuration;
import com.partisiablockchain.language.wasminvoker.SemVer;
import com.partisiablockchain.language.wasminvoker.WasmInvoker;
import com.partisiablockchain.language.wasminvoker.WasmInvokerImpl;

/**
 * Binder for public WASM smart contracts.
 *
 * <p>This binder implements the needed logic for deploying WASM-based smart contracts to Partisia
 * Blockchain. WASM-based contracts are supported by invoking the {@link WasmInvoker} on the
 * associated WASM code.
 *
 * <p>It is assumed that the contract was written in Rust using the <a
 * href="https://gitlab.com/partisiablockchain/language/contract-sdk/">Partisia Blockchain Contract
 * SDK</a>, and compiled to WASM using the <a
 * href="https://gitlab.com/partisiablockchain/language/cargo-partisia-contract">Cargo Partisia
 * Contract CLI</a> tool.
 *
 * <p>This binder does not support REAL/ZK contract feature set.
 */
@CheckReturnValue
public final class PublicWasmContractBinder extends AbstractPublicWasmContractBinder {

  private static final SemVer SUPPORTED_BINDER_VERSION = new SemVer(10, 4, 0);
  private static final Configuration SUPPORTED_CONFIGURATION = new Configuration(true, true);

  /**
   * Default constructor.
   *
   * @param wasmBytes the wasm code to load to binder.
   */
  public PublicWasmContractBinder(byte[] wasmBytes) {
    this(parseWasmInvokerFromBytes(wasmBytes));
  }

  PublicWasmContractBinder(WasmInvoker invoker) {
    super(invoker);
  }

  /**
   * Parses wasm bytes to a {@link WasmInvoker}.
   *
   * @param wasmBytes Bytes to parse.
   * @return Newly parsed {@link WasmInvoker}.
   */
  private static WasmInvoker parseWasmInvokerFromBytes(byte[] wasmBytes) {
    // Parse
    final WasmParser parser = new WasmParser(wasmBytes);
    final WasmModule wasmModule = parser.parse();

    // Find binder version from the WASM module
    final SemVer parsedVersion = Configuration.determineBinderVersion(wasmModule);

    if (!isSupported(parsedVersion)) {
      throw new RuntimeException(
          String.format(
              "Unsupported binder ABI version %s in WASM. Supported version is %s",
              parsedVersion, SUPPORTED_BINDER_VERSION));
    }

    // Produce invoker
    return WasmInvokerImpl.fromModule(wasmModule, SUPPORTED_CONFIGURATION);
  }

  static boolean isSupported(SemVer parsedVersion) {
    return parsedVersion.major() == SUPPORTED_BINDER_VERSION.major()
        && parsedVersion.minor() == SUPPORTED_BINDER_VERSION.minor();
  }
}
