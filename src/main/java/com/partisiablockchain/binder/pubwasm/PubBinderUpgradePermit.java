package com.partisiablockchain.binder.pubwasm;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

/**
 * Upgrade permit for the {@link PublicWasmContractBinder}.
 *
 * <p>The upgrade permit for public WASM contracts are largely defined by the WASM contract itself,
 * and thus the Rust SDK.
 *
 * <p>The permit also maintains the amount of gas that can be used by the state upgrade logic.
 *
 * @param contractPermitData The WASM contract's self-defined permit data. Not nullable.
 * @param gasRemainingForUpgrade Amount of gas that can be used by the state upgrade logic.
 */
@SuppressWarnings("ArrayRecordComponent")
public record PubBinderUpgradePermit(byte[] contractPermitData, long gasRemainingForUpgrade)
    implements DataStreamSerializable {

  /**
   * Construct new {@link PubBinderUpgradePermit}.
   *
   * @param contractPermitData The WASM contract's self-defined permit data. Not nullable.
   * @param gasRemainingForUpgrade Amount of gas that can be used by the state upgrade logic.
   */
  public PubBinderUpgradePermit {
    Objects.requireNonNull(contractPermitData);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeDynamicBytes(contractPermitData);
    stream.writeLong(gasRemainingForUpgrade);
  }

  /**
   * Read new {@link PubBinderUpgradePermit} from stream.
   *
   * @param stream Stream to read from.
   * @return Newly deserialized {@link PubBinderUpgradePermit}.
   */
  public static PubBinderUpgradePermit read(SafeDataInputStream stream) {
    final byte[] contractPermitData = stream.readDynamicBytes();
    final long gasRemainingForUpgrade = stream.readLong();
    return new PubBinderUpgradePermit(contractPermitData, gasRemainingForUpgrade);
  }
}
