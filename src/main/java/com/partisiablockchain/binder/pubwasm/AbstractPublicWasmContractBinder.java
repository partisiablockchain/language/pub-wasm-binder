package com.partisiablockchain.binder.pubwasm;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CheckReturnValue;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.ContractIdentifiers;
import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.binder.pub.PubBinderContext;
import com.partisiablockchain.binder.pub.PubBinderContract;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.language.wasminvoker.Leb128Header;
import com.partisiablockchain.language.wasminvoker.WasmInvoker;
import com.partisiablockchain.language.wasminvoker.WasmState;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTreeWrapper;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTrees;
import com.partisiablockchain.language.wasminvoker.avltrees.ComparableByteArray;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Arrays;
import java.util.HexFormat;
import java.util.Locale;
import java.util.Objects;
import org.bouncycastle.util.encoders.Hex;

/**
 * Abstract binder for public WASM smart contracts.
 *
 * @see PublicWasmContractBinder for more documentation.
 */
@CheckReturnValue
public abstract class AbstractPublicWasmContractBinder implements PubBinderContract<WasmState> {

  /** This is 0xFFFFFFFF encoded in LEB128: 0xFFFFFFFFF0. */
  public static final byte[] INIT_HEADER = HexFormat.ofDelimiter(":").parseHex("FF:FF:FF:FF:0F");

  /**
   * The section ID for result section storing whether an upgrade is allowed.
   *
   * <p>Layout: {@code BOOL} (Byte value where {@code 0} indicates {@code false}, and anything else
   * indicating {@code true}.)
   */
  public static final byte RETURN_SECTION_UPGRADE_ALLOWED = 0x20;

  /**
   * The section ID for result section storing the type witness of the contract state.
   *
   * <p>Layout is undefined. Contract can return whatever it wants, and it will be passed directly
   * into the upgraded contract.
   */
  public static final byte RETURN_SECTION_STATE_TYPE_WITNESS = 0x21;

  /** Shortname used to ask the contract for whether the upgrade is allowed. */
  public static final String FUNCTION_NAME_UPGRADE_IS_ALLOWED = "upgrade_is_allowed";

  /** Shortname used to perform upgrade of the state. */
  public static final String FUNCTION_NAME_UPGRADE_STATE = "upgrade";

  /** The WASM invoker. */
  protected final WasmInvoker invoker;

  /**
   * Default constructor.
   *
   * @param invoker executes wasm interactions
   */
  protected AbstractPublicWasmContractBinder(WasmInvoker invoker) {
    this.invoker = Objects.requireNonNull(invoker);
  }

  @Override
  public Class<WasmState> getStateClass() {
    return WasmState.class;
  }

  @Override
  public BinderResult<WasmState, BinderInteraction> create(PubBinderContext context, byte[] rpc) {
    if (rpc.length < 5) {
      throw new RuntimeException("Contract initialization bytes should start with 0xFFFFFFFF0F");
    }

    byte[] headerBytes = getHeaderBytes(rpc);
    if (!Arrays.equals(headerBytes, INIT_HEADER)) {
      throw new RuntimeException("Contract initialization bytes should start with 0xFFFFFFFF0F");
    }

    byte[] input = excludeHeaderBytes(rpc);
    WasmInvoker.WasmResult wasmResult =
        invoker.executeWasm(
            context.availableGas(), "init", context, null, new AvlTrees(AvlTree.create()), input);

    context.registerCpuFee(wasmResult.usedWasmGas());
    return createResult(wasmResult, context.availableGas());
  }

  @Override
  public BinderResult<WasmState, BinderInteraction> invoke(
      PubBinderContext context, WasmState state, byte[] rpc) {
    boolean topUpGas = rpc.length == 0;
    if (topUpGas) {
      return EventResult.create().toBinderResult(state, context.availableGas());
    } else {
      String name = Hex.toHexString(getHeaderBytes(rpc)).toLowerCase(Locale.ENGLISH);

      byte[] input = excludeHeaderBytes(rpc);
      WasmInvoker.WasmResult wasmResult =
          invoker.executeWasm(
              context.availableGas(),
              "action_" + name,
              context,
              null,
              new AvlTrees(state.getAvlTrees()),
              state.getState(),
              input);

      context.registerCpuFee(wasmResult.usedWasmGas());
      return createResult(wasmResult, context.availableGas());
    }
  }

  private byte[] getHeaderBytes(byte[] rpc) {
    var headerLength = Leb128Header.readHeaderLength(rpc);

    byte[] headerBytes = new byte[headerLength];
    System.arraycopy(rpc, 0, headerBytes, 0, headerBytes.length);
    return headerBytes;
  }

  private byte[] excludeHeaderBytes(byte[] rpc) {
    var headerLength = Leb128Header.readHeaderLength(rpc);

    byte[] input = new byte[rpc.length - headerLength];
    System.arraycopy(rpc, headerLength, input, 0, input.length);
    return input;
  }

  private byte[] prefixWithLength(byte[] rpc) {
    return SafeDataOutputStream.serialize(s -> s.writeDynamicBytes(rpc));
  }

  @Override
  public BinderResult<WasmState, BinderInteraction> callback(
      PubBinderContext context, WasmState state, CallbackContext callbackContext, byte[] rpc) {
    String name = Hex.toHexString(getHeaderBytes(rpc)).toLowerCase(Locale.ENGLISH);

    byte[] input = excludeHeaderBytes(rpc);
    WasmInvoker.WasmResult wasmResult =
        invoker.executeWasm(
            context.availableGas(),
            "callback_" + name,
            context,
            callbackContext,
            new AvlTrees(state.getAvlTrees()),
            state.getState(),
            input);

    context.registerCpuFee(wasmResult.usedWasmGas());
    return createResult(wasmResult, context.availableGas());
  }

  private BinderResult<WasmState, BinderInteraction> createResult(
      WasmInvoker.WasmResult wasmResult, long availableGas) {
    return wasmResult
        .eventResult()
        .toBinderResult(wasmResult.state(), availableGas - wasmResult.usedWasmGas());
  }

  @Override
  public ContractUpgradePermit acquireUpgradePermit(
      PubBinderContext context,
      WasmState state,
      ContractIdentifiers oldContractBytecodeIds,
      ContractIdentifiers newContractBytecodeIds,
      byte[] rpc) {
    if (!invoker.doesFunctionExist(FUNCTION_NAME_UPGRADE_IS_ALLOWED)) {
      throw new UnsupportedOperationException(
          "Contract does not implement \"%s\", and defaults to not upgradable"
              .formatted(FUNCTION_NAME_UPGRADE_IS_ALLOWED));
    }

    final long initialGas = context.availableGas();

    final WasmInvoker.WasmResult wasmResult =
        invoker.executeWasm(
            initialGas,
            FUNCTION_NAME_UPGRADE_IS_ALLOWED,
            context,
            null,
            new AvlTrees(state.getAvlTrees()),
            state.getState(),
            oldContractBytecodeIds.contractHash().getBytes(),
            oldContractBytecodeIds.binderHash().getBytes(),
            oldContractBytecodeIds.abiHash().getBytes(),
            newContractBytecodeIds.contractHash().getBytes(),
            newContractBytecodeIds.binderHash().getBytes(),
            newContractBytecodeIds.abiHash().getBytes(),
            prefixWithLength(rpc));

    context.registerCpuFee(wasmResult.usedWasmGas());
    final long gasRemainingForUpgrade = initialGas - wasmResult.usedWasmGas();

    final boolean didContractAllowThisUpgrade = didContractAllowThisUpgrade(wasmResult);

    if (didContractAllowThisUpgrade) {
      final byte[] wasmPermitData =
          wasmResult.getSection(RETURN_SECTION_STATE_TYPE_WITNESS).sectionData();
      final PubBinderUpgradePermit binderUpgradePermit =
          new PubBinderUpgradePermit(wasmPermitData, gasRemainingForUpgrade);
      return new ContractUpgradePermit(SafeDataOutputStream.serialize(binderUpgradePermit));
    } else {
      throw new RuntimeException("Contract did not allow this upgrade");
    }
  }

  @Override
  public WasmState upgradeState(
      PubBinderContext context, StateAccessor oldState, ContractUpgradePermit permit, byte[] rpc) {
    if (!invoker.doesFunctionExist(FUNCTION_NAME_UPGRADE_STATE)) {
      throw new UnsupportedOperationException(
          "Contract does not implement \"%s\", and can thus not be upgraded to"
              .formatted(FUNCTION_NAME_UPGRADE_STATE));
    }

    final PubBinderUpgradePermit binderUpgradePermit =
        SafeDataInputStream.readFully(permit.oldContractData(), PubBinderUpgradePermit::read);

    byte[] oldStateBytes = oldState.get("state").cast(LargeByteArray.class).getData();
    final AvlTrees avlTrees = new AvlTrees(convertAvlTreesToNewContract(oldState.get("avlTrees")));

    WasmInvoker.WasmResult wasmResult =
        invoker.executeWasm(
            binderUpgradePermit.gasRemainingForUpgrade(),
            FUNCTION_NAME_UPGRADE_STATE,
            context,
            null,
            avlTrees,
            binderUpgradePermit.contractPermitData(),
            oldStateBytes,
            rpc);
    context.registerCpuFee(wasmResult.usedWasmGas());

    return wasmResult.state();
  }

  /**
   * Check whether contract allowed an upgrade operation.
   *
   * <p>Contract is treated as allowing the upgrade if {@link RETURN_SECTION_UPGRADE_ALLOWED}
   * contains a single non-zero byte.
   *
   * @param wasmResult Result to read sections from.
   * @return Whether or not the contract allowed the upgrade.
   */
  private static boolean didContractAllowThisUpgrade(WasmInvoker.WasmResult wasmResult) {
    final byte[] resultSection =
        wasmResult.getSection(RETURN_SECTION_UPGRADE_ALLOWED).sectionData();
    return didContractAllowThisUpgrade(resultSection);
  }

  static boolean didContractAllowThisUpgrade(final byte[] resultSection) {
    return resultSection != null && resultSection.length == 1 && resultSection[0] != (byte) 0;
  }

  /**
   * Converts old {@link AvlTree}s to the upgraded state {@link AvlTree}s.
   *
   * @param avlTreesMap Untyped reference to an {@code AvlTree<Integer, AvlTreeWrapper>}.
   * @return Converted {@link AvlTree}s.
   */
  private static AvlTree<Integer, AvlTreeWrapper> convertAvlTreesToNewContract(
      StateAccessor avlTreesMap) {
    AvlTree<Integer, AvlTreeWrapper> avlTrees = AvlTree.create();
    for (final var avlTreeAccessor : avlTreesMap.getTreeLeaves()) {
      AvlTree<ComparableByteArray, LargeByteArray> avlTree = AvlTree.create();

      for (final var leafNode : avlTreeAccessor.getValue().get("avlTree").getTreeLeaves()) {
        final LargeByteArray keyLba = leafNode.getKey().get("data").cast(LargeByteArray.class);
        final var key = new ComparableByteArray(keyLba.getData());
        avlTree = avlTree.set(key, leafNode.getValue().cast(LargeByteArray.class));
      }

      avlTrees = avlTrees.set(avlTreeAccessor.getKey().intValue(), new AvlTreeWrapper(avlTree));
    }

    return avlTrees;
  }
}
