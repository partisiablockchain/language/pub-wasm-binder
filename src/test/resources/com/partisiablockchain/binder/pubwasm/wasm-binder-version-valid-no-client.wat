(module
  (type (;0;) (func (result i64)))
  (data (;0;) (i32.const 0) "\01\00\00\00\04WASM")
  (func $noresult (type 0) (result i64)
    i64.const 9
    i64.const 32
    i64.shl
  )
  (memory 1)
  (export "noresult" (func $noresult))
  (export "__PBC_VERSION_BINDER_5_0_55" (func $noresult))
)
