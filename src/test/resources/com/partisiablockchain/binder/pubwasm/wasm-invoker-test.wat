(module
  (type (;0;) (func (param i32 i32) (result i64)))
  (type (;1;) (func (param i32 i32)))
  (global $stroffset i32 (i32.const 0x40))
  (data (;0;) (i32.const 0x40) "Hello World WASM") ;; NOTE: Offset must be equal to $stroffset
  (func $init (type 0) (param i32 i32) (result i64) (local $offset i32)
    ;; Define $offset
    global.get $stroffset
    i32.const 0x15
    i32.sub
    local.tee $offset

    i32.const 0x01  ;; State id: state
    i32.store8 offset=0x00

    local.get $offset
    i32.const 0x20000000  ;; State section length
    i32.store offset=0x01

    ;; Write the arguments in mem starting at address 42 + 4
    local.get $offset
    local.get 0
    i32.store offset=0x05

    local.get $offset
    local.get 0
    i32.load offset=0x00
    i32.store offset=0x09

    local.get $offset
    local.get 0
    i32.load offset=0x04
    i32.store offset=0x0d

    local.get $offset
    local.get 0
    i32.load offset=0x08
    i32.store offset=0x11

    ;; Shift length to the leftmost 32bits
    local.get $offset
    i32.const 4
    i32.sub
    i32.const 37 ;; Length
    i32.store8 offset=3

    global.get  $stroffset
    i32.const 25
    i32.sub
    i64.extend_i32_u
  )
  (func $action (type 0) (param i32 i32) (result i64) (local $offset i32)
    ;; Define $offset
    global.get $stroffset
    i32.const 0x1a
    i32.sub
    i32.const 4
    i32.add
    local.set $offset

    ;; Write events section
    local.get $offset
    local.get $offset
    local.get $offset

    i32.const 0x02  ;; Section id: events
    i32.store8 offset=0x00
    i32.const 0x04000000  ;; Section size: |i32|
    i32.store offset=0x01
    i32.const 0  ;; Num events
    i32.store offset=0x05

    ;; Write state section:
    local.get $offset
    local.get $offset

    i32.const 0x01  ;; State id: state
    i32.store8 offset=0x09
    i32.const 0x1c000000  ;; Section size
    i32.store offset=0x0a

    ;; Write state data

    local.get $offset
    local.get $offset
    local.get $offset
    local.get $offset
    local.get $offset
    local.get $offset

    local.get 0
    i32.store offset=0x0e
    local.get 0
    i32.load offset=0x00
    i32.store offset=0x12
    local.get 0
    i32.load offset=0x04
    i32.store offset=0x16
    local.get 0
    i32.load offset=0x08
    i32.store offset=0x1a
    local.get 0
    i32.load offset=0x0c
    i32.store offset=0x1e
    local.get 0
    i32.load offset=0x10
    i32.store offset=0x22

    ;; Calculate base pos
    local.get $offset
    i32.const 4
    i32.sub

    i32.const 0x2a ;; Length
    i32.store8 offset=3 ;; Fake big endian length

    local.get $offset
    i32.const 4
    i32.sub
    i64.extend_i32_u
  )
  (func $noresult (type 1) (param i32 i32)
    nop
  )
  (memory 1)
  (export "init" (func $init))
  (export "action_a125a953" (func $action))
  (export "noresult" (func $noresult))
  (export "__PBC_VERSION_BINDER_10_4_0" (func $noresult))
  (export "__PBC_VERSION_CLIENT_7_3_3" (func $noresult))
)
