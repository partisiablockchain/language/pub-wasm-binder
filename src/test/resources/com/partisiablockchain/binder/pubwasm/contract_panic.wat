(module
  (type (;0;) (func (param i32)))
  (type (;1;) (func (param i32) (result i64)))
  (type (;2;) (func (param i32 i32) (result i32)))
  (type (;3;) (func (param i32 i32)))
  (type (;4;) (func (param i32 i32 i32) (result i32)))
  (type (;5;) (func (param i32 i32 i32 i32) (result i64)))
  (type (;6;) (func (param i32 i32 i32 i32 i32 i32) (result i64)))
  (type (;7;) (func))
  (type (;8;) (func (param i32 i32 i32)))
  (type (;9;) (func (param i32 i32 i32 i32) (result i32)))
  (type (;10;) (func (param i32 i32 i32 i32)))
  (type (;11;) (func (param i32 i32) (result i64)))
  (type (;12;) (func (result i32)))
  (type (;13;) (func (param i32) (result i32)))
  (type (;14;) (func (param i32 i32 i32 i32 i32) (result i32)))
  (type (;15;) (func (param i32 i32 i32 i32 i32)))
  (type (;16;) (func (param i32 i32 i32 i32 i32 i32) (result i32)))
  (type (;17;) (func (param i32 i32 i32) (result i64)))
  (type (;18;) (func (param i64 i32 i32) (result i32)))
  (import "pbc" "memmove" (func $_ZN7pbc_lib3mem11pbc_memmove17h982abf5a0b50774dE (type 4)))
  (import "pbc" "exit" (func $_ZN7pbc_lib4exit8pbc_exit17hc5a8a9b02031e4f6E (type 3)))
  (func $_ZN105_$LT$pbc_contract_common..context..ContractContext$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$13rpc_read_from17hbba672d661e470f7E (type 3) (param i32 i32)
    (local i32 i64 i32 i64 i32 i64 i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 0
    local.get 1
    call $_ZN97_$LT$pbc_contract_common..address..Address$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$13rpc_read_from17hde2ab6e18096e28eE
    local.get 0
    i32.const 21
    i32.add
    local.get 1
    call $_ZN97_$LT$pbc_contract_common..address..Address$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$13rpc_read_from17hde2ab6e18096e28eE
    i32.const 1048812
    i64.extend_i32_u
    i64.const 32
    i64.shl
    local.set 3
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.load offset=4
        local.tee 4
        i32.const 8
        i32.ge_u
        br_if 0 (;@2;)
        local.get 3
        i64.const 9474
        i64.or
        local.set 5
        i64.const 0
        local.set 3
        br 1 (;@1;)
      end
      local.get 1
      local.get 4
      i32.const -8
      i32.add
      local.tee 4
      i32.store offset=4
      local.get 1
      local.get 1
      i32.load
      local.tee 6
      i32.const 8
      i32.add
      i32.store
      local.get 3
      i64.const 9476
      i64.or
      local.set 5
      local.get 6
      i64.load align=1
      local.set 3
    end
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 5
            i32.wrap_i64
            local.tee 6
            i32.const 255
            i32.and
            i32.const 4
            i32.ne
            br_if 0 (;@4;)
            i32.const 1048812
            i64.extend_i32_u
            i64.const 32
            i64.shl
            local.set 5
            block  ;; label = @5
              block  ;; label = @6
                local.get 4
                i32.const 8
                i32.ge_u
                br_if 0 (;@6;)
                local.get 5
                i64.const 9474
                i64.or
                local.set 7
                i64.const 0
                local.set 5
                br 1 (;@5;)
              end
              local.get 1
              local.get 4
              i32.const -8
              i32.add
              local.tee 4
              i32.store offset=4
              local.get 1
              local.get 1
              i32.load
              local.tee 6
              i32.const 8
              i32.add
              i32.store
              local.get 5
              i64.const 9476
              i64.or
              local.set 7
              local.get 6
              i64.load align=1
              local.set 5
            end
            local.get 7
            i32.wrap_i64
            local.tee 6
            i32.const 255
            i32.and
            i32.const 4
            i32.ne
            br_if 1 (;@3;)
            local.get 0
            i64.const 0
            i64.store offset=64
            local.get 0
            i32.const 88
            i32.add
            i64.const 0
            i64.store
            local.get 0
            i32.const 80
            i32.add
            i64.const 0
            i64.store
            local.get 0
            i32.const 72
            i32.add
            i64.const 0
            i64.store
            i32.const 1049264
            i64.extend_i32_u
            i64.const 32
            i64.shl
            local.set 7
            block  ;; label = @5
              block  ;; label = @6
                local.get 4
                i32.const 32
                i32.ge_u
                br_if 0 (;@6;)
                local.get 7
                i64.const 9474
                i64.or
                local.set 7
                br 1 (;@5;)
              end
              local.get 1
              local.get 4
              i32.const -32
              i32.add
              local.tee 4
              i32.store offset=4
              local.get 0
              i32.const 64
              i32.add
              local.tee 8
              local.get 1
              i32.load
              local.tee 6
              i64.load align=1
              i64.store align=1
              local.get 1
              local.get 6
              i32.const 32
              i32.add
              i32.store
              local.get 8
              i32.const 8
              i32.add
              local.get 6
              i32.const 8
              i32.add
              i64.load align=1
              i64.store align=1
              local.get 8
              i32.const 16
              i32.add
              local.get 6
              i32.const 16
              i32.add
              i64.load align=1
              i64.store align=1
              local.get 8
              i32.const 24
              i32.add
              local.get 6
              i32.const 24
              i32.add
              i64.load align=1
              i64.store align=1
              local.get 7
              i64.const 9476
              i64.or
              local.set 7
            end
            local.get 7
            i32.wrap_i64
            local.tee 6
            i32.const 255
            i32.and
            i32.const 4
            i32.ne
            br_if 2 (;@2;)
            local.get 0
            i64.const 0
            i64.store offset=96
            local.get 0
            i32.const 120
            i32.add
            i64.const 0
            i64.store
            local.get 0
            i32.const 112
            i32.add
            i64.const 0
            i64.store
            local.get 0
            i32.const 104
            i32.add
            i64.const 0
            i64.store
            i32.const 1049264
            i64.extend_i32_u
            i64.const 32
            i64.shl
            local.set 7
            block  ;; label = @5
              block  ;; label = @6
                local.get 4
                i32.const 32
                i32.ge_u
                br_if 0 (;@6;)
                local.get 7
                i64.const 9474
                i64.or
                local.set 7
                br 1 (;@5;)
              end
              local.get 1
              local.get 4
              i32.const -32
              i32.add
              i32.store offset=4
              local.get 0
              i32.const 96
              i32.add
              local.tee 6
              local.get 1
              i32.load
              local.tee 4
              i64.load align=1
              i64.store align=1
              local.get 1
              local.get 4
              i32.const 32
              i32.add
              i32.store
              local.get 6
              i32.const 8
              i32.add
              local.get 4
              i32.const 8
              i32.add
              i64.load align=1
              i64.store align=1
              local.get 6
              i32.const 16
              i32.add
              local.get 4
              i32.const 16
              i32.add
              i64.load align=1
              i64.store align=1
              local.get 6
              i32.const 24
              i32.add
              local.get 4
              i32.const 24
              i32.add
              i64.load align=1
              i64.store align=1
              local.get 7
              i64.const 9476
              i64.or
              local.set 7
            end
            local.get 7
            i32.wrap_i64
            local.tee 1
            i32.const 255
            i32.and
            i32.const 4
            i32.ne
            br_if 3 (;@1;)
            local.get 0
            local.get 5
            i64.const 56
            i64.shl
            local.get 5
            i64.const 40
            i64.shl
            i64.const 71776119061217280
            i64.and
            i64.or
            local.get 5
            i64.const 24
            i64.shl
            i64.const 280375465082880
            i64.and
            local.get 5
            i64.const 8
            i64.shl
            i64.const 1095216660480
            i64.and
            i64.or
            i64.or
            local.get 5
            i64.const 8
            i64.shr_u
            i64.const 4278190080
            i64.and
            local.get 5
            i64.const 24
            i64.shr_u
            i64.const 16711680
            i64.and
            i64.or
            local.get 5
            i64.const 40
            i64.shr_u
            i64.const 65280
            i64.and
            local.get 5
            i64.const 56
            i64.shr_u
            i64.or
            i64.or
            i64.or
            i64.store offset=56
            local.get 0
            local.get 3
            i64.const 56
            i64.shl
            local.get 3
            i64.const 40
            i64.shl
            i64.const 71776119061217280
            i64.and
            i64.or
            local.get 3
            i64.const 24
            i64.shl
            i64.const 280375465082880
            i64.and
            local.get 3
            i64.const 8
            i64.shl
            i64.const 1095216660480
            i64.and
            i64.or
            i64.or
            local.get 3
            i64.const 8
            i64.shr_u
            i64.const 4278190080
            i64.and
            local.get 3
            i64.const 24
            i64.shr_u
            i64.const 16711680
            i64.and
            i64.or
            local.get 3
            i64.const 40
            i64.shr_u
            i64.const 65280
            i64.and
            local.get 3
            i64.const 56
            i64.shr_u
            i64.or
            i64.or
            i64.or
            i64.store offset=48
            local.get 2
            i32.const 16
            i32.add
            global.set $__stack_pointer
            return
          end
          local.get 2
          local.get 6
          i32.store8 offset=8
          local.get 2
          local.get 5
          i64.const 56
          i64.shr_u
          i64.store8 offset=15
          local.get 2
          local.get 5
          i64.const 40
          i64.shr_u
          i64.store16 offset=13 align=1
          local.get 2
          local.get 5
          i64.const 8
          i64.shr_u
          i64.store32 offset=9 align=1
          i32.const 1048820
          i32.const 43
          local.get 2
          i32.const 8
          i32.add
          i32.const 1048864
          i32.const 1049016
          call $_ZN4core6result13unwrap_failed17h6d050dbd00b66340E
          unreachable
        end
        local.get 2
        local.get 6
        i32.store8 offset=8
        local.get 2
        local.get 7
        i64.const 56
        i64.shr_u
        i64.store8 offset=15
        local.get 2
        local.get 7
        i64.const 40
        i64.shr_u
        i64.store16 offset=13 align=1
        local.get 2
        local.get 7
        i64.const 8
        i64.shr_u
        i64.store32 offset=9 align=1
        i32.const 1048820
        i32.const 43
        local.get 2
        i32.const 8
        i32.add
        i32.const 1048864
        i32.const 1049016
        call $_ZN4core6result13unwrap_failed17h6d050dbd00b66340E
        unreachable
      end
      local.get 2
      local.get 6
      i32.store8 offset=8
      local.get 2
      local.get 7
      i64.const 56
      i64.shr_u
      i64.store8 offset=15
      local.get 2
      local.get 7
      i64.const 40
      i64.shr_u
      i64.store16 offset=13 align=1
      local.get 2
      local.get 7
      i64.const 8
      i64.shr_u
      i64.store32 offset=9 align=1
      i32.const 1049272
      i32.const 43
      local.get 2
      i32.const 8
      i32.add
      i32.const 1049316
      i32.const 1049472
      call $_ZN4core6result13unwrap_failed17h6d050dbd00b66340E
      unreachable
    end
    local.get 2
    local.get 1
    i32.store8 offset=8
    local.get 2
    local.get 7
    i64.const 56
    i64.shr_u
    i64.store8 offset=15
    local.get 2
    local.get 7
    i64.const 40
    i64.shr_u
    i64.store16 offset=13 align=1
    local.get 2
    local.get 7
    i64.const 8
    i64.shr_u
    i64.store32 offset=9 align=1
    i32.const 1049272
    i32.const 43
    local.get 2
    i32.const 8
    i32.add
    i32.const 1049316
    i32.const 1049472
    call $_ZN4core6result13unwrap_failed17h6d050dbd00b66340E
    unreachable)
  (func $_ZN97_$LT$pbc_contract_common..address..Address$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$13rpc_read_from17hde2ab6e18096e28eE (type 3) (param i32 i32)
    (local i32 i64 i32 i32 i32)
    global.get $__stack_pointer
    i32.const 48
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    i32.const 1048812
    i64.extend_i32_u
    i64.const 32
    i64.shl
    local.set 3
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.load offset=4
        local.tee 4
        br_if 0 (;@2;)
        local.get 3
        i64.const 9474
        i64.or
        local.set 3
        i32.const 0
        local.set 5
        i32.const 0
        local.set 4
        br 1 (;@1;)
      end
      local.get 1
      local.get 4
      i32.const -1
      i32.add
      local.tee 5
      i32.store offset=4
      local.get 1
      local.get 1
      i32.load
      local.tee 4
      i32.const 1
      i32.add
      i32.store
      local.get 3
      i64.const 9476
      i64.or
      local.set 3
      local.get 4
      i32.load8_u
      local.set 4
    end
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 3
          i32.wrap_i64
          local.tee 6
          i32.const 255
          i32.and
          i32.const 4
          i32.ne
          br_if 0 (;@3;)
          local.get 4
          i32.const 255
          i32.and
          i32.const 4
          i32.ge_u
          br_if 1 (;@2;)
          local.get 0
          i64.const 0
          i64.store offset=1 align=1
          local.get 0
          i32.const 17
          i32.add
          i32.const 0
          i32.store align=1
          local.get 0
          i32.const 9
          i32.add
          i64.const 0
          i64.store align=1
          i32.const 1049264
          i64.extend_i32_u
          i64.const 32
          i64.shl
          local.set 3
          block  ;; label = @4
            block  ;; label = @5
              local.get 5
              i32.const 20
              i32.ge_u
              br_if 0 (;@5;)
              local.get 3
              i64.const 9474
              i64.or
              local.set 3
              br 1 (;@4;)
            end
            local.get 1
            local.get 5
            i32.const -20
            i32.add
            i32.store offset=4
            local.get 0
            i32.const 1
            i32.add
            local.tee 6
            local.get 1
            i32.load
            local.tee 5
            i64.load align=1
            i64.store align=1
            local.get 1
            local.get 5
            i32.const 20
            i32.add
            i32.store
            local.get 6
            i32.const 8
            i32.add
            local.get 5
            i32.const 8
            i32.add
            i64.load align=1
            i64.store align=1
            local.get 6
            i32.const 16
            i32.add
            local.get 5
            i32.const 16
            i32.add
            i32.load align=1
            i32.store align=1
            local.get 3
            i64.const 9476
            i64.or
            local.set 3
          end
          local.get 3
          i32.wrap_i64
          local.tee 1
          i32.const 255
          i32.and
          i32.const 4
          i32.ne
          br_if 2 (;@1;)
          local.get 0
          local.get 4
          i32.store8
          local.get 2
          i32.const 48
          i32.add
          global.set $__stack_pointer
          return
        end
        local.get 2
        local.get 6
        i32.store8 offset=16
        local.get 2
        local.get 3
        i64.const 56
        i64.shr_u
        i64.store8 offset=23
        local.get 2
        local.get 3
        i64.const 40
        i64.shr_u
        i64.store16 offset=21 align=1
        local.get 2
        local.get 3
        i64.const 8
        i64.shr_u
        i64.store32 offset=17 align=1
        i32.const 1048820
        i32.const 43
        local.get 2
        i32.const 16
        i32.add
        i32.const 1048864
        i32.const 1049032
        call $_ZN4core6result13unwrap_failed17h6d050dbd00b66340E
        unreachable
      end
      local.get 2
      local.get 4
      i32.store8 offset=15
      local.get 2
      i32.const 36
      i32.add
      i32.const 1
      i32.store
      local.get 2
      i64.const 1
      i64.store offset=20 align=4
      local.get 2
      i32.const 1048616
      i32.store offset=16
      local.get 2
      i32.const 1
      i32.store offset=44
      local.get 2
      local.get 2
      i32.const 40
      i32.add
      i32.store offset=32
      local.get 2
      local.get 2
      i32.const 15
      i32.add
      i32.store offset=40
      local.get 2
      i32.const 16
      i32.add
      i32.const 1048768
      call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
      unreachable
    end
    local.get 2
    local.get 1
    i32.store8 offset=16
    local.get 2
    local.get 3
    i64.const 56
    i64.shr_u
    i64.store8 offset=23
    local.get 2
    local.get 3
    i64.const 40
    i64.shr_u
    i64.store16 offset=21 align=1
    local.get 2
    local.get 3
    i64.const 8
    i64.shr_u
    i64.store32 offset=17 align=1
    i32.const 1049272
    i32.const 43
    local.get 2
    i32.const 16
    i32.add
    i32.const 1049316
    i32.const 1049472
    call $_ZN4core6result13unwrap_failed17h6d050dbd00b66340E
    unreachable)
  (func $init (type 5) (param i32 i32 i32 i32) (result i64)
    (local i32 i64)
    global.get $__stack_pointer
    i32.const 176
    i32.sub
    local.tee 4
    global.set $__stack_pointer
    call $_ZN7pbc_lib4exit14override_panic17h05ff9ac8a3133db2E
    local.get 4
    local.get 1
    i32.store offset=4
    local.get 4
    local.get 0
    i32.store
    local.get 4
    i32.const 8
    i32.add
    local.get 4
    call $_ZN105_$LT$pbc_contract_common..context..ContractContext$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$13rpc_read_from17hbba672d661e470f7E
    block  ;; label = @1
      block  ;; label = @2
        local.get 4
        i32.load offset=4
        br_if 0 (;@2;)
        local.get 3
        i32.eqz
        br_if 1 (;@1;)
        local.get 4
        i32.const 156
        i32.add
        i32.const 0
        i32.store
        local.get 4
        i32.const 1049056
        i32.store offset=152
        local.get 4
        i64.const 1
        i64.store offset=140 align=4
        local.get 4
        i32.const 1049136
        i32.store offset=136
        local.get 4
        i32.const 136
        i32.add
        i32.const 1049100
        call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
        unreachable
      end
      local.get 4
      i32.const 156
      i32.add
      i32.const 0
      i32.store
      local.get 4
      i32.const 1049056
      i32.store offset=152
      local.get 4
      i64.const 1
      i64.store offset=140 align=4
      local.get 4
      i32.const 1049080
      i32.store offset=136
      local.get 4
      i32.const 136
      i32.add
      i32.const 1049100
      call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
      unreachable
    end
    local.get 4
    i32.const 160
    i32.add
    call $_ZN19pbc_contract_common20ContractResultBuffer3new17h91871de4be5bdae6E
    local.get 4
    i64.const 0
    i64.store offset=140 align=4
    local.get 4
    i32.const 0
    i32.load offset=1049048
    i32.store offset=136
    local.get 4
    i32.const 160
    i32.add
    local.get 4
    i32.const 136
    i32.add
    call $_ZN19pbc_contract_common20ContractResultBuffer12write_events17ha13127c9841d667bE
    local.get 4
    i32.const 136
    i32.add
    i32.const 8
    i32.add
    local.get 4
    i32.const 160
    i32.add
    i32.const 8
    i32.add
    i32.load
    i32.store
    local.get 4
    local.get 4
    i64.load offset=160
    i64.store offset=136
    local.get 4
    i32.const 136
    i32.add
    call $_ZN19pbc_contract_common20ContractResultBuffer22finalize_result_buffer17h793b27b507ddb921E
    local.set 5
    local.get 4
    i32.const 176
    i32.add
    global.set $__stack_pointer
    local.get 5)
  (func $action_02 (type 6) (param i32 i32 i32 i32 i32 i32) (result i64)
    (local i32)
    global.get $__stack_pointer
    i32.const 160
    i32.sub
    local.tee 6
    global.set $__stack_pointer
    call $_ZN7pbc_lib4exit14override_panic17h05ff9ac8a3133db2E
    local.get 6
    local.get 1
    i32.store offset=4
    local.get 6
    local.get 0
    i32.store
    local.get 6
    i32.const 8
    i32.add
    local.get 6
    call $_ZN105_$LT$pbc_contract_common..context..ContractContext$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$13rpc_read_from17hbba672d661e470f7E
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 6
          i32.load offset=4
          br_if 0 (;@3;)
          local.get 3
          br_if 1 (;@2;)
          local.get 5
          i32.eqz
          br_if 2 (;@1;)
          local.get 6
          i32.const 156
          i32.add
          i32.const 0
          i32.store
          local.get 6
          i32.const 1049056
          i32.store offset=152
          local.get 6
          i64.const 1
          i64.store offset=140 align=4
          local.get 6
          i32.const 1049136
          i32.store offset=136
          local.get 6
          i32.const 136
          i32.add
          i32.const 1049144
          call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
          unreachable
        end
        local.get 6
        i32.const 156
        i32.add
        i32.const 0
        i32.store
        local.get 6
        i32.const 1049056
        i32.store offset=152
        local.get 6
        i64.const 1
        i64.store offset=140 align=4
        local.get 6
        i32.const 1049080
        i32.store offset=136
        local.get 6
        i32.const 136
        i32.add
        i32.const 1049144
        call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
        unreachable
      end
      local.get 6
      i32.const 156
      i32.add
      i32.const 0
      i32.store
      local.get 6
      i32.const 1049056
      i32.store offset=152
      local.get 6
      i64.const 1
      i64.store offset=140 align=4
      local.get 6
      i32.const 1049188
      i32.store offset=136
      local.get 6
      i32.const 136
      i32.add
      i32.const 1049144
      call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
      unreachable
    end
    call $_ZN14contract_panic5panic17h73778c331a1f214dE
    unreachable)
  (func $_ZN14contract_panic5panic17h73778c331a1f214dE (type 7)
    (local i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 0
    global.set $__stack_pointer
    local.get 0
    i32.const 28
    i32.add
    i32.const 0
    i32.store
    local.get 0
    i32.const 1049056
    i32.store offset=24
    local.get 0
    i64.const 1
    i64.store offset=12 align=4
    local.get 0
    i32.const 1049212
    i32.store offset=8
    local.get 0
    i32.const 8
    i32.add
    i32.const 1049220
    call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
    unreachable)
  (func $_ZN4core3ptr42drop_in_place$LT$std..io..error..Error$GT$17hdff5e6040e67c8c3E (type 0) (param i32)
    (local i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load8_u
      i32.const 3
      i32.ne
      br_if 0 (;@1;)
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.tee 1
      i32.load
      local.get 1
      i32.load offset=4
      i32.load
      call_indirect (type 0)
      block  ;; label = @2
        local.get 1
        i32.load offset=4
        local.tee 2
        i32.load offset=4
        local.tee 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load
        local.get 3
        local.get 2
        i32.load offset=8
        call $__rust_dealloc
      end
      local.get 0
      i32.load offset=4
      i32.const 12
      i32.const 4
      call $__rust_dealloc
    end)
  (func $__rust_alloc (type 2) (param i32 i32) (result i32)
    (local i32)
    local.get 0
    local.get 1
    call $__rdl_alloc
    local.set 2
    local.get 2
    return)
  (func $__rust_dealloc (type 8) (param i32 i32 i32)
    local.get 0
    local.get 1
    local.get 2
    call $__rdl_dealloc
    return)
  (func $__rust_realloc (type 9) (param i32 i32 i32 i32) (result i32)
    (local i32)
    local.get 0
    local.get 1
    local.get 2
    local.get 3
    call $__rdl_realloc
    local.set 4
    local.get 4
    return)
  (func $__rust_alloc_error_handler (type 3) (param i32 i32)
    local.get 0
    local.get 1
    call $__rg_oom
    return)
  (func $memcpy (type 4) (param i32 i32 i32) (result i32)
    local.get 0
    local.get 1
    local.get 2
    call $_ZN7pbc_lib3mem11pbc_memmove17h982abf5a0b50774dE)
  (func $_ZN44_$LT$$RF$T$u20$as$u20$core..fmt..Display$GT$3fmt17h3a4134da8e0bc735E (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    local.get 1
    call $_ZN42_$LT$str$u20$as$u20$core..fmt..Display$GT$3fmt17he4f5a1a8b2979e2bE)
  (func $_ZN4core3ops8function6FnOnce40call_once$u7b$$u7b$vtable.shim$u7d$$u7d$17h1dd71637518887aaE (type 3) (param i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 2
    i32.const 8
    i32.add
    local.get 1
    call $_ZN7pbc_lib4exit14override_panic28_$u7b$$u7b$closure$u7d$$u7d$17h76a7033cabaa5784E
    local.get 2
    i32.const 16
    i32.add
    global.set $__stack_pointer)
  (func $_ZN7pbc_lib4exit14override_panic28_$u7b$$u7b$closure$u7d$$u7d$17h76a7033cabaa5784E (type 3) (param i32 i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 96
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 2
    i32.const 8
    i32.add
    local.get 1
    call $_ZN4core5panic10panic_info9PanicInfo7payload17h7ab0dcc2dcc4528eE
    block  ;; label = @1
      block  ;; label = @2
        local.get 2
        i32.load offset=8
        local.tee 3
        local.get 2
        i32.load offset=12
        i32.load offset=12
        local.tee 4
        call_indirect (type 1)
        i64.const 9147559743429524724
        i64.ne
        br_if 0 (;@2;)
        local.get 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 2
        local.get 3
        i32.load
        i32.store offset=16
        local.get 3
        i32.load offset=4
        local.set 3
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 3
        local.get 4
        call_indirect (type 1)
        i64.const -2170400421240497668
        i64.ne
        br_if 0 (;@2;)
        local.get 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 2
        local.get 3
        i32.load
        i32.store offset=16
        local.get 3
        i32.load offset=8
        local.set 3
        br 1 (;@1;)
      end
      local.get 2
      i32.const 1049488
      i32.store offset=16
      i32.const 0
      local.set 3
    end
    local.get 2
    local.get 3
    i32.store offset=20
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        call $_ZN4core5panic10panic_info9PanicInfo8location17h759003817d7fe3f4E
        local.tee 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 2
        local.get 3
        call $_ZN4core5panic10panic_info9PanicInfo7payload17h7ab0dcc2dcc4528eE
        local.get 2
        local.get 2
        i64.load
        i64.store offset=40
        local.get 3
        call $_ZN4core5panic8location8Location4line17h24c1eca3491f6000E
        local.set 3
        local.get 2
        i32.const 92
        i32.add
        i32.const 4
        i32.store
        local.get 2
        i32.const 76
        i32.add
        i32.const 2
        i32.store
        local.get 2
        i32.const 5
        i32.store offset=84
        local.get 2
        local.get 3
        i32.store offset=36
        local.get 2
        i64.const 2
        i64.store offset=60 align=4
        local.get 2
        i32.const 1049540
        i32.store offset=56
        local.get 2
        local.get 2
        i32.const 36
        i32.add
        i32.store offset=88
        local.get 2
        local.get 2
        i32.const 40
        i32.add
        i32.store offset=80
        local.get 2
        local.get 2
        i32.const 80
        i32.add
        i32.store offset=72
        local.get 2
        i32.const 24
        i32.add
        local.get 2
        i32.const 56
        i32.add
        call $_ZN5alloc3fmt6format17h6ab9c6dede04b06aE
        br 1 (;@1;)
      end
      local.get 2
      i32.const 76
      i32.add
      i32.const 0
      i32.store
      local.get 2
      i32.const 1049488
      i32.store offset=72
      local.get 2
      i64.const 1
      i64.store offset=60 align=4
      local.get 2
      i32.const 1049528
      i32.store offset=56
      local.get 2
      i32.const 24
      i32.add
      local.get 2
      i32.const 56
      i32.add
      call $_ZN5alloc3fmt6format17h6ab9c6dede04b06aE
    end
    local.get 2
    i32.const 76
    i32.add
    i32.const 2
    i32.store
    local.get 2
    i32.const 92
    i32.add
    i32.const 5
    i32.store
    local.get 2
    i64.const 2
    i64.store offset=60 align=4
    local.get 2
    i32.const 1049560
    i32.store offset=56
    local.get 2
    i32.const 6
    i32.store offset=84
    local.get 2
    local.get 2
    i32.const 80
    i32.add
    i32.store offset=72
    local.get 2
    local.get 2
    i32.const 16
    i32.add
    i32.store offset=88
    local.get 2
    local.get 2
    i32.const 24
    i32.add
    i32.store offset=80
    local.get 2
    i32.const 40
    i32.add
    local.get 2
    i32.const 56
    i32.add
    call $_ZN5alloc3fmt6format17h6ab9c6dede04b06aE
    local.get 2
    i32.load offset=40
    local.tee 3
    local.get 2
    i32.load offset=48
    call $_ZN7pbc_lib4exit8pbc_exit17hc5a8a9b02031e4f6E
    block  ;; label = @1
      local.get 2
      i32.load offset=44
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 3
      local.get 1
      i32.const 1
      call $__rust_dealloc
    end
    block  ;; label = @1
      local.get 2
      i32.load offset=28
      local.tee 3
      i32.eqz
      br_if 0 (;@1;)
      local.get 2
      i32.load offset=24
      local.get 3
      i32.const 1
      call $__rust_dealloc
    end
    local.get 2
    i32.const 96
    i32.add
    global.set $__stack_pointer)
  (func $_ZN4core3ptr79drop_in_place$LT$pbc_lib..exit..override_panic..$u7b$$u7b$closure$u7d$$u7d$$GT$17ha262323faf70b39dE (type 0) (param i32))
  (func $_ZN60_$LT$alloc..string..String$u20$as$u20$core..fmt..Display$GT$3fmt17h95dd8a0b9d13f099E (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=8
    local.get 1
    call $_ZN42_$LT$str$u20$as$u20$core..fmt..Display$GT$3fmt17he4f5a1a8b2979e2bE)
  (func $wasm_exit (type 3) (param i32 i32)
    local.get 0
    local.get 1
    call $_ZN7pbc_lib4exit8pbc_exit17hc5a8a9b02031e4f6E)
  (func $_ZN7pbc_lib4exit14override_panic17h05ff9ac8a3133db2E (type 7)
    i32.const 1
    i32.const 1049488
    call $_ZN3std9panicking8set_hook17hb54d22d6dbc51511E)
  (func $_ZN5alloc7raw_vec11finish_grow17h7e4de5545a75ff3bE.llvm.13783129342151782738 (type 10) (param i32 i32 i32 i32)
    (local i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 2
                      i32.eqz
                      br_if 0 (;@9;)
                      i32.const 1
                      local.set 4
                      local.get 1
                      i32.const 0
                      i32.lt_s
                      br_if 1 (;@8;)
                      local.get 3
                      i32.load
                      local.tee 5
                      i32.eqz
                      br_if 2 (;@7;)
                      local.get 3
                      i32.load offset=4
                      local.tee 3
                      br_if 4 (;@5;)
                      local.get 1
                      i32.eqz
                      br_if 3 (;@6;)
                      br 5 (;@4;)
                    end
                    local.get 0
                    local.get 1
                    i32.store offset=4
                    i32.const 1
                    local.set 4
                  end
                  i32.const 0
                  local.set 1
                  br 6 (;@1;)
                end
                local.get 1
                br_if 2 (;@4;)
              end
              local.get 2
              local.set 3
              br 2 (;@3;)
            end
            local.get 5
            local.get 3
            local.get 2
            local.get 1
            call $__rust_realloc
            local.tee 3
            br_if 1 (;@3;)
            br 2 (;@2;)
          end
          local.get 1
          local.get 2
          call $__rust_alloc
          local.tee 3
          i32.eqz
          br_if 1 (;@2;)
        end
        local.get 0
        local.get 3
        i32.store offset=4
        i32.const 0
        local.set 4
        br 1 (;@1;)
      end
      local.get 0
      local.get 1
      i32.store offset=4
      local.get 2
      local.set 1
    end
    local.get 0
    local.get 4
    i32.store
    local.get 0
    i32.const 8
    i32.add
    local.get 1
    i32.store)
  (func $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E (type 8) (param i32 i32 i32)
    (local i32 i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    block  ;; label = @1
      local.get 1
      local.get 2
      i32.add
      local.tee 2
      local.get 1
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=4
      local.tee 1
      i32.const 1
      i32.shl
      local.tee 4
      local.get 2
      local.get 4
      local.get 2
      i32.gt_u
      select
      local.tee 2
      i32.const 8
      local.get 2
      i32.const 8
      i32.gt_u
      select
      local.set 2
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          i32.const 16
          i32.add
          i32.const 8
          i32.add
          i32.const 1
          i32.store
          local.get 3
          local.get 1
          i32.store offset=20
          local.get 3
          local.get 0
          i32.load
          i32.store offset=16
          br 1 (;@2;)
        end
        local.get 3
        i32.const 0
        i32.store offset=16
      end
      local.get 3
      local.get 2
      i32.const 1
      local.get 3
      i32.const 16
      i32.add
      call $_ZN5alloc7raw_vec11finish_grow17h7e4de5545a75ff3bE.llvm.13783129342151782738
      block  ;; label = @2
        local.get 3
        i32.load
        i32.const 1
        i32.ne
        br_if 0 (;@2;)
        local.get 3
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.eqz
        br_if 1 (;@1;)
        local.get 3
        i32.load offset=4
        local.get 0
        call $_ZN5alloc5alloc18handle_alloc_error17h179b47235854889dE
        unreachable
      end
      local.get 0
      local.get 3
      i64.load offset=4 align=4
      i64.store align=4
      local.get 3
      i32.const 32
      i32.add
      global.set $__stack_pointer
      return
    end
    call $_ZN5alloc7raw_vec17capacity_overflow17hada0f1b01ff34e0eE
    unreachable)
  (func $_ZN4core3ptr42drop_in_place$LT$std..io..error..Error$GT$17h6226570f93ddc57dE (type 0) (param i32)
    (local i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load8_u
      i32.const 3
      i32.ne
      br_if 0 (;@1;)
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.tee 1
      i32.load
      local.get 1
      i32.load offset=4
      i32.load
      call_indirect (type 0)
      block  ;; label = @2
        local.get 1
        i32.load offset=4
        local.tee 2
        i32.load offset=4
        local.tee 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load
        local.get 3
        local.get 2
        i32.load offset=8
        call $__rust_dealloc
      end
      local.get 0
      i32.load offset=4
      i32.const 12
      i32.const 4
      call $__rust_dealloc
    end)
  (func $_ZN4core3ptr83drop_in_place$LT$alloc..vec..Vec$LT$pbc_contract_common..events..EventGroup$GT$$GT$17h4a6c5fdbf5af53c3E (type 0) (param i32)
    (local i32 i32 i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=8
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load
      local.tee 2
      local.get 1
      i32.const 40
      i32.mul
      i32.add
      local.set 3
      loop  ;; label = @2
        block  ;; label = @3
          local.get 2
          local.tee 4
          i32.load offset=16
          local.tee 2
          i32.eqz
          br_if 0 (;@3;)
          local.get 4
          i32.const 20
          i32.add
          i32.load
          local.tee 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 2
          local.get 1
          i32.const 1
          call $__rust_dealloc
        end
        block  ;; label = @3
          local.get 4
          i32.const 36
          i32.add
          i32.load
          local.tee 2
          i32.eqz
          br_if 0 (;@3;)
          local.get 2
          i32.const 56
          i32.mul
          local.set 1
          local.get 4
          i32.load offset=28
          i32.const 16
          i32.add
          local.set 2
          loop  ;; label = @4
            block  ;; label = @5
              local.get 2
              i32.const 4
              i32.add
              i32.load
              local.tee 5
              i32.eqz
              br_if 0 (;@5;)
              local.get 2
              i32.load
              local.get 5
              i32.const 1
              call $__rust_dealloc
            end
            local.get 2
            i32.const 56
            i32.add
            local.set 2
            local.get 1
            i32.const -56
            i32.add
            local.tee 1
            br_if 0 (;@4;)
          end
        end
        local.get 4
        i32.const 40
        i32.add
        local.set 2
        block  ;; label = @3
          local.get 4
          i32.const 32
          i32.add
          i32.load
          local.tee 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          i32.const 56
          i32.mul
          local.tee 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 4
          i32.load offset=28
          local.get 1
          i32.const 8
          call $__rust_dealloc
        end
        local.get 2
        local.get 3
        i32.ne
        br_if 0 (;@2;)
      end
    end
    block  ;; label = @1
      local.get 0
      i32.load offset=4
      local.tee 2
      i32.eqz
      br_if 0 (;@1;)
      local.get 2
      i32.const 40
      i32.mul
      local.tee 2
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load
      local.get 2
      i32.const 8
      call $__rust_dealloc
    end)
  (func $_ZN100_$LT$pbc_contract_common..events..Interaction$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$12rpc_write_to17he1dab06e9926938cE (type 11) (param i32 i32) (result i64)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i64)
    i32.const 16777986
    local.get 0
    i32.load8_u offset=28
    i32.const 3
    i32.shl
    i32.const 24
    i32.and
    i32.const 16
    i32.xor
    i32.shr_u
    local.set 2
    block  ;; label = @1
      local.get 1
      i32.const 4
      i32.add
      local.tee 3
      i32.load
      local.tee 4
      local.get 1
      i32.const 8
      i32.add
      local.tee 5
      i32.load
      local.tee 6
      i32.ne
      br_if 0 (;@1;)
      local.get 1
      local.get 6
      i32.const 1
      call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
      local.get 3
      i32.load
      local.set 4
      local.get 5
      i32.load
      local.set 6
    end
    local.get 5
    local.get 6
    i32.const 1
    i32.add
    local.tee 3
    i32.store
    local.get 1
    i32.load
    local.tee 7
    local.get 6
    i32.add
    local.get 2
    i32.store8
    block  ;; label = @1
      local.get 4
      local.get 3
      i32.sub
      i32.const 19
      i32.gt_u
      br_if 0 (;@1;)
      local.get 1
      local.get 3
      i32.const 20
      call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
      local.get 1
      i32.const 4
      i32.add
      i32.load
      local.set 4
      local.get 1
      i32.const 8
      i32.add
      i32.load
      local.set 3
      local.get 1
      i32.load
      local.set 7
    end
    local.get 1
    i32.const 8
    i32.add
    local.tee 2
    local.get 3
    i32.const 20
    i32.add
    local.tee 6
    i32.store
    local.get 7
    local.get 3
    i32.add
    local.tee 3
    i32.const 16
    i32.add
    local.get 0
    i32.const 45
    i32.add
    i32.load align=1
    i32.store align=1
    local.get 3
    i32.const 8
    i32.add
    local.get 0
    i32.const 37
    i32.add
    i64.load align=1
    i64.store align=1
    local.get 3
    local.get 0
    i32.const 29
    i32.add
    i64.load align=1
    i64.store align=1
    local.get 0
    i32.const 24
    i32.add
    i32.load
    local.tee 5
    i32.const 24
    i32.shl
    local.get 5
    i32.const 8
    i32.shl
    i32.const 16711680
    i32.and
    i32.or
    local.get 5
    i32.const 8
    i32.shr_u
    i32.const 65280
    i32.and
    local.get 5
    i32.const 24
    i32.shr_u
    i32.or
    i32.or
    local.set 8
    block  ;; label = @1
      local.get 4
      local.get 6
      i32.sub
      i32.const 3
      i32.gt_u
      br_if 0 (;@1;)
      local.get 1
      local.get 6
      i32.const 4
      call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
      local.get 2
      i32.load
      local.set 6
      local.get 1
      i32.load
      local.set 7
    end
    local.get 2
    local.get 6
    i32.const 4
    i32.add
    local.tee 3
    i32.store
    local.get 7
    local.get 6
    i32.add
    local.get 8
    i32.store align=1
    block  ;; label = @1
      local.get 5
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=16
      local.set 6
      local.get 1
      i32.const 4
      i32.add
      local.set 9
      local.get 1
      i32.const 8
      i32.add
      local.set 8
      loop  ;; label = @2
        local.get 6
        i32.load8_u
        local.set 2
        block  ;; label = @3
          block  ;; label = @4
            local.get 9
            i32.load
            local.get 3
            i32.eq
            br_if 0 (;@4;)
            local.get 3
            local.set 4
            br 1 (;@3;)
          end
          local.get 1
          local.get 3
          i32.const 1
          call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
          local.get 8
          i32.load
          local.set 4
          local.get 1
          i32.load
          local.set 7
        end
        local.get 6
        i32.const 1
        i32.add
        local.set 6
        local.get 8
        local.get 4
        i32.const 1
        i32.add
        local.tee 3
        i32.store
        local.get 7
        local.get 4
        i32.add
        local.get 2
        i32.store8
        local.get 5
        i32.const -1
        i32.add
        local.tee 5
        br_if 0 (;@2;)
      end
    end
    local.get 0
    i32.load8_u offset=49
    local.set 5
    block  ;; label = @1
      local.get 1
      i32.const 4
      i32.add
      i32.load
      local.get 3
      i32.ne
      br_if 0 (;@1;)
      local.get 1
      local.get 3
      i32.const 1
      call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
      local.get 1
      i32.const 8
      i32.add
      i32.load
      local.set 3
    end
    local.get 1
    i32.const 8
    i32.add
    local.get 3
    i32.const 1
    i32.add
    local.tee 6
    i32.store
    local.get 1
    i32.load
    local.tee 4
    local.get 3
    i32.add
    local.get 5
    i32.store8
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i64.load
        i64.const 1
        i64.eq
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 1
          i32.const 4
          i32.add
          i32.load
          local.get 6
          i32.ne
          br_if 0 (;@3;)
          local.get 1
          local.get 6
          i32.const 1
          call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
          local.get 1
          i32.const 8
          i32.add
          i32.load
          local.set 6
          local.get 1
          i32.load
          local.set 4
        end
        local.get 4
        local.get 6
        i32.add
        i32.const 0
        i32.store8
        local.get 6
        i32.const 1
        i32.add
        local.set 3
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 1
        i32.const 4
        i32.add
        local.tee 3
        i32.load
        local.tee 5
        local.get 6
        i32.ne
        br_if 0 (;@2;)
        local.get 1
        local.get 6
        i32.const 1
        call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
        local.get 3
        i32.load
        local.set 5
        local.get 1
        i32.const 8
        i32.add
        i32.load
        local.set 6
        local.get 1
        i32.load
        local.set 4
      end
      local.get 1
      i32.const 8
      i32.add
      local.tee 2
      local.get 6
      i32.const 1
      i32.add
      local.tee 3
      i32.store
      local.get 4
      local.get 6
      i32.add
      i32.const 1
      i32.store8
      local.get 0
      i64.load offset=8
      local.set 10
      block  ;; label = @2
        local.get 5
        local.get 3
        i32.sub
        i32.const 7
        i32.gt_u
        br_if 0 (;@2;)
        local.get 1
        local.get 3
        i32.const 8
        call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
        local.get 2
        i32.load
        local.set 3
        local.get 1
        i32.load
        local.set 4
      end
      local.get 4
      local.get 3
      i32.add
      local.get 10
      i64.const 56
      i64.shl
      local.get 10
      i64.const 40
      i64.shl
      i64.const 71776119061217280
      i64.and
      i64.or
      local.get 10
      i64.const 24
      i64.shl
      i64.const 280375465082880
      i64.and
      local.get 10
      i64.const 8
      i64.shl
      i64.const 1095216660480
      i64.and
      i64.or
      i64.or
      local.get 10
      i64.const 8
      i64.shr_u
      i64.const 4278190080
      i64.and
      local.get 10
      i64.const 24
      i64.shr_u
      i64.const 16711680
      i64.and
      i64.or
      local.get 10
      i64.const 40
      i64.shr_u
      i64.const 65280
      i64.and
      local.get 10
      i64.const 56
      i64.shr_u
      i64.or
      i64.or
      i64.or
      i64.store align=1
      local.get 3
      i32.const 8
      i32.add
      local.set 3
    end
    local.get 1
    i32.const 8
    i32.add
    local.get 3
    i32.store
    i64.const 4)
  (func $_ZN99_$LT$pbc_contract_common..events..EventGroup$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$12rpc_write_to17h247aed33a93ebd1aE (type 11) (param i32 i32) (result i64)
    (local i32 i64 i64 i32 i32 i32 i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 0
            i32.const 16
            i32.add
            local.get 1
            call $_ZN89_$LT$core..option..Option$LT$T$GT$$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$12rpc_write_to17h9fd7e8ba63943397E
            local.tee 3
            i32.wrap_i64
            i32.const 255
            i32.and
            i32.const 4
            i32.eq
            br_if 0 (;@4;)
            local.get 3
            i64.const 255
            i64.and
            local.tee 4
            i64.const 4
            i64.ne
            br_if 1 (;@3;)
          end
          block  ;; label = @4
            local.get 0
            i64.load
            i64.const 1
            i64.eq
            br_if 0 (;@4;)
            block  ;; label = @5
              local.get 1
              i32.const 4
              i32.add
              i32.load
              local.get 1
              i32.const 8
              i32.add
              local.tee 5
              i32.load
              local.tee 6
              i32.ne
              br_if 0 (;@5;)
              local.get 1
              local.get 6
              i32.const 1
              call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
              local.get 5
              i32.load
              local.set 6
            end
            local.get 5
            local.get 6
            i32.const 1
            i32.add
            local.tee 7
            i32.store
            local.get 1
            i32.load
            local.get 6
            i32.add
            i32.const 0
            i32.store8
            br 2 (;@2;)
          end
          block  ;; label = @4
            local.get 1
            i32.const 4
            i32.add
            local.tee 6
            i32.load
            local.tee 5
            local.get 1
            i32.const 8
            i32.add
            local.tee 8
            i32.load
            local.tee 7
            i32.ne
            br_if 0 (;@4;)
            local.get 1
            local.get 7
            i32.const 1
            call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
            local.get 6
            i32.load
            local.set 5
            local.get 8
            i32.load
            local.set 7
          end
          local.get 8
          local.get 7
          i32.const 1
          i32.add
          local.tee 6
          i32.store
          local.get 1
          i32.load
          local.tee 8
          local.get 7
          i32.add
          i32.const 1
          i32.store8
          local.get 0
          i64.load offset=8
          local.set 3
          block  ;; label = @4
            local.get 5
            local.get 6
            i32.sub
            i32.const 7
            i32.gt_u
            br_if 0 (;@4;)
            local.get 1
            local.get 6
            i32.const 8
            call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
            local.get 1
            i32.const 8
            i32.add
            i32.load
            local.set 6
            local.get 1
            i32.load
            local.set 8
          end
          local.get 1
          i32.const 8
          i32.add
          local.get 6
          i32.const 8
          i32.add
          local.tee 7
          i32.store
          local.get 8
          local.get 6
          i32.add
          local.get 3
          i64.const 56
          i64.shl
          local.get 3
          i64.const 40
          i64.shl
          i64.const 71776119061217280
          i64.and
          i64.or
          local.get 3
          i64.const 24
          i64.shl
          i64.const 280375465082880
          i64.and
          local.get 3
          i64.const 8
          i64.shl
          i64.const 1095216660480
          i64.and
          i64.or
          i64.or
          local.get 3
          i64.const 8
          i64.shr_u
          i64.const 4278190080
          i64.and
          local.get 3
          i64.const 24
          i64.shr_u
          i64.const 16711680
          i64.and
          i64.or
          local.get 3
          i64.const 40
          i64.shr_u
          i64.const 65280
          i64.and
          local.get 3
          i64.const 56
          i64.shr_u
          i64.or
          i64.or
          i64.or
          i64.store align=1
          br 1 (;@2;)
        end
        local.get 3
        i64.const -256
        i64.and
        local.set 3
        br 1 (;@1;)
      end
      local.get 0
      i32.const 36
      i32.add
      i32.load
      local.tee 6
      i32.const 24
      i32.shl
      local.get 6
      i32.const 8
      i32.shl
      i32.const 16711680
      i32.and
      i32.or
      local.get 6
      i32.const 8
      i32.shr_u
      i32.const 65280
      i32.and
      local.get 6
      i32.const 24
      i32.shr_u
      i32.or
      i32.or
      local.set 5
      block  ;; label = @2
        local.get 1
        i32.const 4
        i32.add
        i32.load
        local.get 7
        i32.sub
        i32.const 3
        i32.gt_u
        br_if 0 (;@2;)
        local.get 1
        local.get 7
        i32.const 4
        call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
        local.get 1
        i32.const 8
        i32.add
        i32.load
        local.set 7
      end
      local.get 1
      i32.const 8
      i32.add
      local.get 7
      i32.const 4
      i32.add
      i32.store
      local.get 1
      i32.load
      local.get 7
      i32.add
      local.get 5
      i32.store align=1
      local.get 6
      i32.const 56
      i32.mul
      local.set 6
      local.get 0
      i32.load offset=28
      local.set 0
      loop  ;; label = @2
        block  ;; label = @3
          local.get 6
          br_if 0 (;@3;)
          i64.const 4
          local.set 4
          i64.const 0
          local.set 3
          br 2 (;@1;)
        end
        local.get 6
        i32.const -56
        i32.add
        local.set 6
        local.get 0
        local.get 1
        call $_ZN100_$LT$pbc_contract_common..events..Interaction$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$12rpc_write_to17he1dab06e9926938cE
        local.set 3
        local.get 0
        i32.const 56
        i32.add
        local.set 0
        local.get 3
        i32.wrap_i64
        local.tee 7
        i32.const 255
        i32.and
        i32.const 4
        i32.eq
        br_if 0 (;@2;)
      end
      local.get 2
      local.get 7
      i32.store8 offset=8
      local.get 2
      local.get 3
      i64.const 56
      i64.shr_u
      i64.store8 offset=15
      local.get 2
      local.get 3
      i64.const 40
      i64.shr_u
      i64.store16 offset=13 align=1
      local.get 2
      local.get 3
      i64.const 8
      i64.shr_u
      i64.store32 offset=9 align=1
      i32.const 1049576
      i32.const 43
      local.get 2
      i32.const 8
      i32.add
      i32.const 1049620
      i32.const 1049776
      call $_ZN4core6result13unwrap_failed17h6d050dbd00b66340E
      unreachable
    end
    local.get 2
    i32.const 16
    i32.add
    global.set $__stack_pointer
    local.get 4
    local.get 3
    i64.or)
  (func $_ZN19pbc_contract_common20ContractResultBuffer3new17h91871de4be5bdae6E (type 0) (param i32)
    (local i32)
    block  ;; label = @1
      i32.const 10240
      i32.const 1
      call $__rust_alloc
      local.tee 1
      br_if 0 (;@1;)
      i32.const 10240
      i32.const 1
      call $_ZN5alloc5alloc18handle_alloc_error17h179b47235854889dE
      unreachable
    end
    local.get 0
    i64.const 10240
    i64.store offset=4 align=4
    local.get 0
    local.get 1
    i32.store)
  (func $_ZN19pbc_contract_common20ContractResultBuffer12write_events17ha13127c9841d667bE (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32 i64)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.const 8
          i32.add
          i32.load
          i32.eqz
          br_if 0 (;@3;)
          block  ;; label = @4
            local.get 0
            i32.const 4
            i32.add
            local.tee 3
            i32.load
            local.get 0
            i32.const 8
            i32.add
            i32.load
            local.tee 4
            i32.ne
            br_if 0 (;@4;)
            local.get 0
            local.get 4
            i32.const 1
            call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
            local.get 0
            i32.const 8
            i32.add
            i32.load
            local.set 4
          end
          local.get 0
          i32.load
          local.get 4
          i32.add
          i32.const 1
          i32.store8
          local.get 0
          i32.const 8
          i32.add
          local.tee 5
          local.get 4
          i32.const 1
          i32.add
          local.tee 6
          i32.store
          local.get 6
          local.set 7
          block  ;; label = @4
            local.get 3
            i32.load
            local.get 6
            i32.sub
            i32.const 3
            i32.gt_u
            br_if 0 (;@4;)
            local.get 0
            local.get 6
            i32.const 4
            call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
            local.get 5
            i32.load
            local.set 7
          end
          local.get 0
          i32.load
          local.get 7
          i32.add
          i32.const 0
          i32.store align=1
          local.get 5
          local.get 7
          i32.const 4
          i32.add
          local.tee 3
          i32.store
          local.get 1
          i32.const 8
          i32.add
          i32.load
          local.tee 7
          i32.const 24
          i32.shl
          local.get 7
          i32.const 8
          i32.shl
          i32.const 16711680
          i32.and
          i32.or
          local.get 7
          i32.const 8
          i32.shr_u
          i32.const 65280
          i32.and
          local.get 7
          i32.const 24
          i32.shr_u
          i32.or
          i32.or
          local.set 5
          block  ;; label = @4
            local.get 0
            i32.const 4
            i32.add
            i32.load
            local.get 3
            i32.sub
            i32.const 3
            i32.gt_u
            br_if 0 (;@4;)
            local.get 0
            local.get 3
            i32.const 4
            call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
            local.get 0
            i32.const 8
            i32.add
            i32.load
            local.set 3
          end
          local.get 0
          i32.const 8
          i32.add
          local.get 3
          i32.const 4
          i32.add
          i32.store
          local.get 0
          i32.load
          local.get 3
          i32.add
          local.get 5
          i32.store align=1
          local.get 7
          i32.const 40
          i32.mul
          local.set 7
          local.get 1
          i32.load
          local.set 3
          block  ;; label = @4
            loop  ;; label = @5
              local.get 7
              i32.eqz
              br_if 1 (;@4;)
              local.get 7
              i32.const -40
              i32.add
              local.set 7
              local.get 3
              local.get 0
              call $_ZN99_$LT$pbc_contract_common..events..EventGroup$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$12rpc_write_to17h247aed33a93ebd1aE
              local.set 8
              local.get 3
              i32.const 40
              i32.add
              local.set 3
              local.get 8
              i32.wrap_i64
              local.tee 5
              i32.const 255
              i32.and
              i32.const 4
              i32.eq
              br_if 0 (;@5;)
            end
            local.get 2
            local.get 5
            i32.store8 offset=8
            local.get 2
            local.get 8
            i64.const 56
            i64.shr_u
            i64.store8 offset=15
            local.get 2
            local.get 8
            i64.const 40
            i64.shr_u
            i64.store16 offset=13 align=1
            local.get 2
            local.get 8
            i64.const 8
            i64.shr_u
            i64.store32 offset=9 align=1
            i32.const 1049576
            i32.const 43
            local.get 2
            i32.const 8
            i32.add
            i32.const 1049620
            i32.const 1049776
            call $_ZN4core6result13unwrap_failed17h6d050dbd00b66340E
            unreachable
          end
          local.get 4
          i32.const 5
          i32.add
          local.set 7
          local.get 6
          i32.const -5
          i32.gt_u
          br_if 1 (;@2;)
          local.get 7
          local.get 0
          i32.const 8
          i32.add
          i32.load
          local.tee 3
          i32.gt_u
          br_if 2 (;@1;)
          local.get 0
          i32.load
          local.get 6
          i32.add
          local.get 3
          local.get 4
          i32.sub
          i32.const -5
          i32.add
          local.tee 7
          i32.const 24
          i32.shl
          local.get 7
          i32.const 8
          i32.shl
          i32.const 16711680
          i32.and
          i32.or
          local.get 7
          i32.const 8
          i32.shr_u
          i32.const 65280
          i32.and
          local.get 7
          i32.const 24
          i32.shr_u
          i32.or
          i32.or
          i32.store align=1
        end
        local.get 1
        call $_ZN4core3ptr83drop_in_place$LT$alloc..vec..Vec$LT$pbc_contract_common..events..EventGroup$GT$$GT$17h4a6c5fdbf5af53c3E
        local.get 2
        i32.const 16
        i32.add
        global.set $__stack_pointer
        return
      end
      local.get 6
      local.get 7
      i32.const 1049932
      call $_ZN4core5slice5index22slice_index_order_fail17hbafd09170640ee9bE
      unreachable
    end
    local.get 7
    local.get 3
    i32.const 1049932
    call $_ZN4core5slice5index24slice_end_index_len_fail17h9f1e36a81f2d5daaE
    unreachable)
  (func $_ZN19pbc_contract_common20ContractResultBuffer22finalize_result_buffer17h793b27b507ddb921E (type 1) (param i32) (result i64)
    local.get 0
    i64.load32_u offset=8
    i64.const 32
    i64.shl
    local.get 0
    i64.load32_u
    i64.or)
  (func $_ZN89_$LT$core..option..Option$LT$T$GT$$u20$as$u20$pbc_traits..readwrite_rpc..ReadWriteRPC$GT$12rpc_write_to17h9fd7e8ba63943397E (type 11) (param i32 i32) (result i64)
    (local i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 2
      br_if 0 (;@1;)
      block  ;; label = @2
        local.get 1
        i32.const 4
        i32.add
        i32.load
        local.get 1
        i32.const 8
        i32.add
        local.tee 0
        i32.load
        local.tee 2
        i32.ne
        br_if 0 (;@2;)
        local.get 1
        local.get 2
        i32.const 1
        call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
        local.get 0
        i32.load
        local.set 2
      end
      local.get 0
      local.get 2
      i32.const 1
      i32.add
      i32.store
      local.get 1
      i32.load
      local.get 2
      i32.add
      i32.const 0
      i32.store8
      i64.const 4
      return
    end
    block  ;; label = @1
      local.get 1
      i32.const 4
      i32.add
      local.tee 3
      i32.load
      local.tee 4
      local.get 1
      i32.const 8
      i32.add
      local.tee 5
      i32.load
      local.tee 6
      i32.ne
      br_if 0 (;@1;)
      local.get 1
      local.get 6
      i32.const 1
      call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
      local.get 3
      i32.load
      local.set 4
      local.get 5
      i32.load
      local.set 6
    end
    local.get 5
    local.get 6
    i32.const 1
    i32.add
    local.tee 3
    i32.store
    local.get 1
    i32.load
    local.tee 5
    local.get 6
    i32.add
    i32.const 1
    i32.store8
    local.get 0
    i32.const 8
    i32.add
    i32.load
    local.tee 6
    i32.const 24
    i32.shl
    local.get 6
    i32.const 8
    i32.shl
    i32.const 16711680
    i32.and
    i32.or
    local.get 6
    i32.const 8
    i32.shr_u
    i32.const 65280
    i32.and
    local.get 6
    i32.const 24
    i32.shr_u
    i32.or
    i32.or
    local.set 7
    block  ;; label = @1
      local.get 4
      local.get 3
      i32.sub
      i32.const 3
      i32.gt_u
      br_if 0 (;@1;)
      local.get 1
      local.get 3
      i32.const 4
      call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
      local.get 1
      i32.const 8
      i32.add
      i32.load
      local.set 3
      local.get 1
      i32.load
      local.set 5
    end
    local.get 1
    i32.const 8
    i32.add
    local.tee 8
    local.get 3
    i32.const 4
    i32.add
    local.tee 0
    i32.store
    local.get 5
    local.get 3
    i32.add
    local.get 7
    i32.store align=1
    block  ;; label = @1
      local.get 6
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 4
      i32.add
      local.set 7
      loop  ;; label = @2
        local.get 2
        i32.load8_u
        local.set 4
        block  ;; label = @3
          block  ;; label = @4
            local.get 7
            i32.load
            local.get 0
            i32.eq
            br_if 0 (;@4;)
            local.get 0
            local.set 3
            br 1 (;@3;)
          end
          local.get 1
          local.get 0
          i32.const 1
          call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h9d80ecf3d392d0a1E
          local.get 8
          i32.load
          local.set 3
          local.get 1
          i32.load
          local.set 5
        end
        local.get 2
        i32.const 1
        i32.add
        local.set 2
        local.get 8
        local.get 3
        i32.const 1
        i32.add
        local.tee 0
        i32.store
        local.get 5
        local.get 3
        i32.add
        local.get 4
        i32.store8
        local.get 6
        i32.const -1
        i32.add
        local.tee 6
        br_if 0 (;@2;)
      end
    end
    i64.const 4)
  (func $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17h652f73c7551ae1b1E (type 1) (param i32) (result i64)
    i64.const -2170400421240497668)
  (func $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17hd3828b2d5b220c95E (type 1) (param i32) (result i64)
    i64.const 9147559743429524724)
  (func $_ZN42_$LT$$RF$T$u20$as$u20$core..fmt..Debug$GT$3fmt17h2e70fb27e7e00addE (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.tee 0
    i32.load
    local.get 1
    local.get 0
    i32.load offset=4
    i32.load offset=12
    call_indirect (type 2))
  (func $_ZN42_$LT$$RF$T$u20$as$u20$core..fmt..Debug$GT$3fmt17h40ebbd3d0573d9c8E (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    call $_ZN62_$LT$std..io..error..ErrorKind$u20$as$u20$core..fmt..Debug$GT$3fmt17h3c7f418217a51dfeE)
  (func $_ZN62_$LT$std..io..error..ErrorKind$u20$as$u20$core..fmt..Debug$GT$3fmt17h3c7f418217a51dfeE (type 2) (param i32 i32) (result i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          block  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                block  ;; label = @15
                                  block  ;; label = @16
                                    block  ;; label = @17
                                      block  ;; label = @18
                                        block  ;; label = @19
                                          block  ;; label = @20
                                            block  ;; label = @21
                                              block  ;; label = @22
                                                block  ;; label = @23
                                                  block  ;; label = @24
                                                    block  ;; label = @25
                                                      block  ;; label = @26
                                                        block  ;; label = @27
                                                          block  ;; label = @28
                                                            block  ;; label = @29
                                                              block  ;; label = @30
                                                                block  ;; label = @31
                                                                  block  ;; label = @32
                                                                    block  ;; label = @33
                                                                      block  ;; label = @34
                                                                        block  ;; label = @35
                                                                          block  ;; label = @36
                                                                            block  ;; label = @37
                                                                              block  ;; label = @38
                                                                                block  ;; label = @39
                                                                                  block  ;; label = @40
                                                                                    block  ;; label = @41
                                                                                      local.get 0
                                                                                      i32.load8_u
                                                                                      br_table 0 (;@41;) 1 (;@40;) 2 (;@39;) 3 (;@38;) 4 (;@37;) 5 (;@36;) 6 (;@35;) 7 (;@34;) 8 (;@33;) 9 (;@32;) 10 (;@31;) 11 (;@30;) 12 (;@29;) 13 (;@28;) 14 (;@27;) 15 (;@26;) 16 (;@25;) 17 (;@24;) 18 (;@23;) 19 (;@22;) 20 (;@21;) 21 (;@20;) 22 (;@19;) 23 (;@18;) 24 (;@17;) 25 (;@16;) 26 (;@15;) 27 (;@14;) 28 (;@13;) 29 (;@12;) 30 (;@11;) 31 (;@10;) 32 (;@9;) 33 (;@8;) 34 (;@7;) 35 (;@6;) 36 (;@5;) 37 (;@4;) 38 (;@3;) 39 (;@2;) 40 (;@1;) 0 (;@41;)
                                                                                    end
                                                                                    local.get 1
                                                                                    i32.const 1050919
                                                                                    i32.const 8
                                                                                    call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                                    return
                                                                                  end
                                                                                  local.get 1
                                                                                  i32.const 1050903
                                                                                  i32.const 16
                                                                                  call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                                  return
                                                                                end
                                                                                local.get 1
                                                                                i32.const 1050886
                                                                                i32.const 17
                                                                                call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                                return
                                                                              end
                                                                              local.get 1
                                                                              i32.const 1050871
                                                                              i32.const 15
                                                                              call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                              return
                                                                            end
                                                                            local.get 1
                                                                            i32.const 1050856
                                                                            i32.const 15
                                                                            call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                            return
                                                                          end
                                                                          local.get 1
                                                                          i32.const 1050838
                                                                          i32.const 18
                                                                          call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                          return
                                                                        end
                                                                        local.get 1
                                                                        i32.const 1050821
                                                                        i32.const 17
                                                                        call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                        return
                                                                      end
                                                                      local.get 1
                                                                      i32.const 1050809
                                                                      i32.const 12
                                                                      call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                      return
                                                                    end
                                                                    local.get 1
                                                                    i32.const 1050800
                                                                    i32.const 9
                                                                    call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                    return
                                                                  end
                                                                  local.get 1
                                                                  i32.const 1050784
                                                                  i32.const 16
                                                                  call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                  return
                                                                end
                                                                local.get 1
                                                                i32.const 1050773
                                                                i32.const 11
                                                                call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                                return
                                                              end
                                                              local.get 1
                                                              i32.const 1050763
                                                              i32.const 10
                                                              call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                              return
                                                            end
                                                            local.get 1
                                                            i32.const 1050750
                                                            i32.const 13
                                                            call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                            return
                                                          end
                                                          local.get 1
                                                          i32.const 1050740
                                                          i32.const 10
                                                          call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                          return
                                                        end
                                                        local.get 1
                                                        i32.const 1050727
                                                        i32.const 13
                                                        call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                        return
                                                      end
                                                      local.get 1
                                                      i32.const 1050715
                                                      i32.const 12
                                                      call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                      return
                                                    end
                                                    local.get 1
                                                    i32.const 1050698
                                                    i32.const 17
                                                    call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                    return
                                                  end
                                                  local.get 1
                                                  i32.const 1050680
                                                  i32.const 18
                                                  call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                  return
                                                end
                                                local.get 1
                                                i32.const 1050666
                                                i32.const 14
                                                call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                                return
                                              end
                                              local.get 1
                                              i32.const 1050644
                                              i32.const 22
                                              call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                              return
                                            end
                                            local.get 1
                                            i32.const 1050632
                                            i32.const 12
                                            call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                            return
                                          end
                                          local.get 1
                                          i32.const 1050621
                                          i32.const 11
                                          call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                          return
                                        end
                                        local.get 1
                                        i32.const 1050613
                                        i32.const 8
                                        call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                        return
                                      end
                                      local.get 1
                                      i32.const 1050604
                                      i32.const 9
                                      call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                      return
                                    end
                                    local.get 1
                                    i32.const 1050593
                                    i32.const 11
                                    call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                    return
                                  end
                                  local.get 1
                                  i32.const 1050582
                                  i32.const 11
                                  call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                  return
                                end
                                local.get 1
                                i32.const 1050559
                                i32.const 23
                                call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                                return
                              end
                              local.get 1
                              i32.const 1050547
                              i32.const 12
                              call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                              return
                            end
                            local.get 1
                            i32.const 1050535
                            i32.const 12
                            call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                            return
                          end
                          local.get 1
                          i32.const 1050517
                          i32.const 18
                          call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                          return
                        end
                        local.get 1
                        i32.const 1050509
                        i32.const 8
                        call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                        return
                      end
                      local.get 1
                      i32.const 1050495
                      i32.const 14
                      call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                      return
                    end
                    local.get 1
                    i32.const 1050483
                    i32.const 12
                    call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                    return
                  end
                  local.get 1
                  i32.const 1050468
                  i32.const 15
                  call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                  return
                end
                local.get 1
                i32.const 1050449
                i32.const 19
                call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
                return
              end
              local.get 1
              i32.const 1050438
              i32.const 11
              call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
              return
            end
            local.get 1
            i32.const 1050340
            i32.const 11
            call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
            return
          end
          local.get 1
          i32.const 1050425
          i32.const 13
          call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
          return
        end
        local.get 1
        i32.const 1050414
        i32.const 11
        call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
        return
      end
      local.get 1
      i32.const 1050409
      i32.const 5
      call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E
      return
    end
    local.get 1
    i32.const 1050396
    i32.const 13
    call $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E)
  (func $_ZN42_$LT$$RF$T$u20$as$u20$core..fmt..Debug$GT$3fmt17hd5a0e5bf2fc11a8eE (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    local.get 1
    call $_ZN40_$LT$str$u20$as$u20$core..fmt..Debug$GT$3fmt17h7f08598322370188E)
  (func $_ZN4core3fmt3num50_$LT$impl$u20$core..fmt..Debug$u20$for$u20$i32$GT$3fmt17he5b0a965714491afE (type 2) (param i32 i32) (result i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        call $_ZN4core3fmt9Formatter15debug_lower_hex17h4a106312d74296a5E
        br_if 0 (;@2;)
        local.get 1
        call $_ZN4core3fmt9Formatter15debug_upper_hex17hf60b1b844cd134fcE
        br_if 1 (;@1;)
        local.get 0
        local.get 1
        call $_ZN4core3fmt3num3imp52_$LT$impl$u20$core..fmt..Display$u20$for$u20$i32$GT$3fmt17hf4754ab964e4babeE
        return
      end
      local.get 0
      local.get 1
      call $_ZN4core3fmt3num53_$LT$impl$u20$core..fmt..LowerHex$u20$for$u20$i32$GT$3fmt17hbc57992e5da9d71eE
      return
    end
    local.get 0
    local.get 1
    call $_ZN4core3fmt3num53_$LT$impl$u20$core..fmt..UpperHex$u20$for$u20$i32$GT$3fmt17h4cba4dfa34406e54E)
  (func $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h128f4370d749c5f7E (type 8) (param i32 i32 i32)
    (local i32 i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    block  ;; label = @1
      local.get 1
      local.get 2
      i32.add
      local.tee 2
      local.get 1
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.tee 1
      i32.const 1
      i32.shl
      local.tee 4
      local.get 2
      local.get 4
      local.get 2
      i32.gt_u
      select
      local.tee 2
      i32.const 8
      local.get 2
      i32.const 8
      i32.gt_u
      select
      local.set 2
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          i32.const 16
          i32.add
          i32.const 8
          i32.add
          i32.const 1
          i32.store
          local.get 3
          local.get 1
          i32.store offset=20
          local.get 3
          local.get 0
          i32.load
          i32.store offset=16
          br 1 (;@2;)
        end
        local.get 3
        i32.const 0
        i32.store offset=16
      end
      local.get 3
      local.get 2
      i32.const 1
      local.get 3
      i32.const 16
      i32.add
      call $_ZN5alloc7raw_vec11finish_grow17h64fec9e79f196b69E
      block  ;; label = @2
        local.get 3
        i32.load
        i32.const 1
        i32.ne
        br_if 0 (;@2;)
        local.get 3
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.eqz
        br_if 1 (;@1;)
        local.get 3
        i32.load offset=4
        local.get 0
        call $_ZN5alloc5alloc18handle_alloc_error17h179b47235854889dE
        unreachable
      end
      local.get 0
      local.get 3
      i64.load offset=4 align=4
      i64.store align=4
      local.get 3
      i32.const 32
      i32.add
      global.set $__stack_pointer
      return
    end
    call $_ZN5alloc7raw_vec17capacity_overflow17hada0f1b01ff34e0eE
    unreachable)
  (func $_ZN4core3ptr100drop_in_place$LT$$RF$mut$u20$std..io..Write..write_fmt..Adapter$LT$alloc..vec..Vec$LT$u8$GT$$GT$$GT$17h339b3d5f0693762fE (type 0) (param i32))
  (func $_ZN3std9panicking11panic_count17is_zero_slow_path17hd1df70a45d849404E (type 12) (result i32)
    block  ;; label = @1
      i32.const 0
      i32.load offset=1055088
      i32.const 1
      i32.ne
      br_if 0 (;@1;)
      i32.const 0
      i32.load offset=1055092
      i32.eqz
      return
    end
    i32.const 0
    i64.const 1
    i64.store offset=1055088
    i32.const 1)
  (func $_ZN4core3ptr226drop_in_place$LT$std..error..$LT$impl$u20$core..convert..From$LT$alloc..string..String$GT$$u20$for$u20$alloc..boxed..Box$LT$dyn$u20$std..error..Error$u2b$core..marker..Sync$u2b$core..marker..Send$GT$$GT$..from..StringError$GT$17h0ee9186882c5bd0bE (type 0) (param i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.load
      local.tee 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      i32.const 1
      call $__rust_dealloc
    end)
  (func $_ZN4core3ptr70drop_in_place$LT$std..panicking..begin_panic_handler..PanicPayload$GT$17h84169870e0654f03E (type 0) (param i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=4
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const 8
      i32.add
      i32.load
      local.tee 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      local.get 0
      i32.const 1
      call $__rust_dealloc
    end)
  (func $_ZN4core6option15Option$LT$T$GT$6unwrap17h97c6976d505cbddeE (type 2) (param i32 i32) (result i32)
    block  ;; label = @1
      local.get 0
      br_if 0 (;@1;)
      i32.const 1049972
      i32.const 43
      local.get 1
      call $_ZN4core9panicking5panic17h35778e3c18266c93E
      unreachable
    end
    local.get 0)
  (func $_ZN4core6option15Option$LT$T$GT$6unwrap17hbc0c812220a7cf70E (type 13) (param i32) (result i32)
    block  ;; label = @1
      local.get 0
      br_if 0 (;@1;)
      i32.const 1049972
      i32.const 43
      i32.const 1050232
      call $_ZN4core9panicking5panic17h35778e3c18266c93E
      unreachable
    end
    local.get 0)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$10write_char17hf8370214c82b4737E (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 0
    i32.load
    local.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 127
        i32.gt_u
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 0
          i32.load offset=8
          local.tee 3
          local.get 0
          i32.const 4
          i32.add
          i32.load
          i32.ne
          br_if 0 (;@3;)
          local.get 0
          local.get 3
          i32.const 1
          call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h128f4370d749c5f7E
          local.get 0
          i32.load offset=8
          local.set 3
        end
        local.get 0
        local.get 3
        i32.const 1
        i32.add
        i32.store offset=8
        local.get 0
        i32.load
        local.get 3
        i32.add
        local.get 1
        i32.store8
        br 1 (;@1;)
      end
      local.get 2
      i32.const 0
      i32.store offset=12
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.const 2048
          i32.lt_u
          br_if 0 (;@3;)
          block  ;; label = @4
            local.get 1
            i32.const 65536
            i32.lt_u
            br_if 0 (;@4;)
            local.get 2
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=15
            local.get 2
            local.get 1
            i32.const 18
            i32.shr_u
            i32.const 240
            i32.or
            i32.store8 offset=12
            local.get 2
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=14
            local.get 2
            local.get 1
            i32.const 12
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=13
            i32.const 4
            local.set 1
            br 2 (;@2;)
          end
          local.get 2
          local.get 1
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=14
          local.get 2
          local.get 1
          i32.const 12
          i32.shr_u
          i32.const 224
          i32.or
          i32.store8 offset=12
          local.get 2
          local.get 1
          i32.const 6
          i32.shr_u
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=13
          i32.const 3
          local.set 1
          br 1 (;@2;)
        end
        local.get 2
        local.get 1
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=13
        local.get 2
        local.get 1
        i32.const 6
        i32.shr_u
        i32.const 192
        i32.or
        i32.store8 offset=12
        i32.const 2
        local.set 1
      end
      block  ;; label = @2
        local.get 0
        i32.const 4
        i32.add
        i32.load
        local.get 0
        i32.const 8
        i32.add
        local.tee 4
        i32.load
        local.tee 3
        i32.sub
        local.get 1
        i32.ge_u
        br_if 0 (;@2;)
        local.get 0
        local.get 3
        local.get 1
        call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h128f4370d749c5f7E
        local.get 4
        i32.load
        local.set 3
      end
      local.get 0
      i32.load
      local.get 3
      i32.add
      local.get 2
      i32.const 12
      i32.add
      local.get 1
      call $memcpy
      drop
      local.get 4
      local.get 3
      local.get 1
      i32.add
      i32.store
    end
    local.get 2
    i32.const 16
    i32.add
    global.set $__stack_pointer
    i32.const 0)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_fmt17h9091e91844c7467cE (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 2
    local.get 0
    i32.load
    i32.store offset=4
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=8
    local.get 2
    i32.const 4
    i32.add
    i32.const 1049948
    local.get 2
    i32.const 8
    i32.add
    call $_ZN4core3fmt5write17h8874ba14b0fd43feE
    local.set 1
    local.get 2
    i32.const 32
    i32.add
    global.set $__stack_pointer
    local.get 1)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_str17hb2141a7688c38d13E (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 3
      i32.const 4
      i32.add
      i32.load
      local.get 3
      i32.const 8
      i32.add
      local.tee 4
      i32.load
      local.tee 0
      i32.sub
      local.get 2
      i32.ge_u
      br_if 0 (;@1;)
      local.get 3
      local.get 0
      local.get 2
      call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h128f4370d749c5f7E
      local.get 4
      i32.load
      local.set 0
    end
    local.get 3
    i32.load
    local.get 0
    i32.add
    local.get 1
    local.get 2
    call $memcpy
    drop
    local.get 4
    local.get 0
    local.get 2
    i32.add
    i32.store
    i32.const 0)
  (func $_ZN58_$LT$alloc..string..String$u20$as$u20$core..fmt..Debug$GT$3fmt17h97fafa98adacfdd8E (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 0
    i32.const 8
    i32.add
    i32.load
    local.get 1
    call $_ZN40_$LT$str$u20$as$u20$core..fmt..Debug$GT$3fmt17h7f08598322370188E)
  (func $_ZN5alloc7raw_vec11finish_grow17h64fec9e79f196b69E (type 10) (param i32 i32 i32 i32)
    (local i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 2
                  i32.eqz
                  br_if 0 (;@7;)
                  i32.const 1
                  local.set 4
                  local.get 1
                  i32.const 0
                  i32.lt_s
                  br_if 1 (;@6;)
                  local.get 3
                  i32.load
                  local.tee 5
                  i32.eqz
                  br_if 3 (;@4;)
                  local.get 3
                  i32.load offset=4
                  local.tee 3
                  br_if 2 (;@5;)
                  local.get 1
                  br_if 4 (;@3;)
                  local.get 2
                  local.set 3
                  br 5 (;@2;)
                end
                local.get 0
                local.get 1
                i32.store offset=4
                i32.const 1
                local.set 4
              end
              i32.const 0
              local.set 1
              br 4 (;@1;)
            end
            local.get 5
            local.get 3
            local.get 2
            local.get 1
            call $__rust_realloc
            local.set 3
            br 2 (;@2;)
          end
          local.get 1
          br_if 0 (;@3;)
          local.get 2
          local.set 3
          br 1 (;@2;)
        end
        local.get 1
        local.get 2
        call $__rust_alloc
        local.set 3
      end
      block  ;; label = @2
        local.get 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        local.get 3
        i32.store offset=4
        i32.const 0
        local.set 4
        br 1 (;@1;)
      end
      local.get 0
      local.get 1
      i32.store offset=4
      local.get 2
      local.set 1
    end
    local.get 0
    local.get 4
    i32.store
    local.get 0
    i32.const 8
    i32.add
    local.get 1
    i32.store)
  (func $_ZN8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h51a06b01c68229b2E (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 1
              i32.const 9
              i32.lt_u
              br_if 0 (;@5;)
              i32.const 16
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
              local.get 1
              i32.gt_u
              br_if 1 (;@4;)
              br 2 (;@3;)
            end
            local.get 0
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h1538d4b11d1da1beE
            local.set 2
            br 2 (;@2;)
          end
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.set 1
        end
        call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
        local.set 3
        i32.const 0
        local.set 2
        local.get 3
        local.get 3
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        i32.const 20
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        i32.add
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        i32.add
        i32.sub
        i32.const -65544
        i32.add
        i32.const -9
        i32.and
        i32.const -3
        i32.add
        local.tee 3
        i32.const 0
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        i32.const 2
        i32.shl
        i32.sub
        local.tee 4
        local.get 4
        local.get 3
        i32.gt_u
        select
        local.get 1
        i32.sub
        local.get 0
        i32.le_u
        br_if 0 (;@2;)
        local.get 1
        i32.const 16
        local.get 0
        i32.const 4
        i32.add
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        i32.const -5
        i32.add
        local.get 0
        i32.gt_u
        select
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        local.tee 4
        i32.add
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        i32.add
        i32.const -4
        i32.add
        call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h1538d4b11d1da1beE
        local.tee 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        call $_ZN8dlmalloc8dlmalloc5Chunk8from_mem17h7acff0b43825b6c7E
        local.set 0
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.const -1
            i32.add
            local.tee 2
            local.get 3
            i32.and
            br_if 0 (;@4;)
            local.get 0
            local.set 1
            br 1 (;@3;)
          end
          local.get 2
          local.get 3
          i32.add
          i32.const 0
          local.get 1
          i32.sub
          i32.and
          call $_ZN8dlmalloc8dlmalloc5Chunk8from_mem17h7acff0b43825b6c7E
          local.set 2
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.set 3
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
          local.get 2
          i32.const 0
          local.get 1
          local.get 2
          local.get 0
          i32.sub
          local.get 3
          i32.gt_u
          select
          i32.add
          local.tee 1
          local.get 0
          i32.sub
          local.tee 2
          i32.sub
          local.set 3
          block  ;; label = @4
            local.get 0
            call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17h374540cc5223b826E
            br_if 0 (;@4;)
            local.get 1
            local.get 3
            call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
            local.get 0
            local.get 2
            call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
            local.get 0
            local.get 2
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$13dispose_chunk17hd7d0611236d858ddE
            br 1 (;@3;)
          end
          local.get 0
          i32.load
          local.set 0
          local.get 1
          local.get 3
          i32.store offset=4
          local.get 1
          local.get 0
          local.get 2
          i32.add
          i32.store
        end
        local.get 1
        call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17h374540cc5223b826E
        br_if 1 (;@1;)
        local.get 1
        call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
        local.tee 0
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        local.get 4
        i32.add
        i32.le_u
        br_if 1 (;@1;)
        local.get 1
        local.get 4
        call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
        local.set 2
        local.get 1
        local.get 4
        call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
        local.get 2
        local.get 0
        local.get 4
        i32.sub
        local.tee 0
        call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
        local.get 2
        local.get 0
        call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$13dispose_chunk17hd7d0611236d858ddE
        br 1 (;@1;)
      end
      local.get 2
      return
    end
    local.get 1
    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
    local.set 0
    local.get 1
    call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17h374540cc5223b826E
    drop
    local.get 0)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h1538d4b11d1da1beE (type 13) (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 1
    global.set $__stack_pointer
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          i32.const 245
          i32.lt_u
          br_if 0 (;@3;)
          call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
          local.set 2
          i32.const 0
          local.set 3
          local.get 2
          local.get 2
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.const 20
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.add
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.add
          i32.sub
          i32.const -65544
          i32.add
          i32.const -9
          i32.and
          i32.const -3
          i32.add
          local.tee 2
          i32.const 0
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.const 2
          i32.shl
          i32.sub
          local.tee 4
          local.get 4
          local.get 2
          i32.gt_u
          select
          local.get 0
          i32.le_u
          br_if 2 (;@1;)
          local.get 0
          i32.const 4
          i32.add
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.set 2
          i32.const 0
          i32.load offset=1054640
          i32.eqz
          br_if 1 (;@2;)
          i32.const 0
          local.set 5
          block  ;; label = @4
            local.get 2
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            i32.const 31
            local.set 5
            local.get 2
            i32.const 16777215
            i32.gt_u
            br_if 0 (;@4;)
            local.get 2
            i32.const 6
            local.get 2
            i32.const 8
            i32.shr_u
            i32.clz
            local.tee 0
            i32.sub
            i32.shr_u
            i32.const 1
            i32.and
            local.get 0
            i32.const 1
            i32.shl
            i32.sub
            i32.const 62
            i32.add
            local.set 5
          end
          i32.const 0
          local.get 2
          i32.sub
          local.set 3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 5
                i32.const 2
                i32.shl
                i32.const 1054908
                i32.add
                i32.load
                local.tee 0
                i32.eqz
                br_if 0 (;@6;)
                local.get 2
                local.get 5
                call $_ZN8dlmalloc8dlmalloc24leftshift_for_tree_index17he003bc47d801254fE
                i32.shl
                local.set 6
                i32.const 0
                local.set 7
                i32.const 0
                local.set 4
                loop  ;; label = @7
                  block  ;; label = @8
                    local.get 0
                    call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
                    call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
                    local.tee 8
                    local.get 2
                    i32.lt_u
                    br_if 0 (;@8;)
                    local.get 8
                    local.get 2
                    i32.sub
                    local.tee 8
                    local.get 3
                    i32.ge_u
                    br_if 0 (;@8;)
                    local.get 8
                    local.set 3
                    local.get 0
                    local.set 4
                    local.get 8
                    br_if 0 (;@8;)
                    i32.const 0
                    local.set 3
                    local.get 0
                    local.set 4
                    br 3 (;@5;)
                  end
                  local.get 0
                  i32.const 20
                  i32.add
                  i32.load
                  local.tee 8
                  local.get 7
                  local.get 8
                  local.get 0
                  local.get 6
                  i32.const 29
                  i32.shr_u
                  i32.const 4
                  i32.and
                  i32.add
                  i32.const 16
                  i32.add
                  i32.load
                  local.tee 0
                  i32.ne
                  select
                  local.get 7
                  local.get 8
                  select
                  local.set 7
                  local.get 6
                  i32.const 1
                  i32.shl
                  local.set 6
                  local.get 0
                  br_if 0 (;@7;)
                end
                block  ;; label = @7
                  local.get 7
                  i32.eqz
                  br_if 0 (;@7;)
                  local.get 7
                  local.set 0
                  br 2 (;@5;)
                end
                local.get 4
                br_if 2 (;@4;)
              end
              i32.const 0
              local.set 4
              i32.const 1
              local.get 5
              i32.shl
              call $_ZN8dlmalloc8dlmalloc9left_bits17hde52ae0f2ac5b398E
              i32.const 0
              i32.load offset=1054640
              i32.and
              local.tee 0
              i32.eqz
              br_if 3 (;@2;)
              local.get 0
              call $_ZN8dlmalloc8dlmalloc9least_bit17h06921975976171dcE
              i32.ctz
              i32.const 2
              i32.shl
              i32.const 1054908
              i32.add
              i32.load
              local.tee 0
              i32.eqz
              br_if 3 (;@2;)
            end
            loop  ;; label = @5
              local.get 0
              local.get 4
              local.get 0
              call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
              call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
              local.tee 7
              local.get 2
              i32.ge_u
              local.get 7
              local.get 2
              i32.sub
              local.tee 7
              local.get 3
              i32.lt_u
              i32.and
              local.tee 6
              select
              local.set 4
              local.get 7
              local.get 3
              local.get 6
              select
              local.set 3
              local.get 0
              call $_ZN8dlmalloc8dlmalloc9TreeChunk14leftmost_child17hf6286ca5315942e0E
              local.tee 0
              br_if 0 (;@5;)
            end
            local.get 4
            i32.eqz
            br_if 2 (;@2;)
          end
          block  ;; label = @4
            i32.const 0
            i32.load offset=1055036
            local.tee 0
            local.get 2
            i32.lt_u
            br_if 0 (;@4;)
            local.get 3
            local.get 0
            local.get 2
            i32.sub
            i32.ge_u
            br_if 2 (;@2;)
          end
          local.get 4
          call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
          local.tee 0
          local.get 2
          call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
          local.set 7
          local.get 4
          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hd87001f22d4c88d2E
          block  ;; label = @4
            block  ;; label = @5
              local.get 3
              i32.const 16
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
              i32.lt_u
              br_if 0 (;@5;)
              local.get 0
              local.get 2
              call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hbc5285bcaa5cf3fdE
              local.get 7
              local.get 3
              call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE
              block  ;; label = @6
                local.get 3
                i32.const 256
                i32.lt_u
                br_if 0 (;@6;)
                local.get 7
                local.get 3
                call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17h109ba5955a0705b3E
                br 2 (;@4;)
              end
              local.get 3
              i32.const 3
              i32.shr_u
              local.tee 4
              i32.const 3
              i32.shl
              i32.const 1054644
              i32.add
              local.set 3
              block  ;; label = @6
                block  ;; label = @7
                  i32.const 0
                  i32.load offset=1054636
                  local.tee 6
                  i32.const 1
                  local.get 4
                  i32.shl
                  local.tee 4
                  i32.and
                  i32.eqz
                  br_if 0 (;@7;)
                  local.get 3
                  i32.load offset=8
                  local.set 4
                  br 1 (;@6;)
                end
                i32.const 0
                local.get 6
                local.get 4
                i32.or
                i32.store offset=1054636
                local.get 3
                local.set 4
              end
              local.get 3
              local.get 7
              i32.store offset=8
              local.get 4
              local.get 7
              i32.store offset=12
              local.get 7
              local.get 3
              i32.store offset=12
              local.get 7
              local.get 4
              i32.store offset=8
              br 1 (;@4;)
            end
            local.get 0
            local.get 3
            local.get 2
            i32.add
            call $_ZN8dlmalloc8dlmalloc5Chunk20set_inuse_and_pinuse17h37a663285e2dcd9dE
          end
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
          local.tee 3
          i32.eqz
          br_if 1 (;@2;)
          br 2 (;@1;)
        end
        i32.const 16
        local.get 0
        i32.const 4
        i32.add
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        i32.const -5
        i32.add
        local.get 0
        i32.gt_u
        select
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        local.set 2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      i32.const 0
                      i32.load offset=1054636
                      local.tee 7
                      local.get 2
                      i32.const 3
                      i32.shr_u
                      local.tee 3
                      i32.shr_u
                      local.tee 0
                      i32.const 3
                      i32.and
                      br_if 0 (;@9;)
                      local.get 2
                      i32.const 0
                      i32.load offset=1055036
                      i32.le_u
                      br_if 7 (;@2;)
                      local.get 0
                      br_if 1 (;@8;)
                      i32.const 0
                      i32.load offset=1054640
                      local.tee 0
                      i32.eqz
                      br_if 7 (;@2;)
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc9least_bit17h06921975976171dcE
                      i32.ctz
                      i32.const 2
                      i32.shl
                      i32.const 1054908
                      i32.add
                      i32.load
                      local.tee 4
                      call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
                      call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
                      local.get 2
                      i32.sub
                      local.set 3
                      block  ;; label = @10
                        local.get 4
                        call $_ZN8dlmalloc8dlmalloc9TreeChunk14leftmost_child17hf6286ca5315942e0E
                        local.tee 0
                        i32.eqz
                        br_if 0 (;@10;)
                        loop  ;; label = @11
                          local.get 0
                          call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
                          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
                          local.get 2
                          i32.sub
                          local.tee 7
                          local.get 3
                          local.get 7
                          local.get 3
                          i32.lt_u
                          local.tee 7
                          select
                          local.set 3
                          local.get 0
                          local.get 4
                          local.get 7
                          select
                          local.set 4
                          local.get 0
                          call $_ZN8dlmalloc8dlmalloc9TreeChunk14leftmost_child17hf6286ca5315942e0E
                          local.tee 0
                          br_if 0 (;@11;)
                        end
                      end
                      local.get 4
                      call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
                      local.tee 0
                      local.get 2
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                      local.set 7
                      local.get 4
                      call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hd87001f22d4c88d2E
                      local.get 3
                      i32.const 16
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      i32.lt_u
                      br_if 5 (;@4;)
                      local.get 7
                      call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
                      local.set 7
                      local.get 0
                      local.get 2
                      call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hbc5285bcaa5cf3fdE
                      local.get 7
                      local.get 3
                      call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE
                      i32.const 0
                      i32.load offset=1055036
                      local.tee 4
                      i32.eqz
                      br_if 4 (;@5;)
                      local.get 4
                      i32.const 3
                      i32.shr_u
                      local.tee 8
                      i32.const 3
                      i32.shl
                      i32.const 1054644
                      i32.add
                      local.set 6
                      i32.const 0
                      i32.load offset=1055044
                      local.set 4
                      i32.const 0
                      i32.load offset=1054636
                      local.tee 5
                      i32.const 1
                      local.get 8
                      i32.shl
                      local.tee 8
                      i32.and
                      i32.eqz
                      br_if 2 (;@7;)
                      local.get 6
                      i32.load offset=8
                      local.set 8
                      br 3 (;@6;)
                    end
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 0
                        i32.const -1
                        i32.xor
                        i32.const 1
                        i32.and
                        local.get 3
                        i32.add
                        local.tee 2
                        i32.const 3
                        i32.shl
                        local.tee 4
                        i32.const 1054652
                        i32.add
                        i32.load
                        local.tee 0
                        i32.const 8
                        i32.add
                        i32.load
                        local.tee 3
                        local.get 4
                        i32.const 1054644
                        i32.add
                        local.tee 4
                        i32.eq
                        br_if 0 (;@10;)
                        local.get 3
                        local.get 4
                        i32.store offset=12
                        local.get 4
                        local.get 3
                        i32.store offset=8
                        br 1 (;@9;)
                      end
                      i32.const 0
                      local.get 7
                      i32.const -2
                      local.get 2
                      i32.rotl
                      i32.and
                      i32.store offset=1054636
                    end
                    local.get 0
                    local.get 2
                    i32.const 3
                    i32.shl
                    call $_ZN8dlmalloc8dlmalloc5Chunk20set_inuse_and_pinuse17h37a663285e2dcd9dE
                    local.get 0
                    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                    local.set 3
                    br 7 (;@1;)
                  end
                  block  ;; label = @8
                    block  ;; label = @9
                      i32.const 1
                      local.get 3
                      i32.const 31
                      i32.and
                      local.tee 3
                      i32.shl
                      call $_ZN8dlmalloc8dlmalloc9left_bits17hde52ae0f2ac5b398E
                      local.get 0
                      local.get 3
                      i32.shl
                      i32.and
                      call $_ZN8dlmalloc8dlmalloc9least_bit17h06921975976171dcE
                      i32.ctz
                      local.tee 3
                      i32.const 3
                      i32.shl
                      local.tee 7
                      i32.const 1054652
                      i32.add
                      i32.load
                      local.tee 0
                      i32.const 8
                      i32.add
                      i32.load
                      local.tee 4
                      local.get 7
                      i32.const 1054644
                      i32.add
                      local.tee 7
                      i32.eq
                      br_if 0 (;@9;)
                      local.get 4
                      local.get 7
                      i32.store offset=12
                      local.get 7
                      local.get 4
                      i32.store offset=8
                      br 1 (;@8;)
                    end
                    i32.const 0
                    i32.const 0
                    i32.load offset=1054636
                    i32.const -2
                    local.get 3
                    i32.rotl
                    i32.and
                    i32.store offset=1054636
                  end
                  local.get 0
                  local.get 2
                  call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hbc5285bcaa5cf3fdE
                  local.get 0
                  local.get 2
                  call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                  local.tee 4
                  local.get 3
                  i32.const 3
                  i32.shl
                  local.get 2
                  i32.sub
                  local.tee 7
                  call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE
                  block  ;; label = @8
                    i32.const 0
                    i32.load offset=1055036
                    local.tee 2
                    i32.eqz
                    br_if 0 (;@8;)
                    local.get 2
                    i32.const 3
                    i32.shr_u
                    local.tee 6
                    i32.const 3
                    i32.shl
                    i32.const 1054644
                    i32.add
                    local.set 3
                    i32.const 0
                    i32.load offset=1055044
                    local.set 2
                    block  ;; label = @9
                      block  ;; label = @10
                        i32.const 0
                        i32.load offset=1054636
                        local.tee 8
                        i32.const 1
                        local.get 6
                        i32.shl
                        local.tee 6
                        i32.and
                        i32.eqz
                        br_if 0 (;@10;)
                        local.get 3
                        i32.load offset=8
                        local.set 6
                        br 1 (;@9;)
                      end
                      i32.const 0
                      local.get 8
                      local.get 6
                      i32.or
                      i32.store offset=1054636
                      local.get 3
                      local.set 6
                    end
                    local.get 3
                    local.get 2
                    i32.store offset=8
                    local.get 6
                    local.get 2
                    i32.store offset=12
                    local.get 2
                    local.get 3
                    i32.store offset=12
                    local.get 2
                    local.get 6
                    i32.store offset=8
                  end
                  i32.const 0
                  local.get 4
                  i32.store offset=1055044
                  i32.const 0
                  local.get 7
                  i32.store offset=1055036
                  local.get 0
                  call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                  local.set 3
                  br 6 (;@1;)
                end
                i32.const 0
                local.get 5
                local.get 8
                i32.or
                i32.store offset=1054636
                local.get 6
                local.set 8
              end
              local.get 6
              local.get 4
              i32.store offset=8
              local.get 8
              local.get 4
              i32.store offset=12
              local.get 4
              local.get 6
              i32.store offset=12
              local.get 4
              local.get 8
              i32.store offset=8
            end
            i32.const 0
            local.get 7
            i32.store offset=1055044
            i32.const 0
            local.get 3
            i32.store offset=1055036
            br 1 (;@3;)
          end
          local.get 0
          local.get 3
          local.get 2
          i32.add
          call $_ZN8dlmalloc8dlmalloc5Chunk20set_inuse_and_pinuse17h37a663285e2dcd9dE
        end
        local.get 0
        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
        local.tee 3
        br_if 1 (;@1;)
      end
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        i32.const 0
                        i32.load offset=1055036
                        local.tee 3
                        local.get 2
                        i32.ge_u
                        br_if 0 (;@10;)
                        i32.const 0
                        i32.load offset=1055040
                        local.tee 0
                        local.get 2
                        i32.gt_u
                        br_if 2 (;@8;)
                        local.get 1
                        i32.const 1054636
                        local.get 2
                        call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
                        local.tee 0
                        i32.sub
                        local.get 0
                        i32.const 8
                        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                        i32.add
                        i32.const 20
                        i32.const 8
                        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                        i32.add
                        i32.const 16
                        i32.const 8
                        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                        i32.add
                        i32.const 8
                        i32.add
                        i32.const 65536
                        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                        call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$5alloc17h870b2dd71ff5fddcE
                        local.get 1
                        i32.load
                        local.tee 3
                        br_if 1 (;@9;)
                        i32.const 0
                        local.set 3
                        br 9 (;@1;)
                      end
                      i32.const 0
                      i32.load offset=1055044
                      local.set 0
                      block  ;; label = @10
                        local.get 3
                        local.get 2
                        i32.sub
                        local.tee 3
                        i32.const 16
                        i32.const 8
                        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                        i32.ge_u
                        br_if 0 (;@10;)
                        i32.const 0
                        i32.const 0
                        i32.store offset=1055044
                        i32.const 0
                        i32.load offset=1055036
                        local.set 2
                        i32.const 0
                        i32.const 0
                        i32.store offset=1055036
                        local.get 0
                        local.get 2
                        call $_ZN8dlmalloc8dlmalloc5Chunk20set_inuse_and_pinuse17h37a663285e2dcd9dE
                        local.get 0
                        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                        local.set 3
                        br 9 (;@1;)
                      end
                      local.get 0
                      local.get 2
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                      local.set 4
                      i32.const 0
                      local.get 3
                      i32.store offset=1055036
                      i32.const 0
                      local.get 4
                      i32.store offset=1055044
                      local.get 4
                      local.get 3
                      call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE
                      local.get 0
                      local.get 2
                      call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hbc5285bcaa5cf3fdE
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                      local.set 3
                      br 8 (;@1;)
                    end
                    local.get 1
                    i32.load offset=8
                    local.set 5
                    i32.const 0
                    i32.const 0
                    i32.load offset=1055052
                    local.get 1
                    i32.load offset=4
                    local.tee 6
                    i32.add
                    local.tee 0
                    i32.store offset=1055052
                    i32.const 0
                    i32.const 0
                    i32.load offset=1055056
                    local.tee 4
                    local.get 0
                    local.get 4
                    local.get 0
                    i32.gt_u
                    select
                    i32.store offset=1055056
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          i32.const 0
                          i32.load offset=1055048
                          i32.eqz
                          br_if 0 (;@11;)
                          i32.const 1055060
                          local.set 0
                          loop  ;; label = @12
                            local.get 3
                            local.get 0
                            call $_ZN8dlmalloc8dlmalloc7Segment3top17h1783faaf96129d8aE
                            i32.eq
                            br_if 2 (;@10;)
                            local.get 0
                            i32.load offset=8
                            local.tee 0
                            br_if 0 (;@12;)
                            br 3 (;@9;)
                          end
                        end
                        i32.const 0
                        i32.load offset=1055080
                        local.tee 0
                        i32.eqz
                        br_if 3 (;@7;)
                        local.get 3
                        local.get 0
                        i32.lt_u
                        br_if 3 (;@7;)
                        br 7 (;@3;)
                      end
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc7Segment9is_extern17h512142d4de1be17dE
                      br_if 0 (;@9;)
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc7Segment9sys_flags17h17058350dbbb4cc7E
                      local.get 5
                      i32.ne
                      br_if 0 (;@9;)
                      local.get 0
                      i32.const 0
                      i32.load offset=1055048
                      call $_ZN8dlmalloc8dlmalloc7Segment5holds17h3b728386e6fb8f6cE
                      br_if 3 (;@6;)
                    end
                    i32.const 0
                    i32.const 0
                    i32.load offset=1055080
                    local.tee 0
                    local.get 3
                    local.get 3
                    local.get 0
                    i32.gt_u
                    select
                    i32.store offset=1055080
                    local.get 3
                    local.get 6
                    i32.add
                    local.set 4
                    i32.const 1055060
                    local.set 0
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          loop  ;; label = @12
                            local.get 0
                            i32.load
                            local.get 4
                            i32.eq
                            br_if 1 (;@11;)
                            local.get 0
                            i32.load offset=8
                            local.tee 0
                            br_if 0 (;@12;)
                            br 2 (;@10;)
                          end
                        end
                        local.get 0
                        call $_ZN8dlmalloc8dlmalloc7Segment9is_extern17h512142d4de1be17dE
                        br_if 0 (;@10;)
                        local.get 0
                        call $_ZN8dlmalloc8dlmalloc7Segment9sys_flags17h17058350dbbb4cc7E
                        local.get 5
                        i32.eq
                        br_if 1 (;@9;)
                      end
                      i32.const 0
                      i32.load offset=1055048
                      local.set 4
                      i32.const 1055060
                      local.set 0
                      block  ;; label = @10
                        loop  ;; label = @11
                          block  ;; label = @12
                            local.get 0
                            i32.load
                            local.get 4
                            i32.gt_u
                            br_if 0 (;@12;)
                            local.get 0
                            call $_ZN8dlmalloc8dlmalloc7Segment3top17h1783faaf96129d8aE
                            local.get 4
                            i32.gt_u
                            br_if 2 (;@10;)
                          end
                          local.get 0
                          i32.load offset=8
                          local.tee 0
                          br_if 0 (;@11;)
                        end
                        i32.const 0
                        local.set 0
                      end
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc7Segment3top17h1783faaf96129d8aE
                      local.tee 7
                      i32.const 20
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      local.tee 9
                      i32.sub
                      i32.const -23
                      i32.add
                      local.set 0
                      local.get 4
                      local.get 0
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                      local.tee 8
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      local.get 8
                      i32.sub
                      i32.add
                      local.tee 0
                      local.get 0
                      local.get 4
                      i32.const 16
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      i32.add
                      i32.lt_u
                      select
                      local.tee 8
                      call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                      local.set 10
                      local.get 8
                      local.get 9
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                      local.set 0
                      call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
                      local.tee 11
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      local.set 12
                      i32.const 20
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      local.set 13
                      i32.const 16
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      local.set 14
                      i32.const 0
                      local.get 3
                      local.get 3
                      call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                      local.tee 15
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      local.get 15
                      i32.sub
                      local.tee 16
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                      local.tee 15
                      i32.store offset=1055048
                      i32.const 0
                      local.get 11
                      local.get 6
                      i32.add
                      local.get 14
                      local.get 12
                      local.get 13
                      i32.add
                      i32.add
                      local.get 16
                      i32.add
                      i32.sub
                      local.tee 11
                      i32.store offset=1055040
                      local.get 15
                      local.get 11
                      i32.const 1
                      i32.or
                      i32.store offset=4
                      call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
                      local.tee 12
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      local.set 13
                      i32.const 20
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      local.set 14
                      i32.const 16
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      local.set 16
                      local.get 15
                      local.get 11
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                      local.get 16
                      local.get 14
                      local.get 13
                      local.get 12
                      i32.sub
                      i32.add
                      i32.add
                      i32.store offset=4
                      i32.const 0
                      i32.const 2097152
                      i32.store offset=1055076
                      local.get 8
                      local.get 9
                      call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hbc5285bcaa5cf3fdE
                      i32.const 0
                      i64.load offset=1055060 align=4
                      local.set 17
                      local.get 10
                      i32.const 8
                      i32.add
                      i32.const 0
                      i64.load offset=1055068 align=4
                      i64.store align=4
                      local.get 10
                      local.get 17
                      i64.store align=4
                      i32.const 0
                      local.get 5
                      i32.store offset=1055072
                      i32.const 0
                      local.get 6
                      i32.store offset=1055064
                      i32.const 0
                      local.get 3
                      i32.store offset=1055060
                      i32.const 0
                      local.get 10
                      i32.store offset=1055068
                      loop  ;; label = @10
                        local.get 0
                        i32.const 4
                        call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                        local.set 3
                        local.get 0
                        call $_ZN8dlmalloc8dlmalloc5Chunk14fencepost_head17h49e62dea37f7cefeE
                        i32.store offset=4
                        local.get 3
                        local.set 0
                        local.get 7
                        local.get 3
                        i32.const 4
                        i32.add
                        i32.gt_u
                        br_if 0 (;@10;)
                      end
                      local.get 8
                      local.get 4
                      i32.eq
                      br_if 7 (;@2;)
                      local.get 8
                      local.get 4
                      i32.sub
                      local.set 0
                      local.get 4
                      local.get 0
                      local.get 4
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                      call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17h774ca7c76a5c5aabE
                      block  ;; label = @10
                        local.get 0
                        i32.const 256
                        i32.lt_u
                        br_if 0 (;@10;)
                        local.get 4
                        local.get 0
                        call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17h109ba5955a0705b3E
                        br 8 (;@2;)
                      end
                      local.get 0
                      i32.const 3
                      i32.shr_u
                      local.tee 3
                      i32.const 3
                      i32.shl
                      i32.const 1054644
                      i32.add
                      local.set 0
                      block  ;; label = @10
                        block  ;; label = @11
                          i32.const 0
                          i32.load offset=1054636
                          local.tee 7
                          i32.const 1
                          local.get 3
                          i32.shl
                          local.tee 3
                          i32.and
                          i32.eqz
                          br_if 0 (;@11;)
                          local.get 0
                          i32.load offset=8
                          local.set 3
                          br 1 (;@10;)
                        end
                        i32.const 0
                        local.get 7
                        local.get 3
                        i32.or
                        i32.store offset=1054636
                        local.get 0
                        local.set 3
                      end
                      local.get 0
                      local.get 4
                      i32.store offset=8
                      local.get 3
                      local.get 4
                      i32.store offset=12
                      local.get 4
                      local.get 0
                      i32.store offset=12
                      local.get 4
                      local.get 3
                      i32.store offset=8
                      br 7 (;@2;)
                    end
                    local.get 0
                    i32.load
                    local.set 7
                    local.get 0
                    local.get 3
                    i32.store
                    local.get 0
                    local.get 0
                    i32.load offset=4
                    local.get 6
                    i32.add
                    i32.store offset=4
                    local.get 3
                    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                    local.tee 0
                    i32.const 8
                    call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                    local.set 4
                    local.get 7
                    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                    local.tee 6
                    i32.const 8
                    call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                    local.set 8
                    local.get 3
                    local.get 4
                    local.get 0
                    i32.sub
                    i32.add
                    local.tee 3
                    local.get 2
                    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                    local.set 4
                    local.get 3
                    local.get 2
                    call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hbc5285bcaa5cf3fdE
                    local.get 7
                    local.get 8
                    local.get 6
                    i32.sub
                    i32.add
                    local.tee 0
                    local.get 2
                    local.get 3
                    i32.add
                    i32.sub
                    local.set 2
                    block  ;; label = @9
                      i32.const 0
                      i32.load offset=1055048
                      local.get 0
                      i32.eq
                      br_if 0 (;@9;)
                      i32.const 0
                      i32.load offset=1055044
                      local.get 0
                      i32.eq
                      br_if 4 (;@5;)
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc5Chunk5inuse17hba1a84bba98ecaa7E
                      br_if 5 (;@4;)
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 0
                          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
                          local.tee 7
                          i32.const 256
                          i32.lt_u
                          br_if 0 (;@11;)
                          local.get 0
                          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hd87001f22d4c88d2E
                          br 1 (;@10;)
                        end
                        block  ;; label = @11
                          local.get 0
                          i32.const 12
                          i32.add
                          i32.load
                          local.tee 6
                          local.get 0
                          i32.const 8
                          i32.add
                          i32.load
                          local.tee 8
                          i32.eq
                          br_if 0 (;@11;)
                          local.get 8
                          local.get 6
                          i32.store offset=12
                          local.get 6
                          local.get 8
                          i32.store offset=8
                          br 1 (;@10;)
                        end
                        i32.const 0
                        i32.const 0
                        i32.load offset=1054636
                        i32.const -2
                        local.get 7
                        i32.const 3
                        i32.shr_u
                        i32.rotl
                        i32.and
                        i32.store offset=1054636
                      end
                      local.get 7
                      local.get 2
                      i32.add
                      local.set 2
                      local.get 0
                      local.get 7
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                      local.set 0
                      br 5 (;@4;)
                    end
                    i32.const 0
                    local.get 4
                    i32.store offset=1055048
                    i32.const 0
                    i32.const 0
                    i32.load offset=1055040
                    local.get 2
                    i32.add
                    local.tee 0
                    i32.store offset=1055040
                    local.get 4
                    local.get 0
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    local.get 3
                    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                    local.set 3
                    br 7 (;@1;)
                  end
                  i32.const 0
                  local.get 0
                  local.get 2
                  i32.sub
                  local.tee 3
                  i32.store offset=1055040
                  i32.const 0
                  i32.const 0
                  i32.load offset=1055048
                  local.tee 0
                  local.get 2
                  call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                  local.tee 4
                  i32.store offset=1055048
                  local.get 4
                  local.get 3
                  i32.const 1
                  i32.or
                  i32.store offset=4
                  local.get 0
                  local.get 2
                  call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hbc5285bcaa5cf3fdE
                  local.get 0
                  call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
                  local.set 3
                  br 6 (;@1;)
                end
                i32.const 0
                local.get 3
                i32.store offset=1055080
                br 3 (;@3;)
              end
              local.get 0
              local.get 0
              i32.load offset=4
              local.get 6
              i32.add
              i32.store offset=4
              i32.const 0
              i32.load offset=1055040
              local.set 3
              i32.const 0
              i32.load offset=1055048
              local.set 0
              local.get 0
              local.get 0
              call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
              local.tee 4
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
              local.get 4
              i32.sub
              local.tee 4
              call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
              local.set 0
              i32.const 0
              local.get 3
              local.get 6
              i32.add
              local.get 4
              i32.sub
              local.tee 3
              i32.store offset=1055040
              i32.const 0
              local.get 0
              i32.store offset=1055048
              local.get 0
              local.get 3
              i32.const 1
              i32.or
              i32.store offset=4
              call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
              local.tee 4
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
              local.set 7
              i32.const 20
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
              local.set 6
              i32.const 16
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
              local.set 8
              local.get 0
              local.get 3
              call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
              local.get 8
              local.get 6
              local.get 7
              local.get 4
              i32.sub
              i32.add
              i32.add
              i32.store offset=4
              i32.const 0
              i32.const 2097152
              i32.store offset=1055076
              br 3 (;@2;)
            end
            i32.const 0
            local.get 4
            i32.store offset=1055044
            i32.const 0
            i32.const 0
            i32.load offset=1055036
            local.get 2
            i32.add
            local.tee 0
            i32.store offset=1055036
            local.get 4
            local.get 0
            call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE
            local.get 3
            call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
            local.set 3
            br 3 (;@1;)
          end
          local.get 4
          local.get 2
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17h774ca7c76a5c5aabE
          block  ;; label = @4
            local.get 2
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 4
            local.get 2
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17h109ba5955a0705b3E
            local.get 3
            call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
            local.set 3
            br 3 (;@1;)
          end
          local.get 2
          i32.const 3
          i32.shr_u
          local.tee 2
          i32.const 3
          i32.shl
          i32.const 1054644
          i32.add
          local.set 0
          block  ;; label = @4
            block  ;; label = @5
              i32.const 0
              i32.load offset=1054636
              local.tee 7
              i32.const 1
              local.get 2
              i32.shl
              local.tee 2
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 0
              i32.load offset=8
              local.set 2
              br 1 (;@4;)
            end
            i32.const 0
            local.get 7
            local.get 2
            i32.or
            i32.store offset=1054636
            local.get 0
            local.set 2
          end
          local.get 0
          local.get 4
          i32.store offset=8
          local.get 2
          local.get 4
          i32.store offset=12
          local.get 4
          local.get 0
          i32.store offset=12
          local.get 4
          local.get 2
          i32.store offset=8
          local.get 3
          call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
          local.set 3
          br 2 (;@1;)
        end
        i32.const 0
        i32.const 4095
        i32.store offset=1055084
        i32.const 0
        local.get 5
        i32.store offset=1055072
        i32.const 0
        local.get 6
        i32.store offset=1055064
        i32.const 0
        local.get 3
        i32.store offset=1055060
        i32.const 0
        i32.const 1054644
        i32.store offset=1054656
        i32.const 0
        i32.const 1054652
        i32.store offset=1054664
        i32.const 0
        i32.const 1054644
        i32.store offset=1054652
        i32.const 0
        i32.const 1054660
        i32.store offset=1054672
        i32.const 0
        i32.const 1054652
        i32.store offset=1054660
        i32.const 0
        i32.const 1054668
        i32.store offset=1054680
        i32.const 0
        i32.const 1054660
        i32.store offset=1054668
        i32.const 0
        i32.const 1054676
        i32.store offset=1054688
        i32.const 0
        i32.const 1054668
        i32.store offset=1054676
        i32.const 0
        i32.const 1054684
        i32.store offset=1054696
        i32.const 0
        i32.const 1054676
        i32.store offset=1054684
        i32.const 0
        i32.const 1054692
        i32.store offset=1054704
        i32.const 0
        i32.const 1054684
        i32.store offset=1054692
        i32.const 0
        i32.const 1054700
        i32.store offset=1054712
        i32.const 0
        i32.const 1054692
        i32.store offset=1054700
        i32.const 0
        i32.const 1054708
        i32.store offset=1054720
        i32.const 0
        i32.const 1054700
        i32.store offset=1054708
        i32.const 0
        i32.const 1054708
        i32.store offset=1054716
        i32.const 0
        i32.const 1054716
        i32.store offset=1054728
        i32.const 0
        i32.const 1054716
        i32.store offset=1054724
        i32.const 0
        i32.const 1054724
        i32.store offset=1054736
        i32.const 0
        i32.const 1054724
        i32.store offset=1054732
        i32.const 0
        i32.const 1054732
        i32.store offset=1054744
        i32.const 0
        i32.const 1054732
        i32.store offset=1054740
        i32.const 0
        i32.const 1054740
        i32.store offset=1054752
        i32.const 0
        i32.const 1054740
        i32.store offset=1054748
        i32.const 0
        i32.const 1054748
        i32.store offset=1054760
        i32.const 0
        i32.const 1054748
        i32.store offset=1054756
        i32.const 0
        i32.const 1054756
        i32.store offset=1054768
        i32.const 0
        i32.const 1054756
        i32.store offset=1054764
        i32.const 0
        i32.const 1054764
        i32.store offset=1054776
        i32.const 0
        i32.const 1054764
        i32.store offset=1054772
        i32.const 0
        i32.const 1054772
        i32.store offset=1054784
        i32.const 0
        i32.const 1054780
        i32.store offset=1054792
        i32.const 0
        i32.const 1054772
        i32.store offset=1054780
        i32.const 0
        i32.const 1054788
        i32.store offset=1054800
        i32.const 0
        i32.const 1054780
        i32.store offset=1054788
        i32.const 0
        i32.const 1054796
        i32.store offset=1054808
        i32.const 0
        i32.const 1054788
        i32.store offset=1054796
        i32.const 0
        i32.const 1054804
        i32.store offset=1054816
        i32.const 0
        i32.const 1054796
        i32.store offset=1054804
        i32.const 0
        i32.const 1054812
        i32.store offset=1054824
        i32.const 0
        i32.const 1054804
        i32.store offset=1054812
        i32.const 0
        i32.const 1054820
        i32.store offset=1054832
        i32.const 0
        i32.const 1054812
        i32.store offset=1054820
        i32.const 0
        i32.const 1054828
        i32.store offset=1054840
        i32.const 0
        i32.const 1054820
        i32.store offset=1054828
        i32.const 0
        i32.const 1054836
        i32.store offset=1054848
        i32.const 0
        i32.const 1054828
        i32.store offset=1054836
        i32.const 0
        i32.const 1054844
        i32.store offset=1054856
        i32.const 0
        i32.const 1054836
        i32.store offset=1054844
        i32.const 0
        i32.const 1054852
        i32.store offset=1054864
        i32.const 0
        i32.const 1054844
        i32.store offset=1054852
        i32.const 0
        i32.const 1054860
        i32.store offset=1054872
        i32.const 0
        i32.const 1054852
        i32.store offset=1054860
        i32.const 0
        i32.const 1054868
        i32.store offset=1054880
        i32.const 0
        i32.const 1054860
        i32.store offset=1054868
        i32.const 0
        i32.const 1054876
        i32.store offset=1054888
        i32.const 0
        i32.const 1054868
        i32.store offset=1054876
        i32.const 0
        i32.const 1054884
        i32.store offset=1054896
        i32.const 0
        i32.const 1054876
        i32.store offset=1054884
        i32.const 0
        i32.const 1054892
        i32.store offset=1054904
        i32.const 0
        i32.const 1054884
        i32.store offset=1054892
        i32.const 0
        i32.const 1054892
        i32.store offset=1054900
        call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
        local.tee 4
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        local.set 7
        i32.const 20
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        local.set 8
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        local.set 5
        i32.const 0
        local.get 3
        local.get 3
        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
        local.tee 0
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        local.get 0
        i32.sub
        local.tee 10
        call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
        local.tee 0
        i32.store offset=1055048
        i32.const 0
        local.get 4
        local.get 6
        i32.add
        local.get 5
        local.get 7
        local.get 8
        i32.add
        i32.add
        local.get 10
        i32.add
        i32.sub
        local.tee 3
        i32.store offset=1055040
        local.get 0
        local.get 3
        i32.const 1
        i32.or
        i32.store offset=4
        call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
        local.tee 4
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        local.set 7
        i32.const 20
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        local.set 6
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
        local.set 8
        local.get 0
        local.get 3
        call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
        local.get 8
        local.get 6
        local.get 7
        local.get 4
        i32.sub
        i32.add
        i32.add
        i32.store offset=4
        i32.const 0
        i32.const 2097152
        i32.store offset=1055076
      end
      i32.const 0
      local.set 3
      i32.const 0
      i32.load offset=1055040
      local.tee 0
      local.get 2
      i32.le_u
      br_if 0 (;@1;)
      i32.const 0
      local.get 0
      local.get 2
      i32.sub
      local.tee 3
      i32.store offset=1055040
      i32.const 0
      i32.const 0
      i32.load offset=1055048
      local.tee 0
      local.get 2
      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
      local.tee 4
      i32.store offset=1055048
      local.get 4
      local.get 3
      i32.const 1
      i32.or
      i32.store offset=4
      local.get 0
      local.get 2
      call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hbc5285bcaa5cf3fdE
      local.get 0
      call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
      local.set 3
    end
    local.get 1
    i32.const 16
    i32.add
    global.set $__stack_pointer
    local.get 3)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$13dispose_chunk17hd7d0611236d858ddE (type 3) (param i32 i32)
    (local i32 i32 i32 i32)
    local.get 0
    local.get 1
    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
    local.set 2
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk6pinuse17h7e78a342f64f614cE
          br_if 0 (;@3;)
          local.get 0
          i32.load
          local.set 3
          block  ;; label = @4
            block  ;; label = @5
              local.get 0
              call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17h374540cc5223b826E
              br_if 0 (;@5;)
              local.get 3
              local.get 1
              i32.add
              local.set 1
              local.get 0
              local.get 3
              call $_ZN8dlmalloc8dlmalloc5Chunk12minus_offset17hc64997bcfff16288E
              local.tee 0
              i32.const 0
              i32.load offset=1055044
              i32.ne
              br_if 1 (;@4;)
              local.get 2
              i32.load offset=4
              i32.const 3
              i32.and
              i32.const 3
              i32.ne
              br_if 2 (;@3;)
              i32.const 0
              local.get 1
              i32.store offset=1055036
              local.get 0
              local.get 1
              local.get 2
              call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17h774ca7c76a5c5aabE
              return
            end
            i32.const 1054636
            local.get 0
            local.get 3
            i32.sub
            local.get 3
            local.get 1
            i32.add
            i32.const 16
            i32.add
            local.tee 0
            call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$4free17hdf442810a3c47608E
            i32.eqz
            br_if 2 (;@2;)
            i32.const 0
            i32.const 0
            i32.load offset=1055052
            local.get 0
            i32.sub
            i32.store offset=1055052
            return
          end
          block  ;; label = @4
            local.get 3
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 0
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hd87001f22d4c88d2E
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 0
            i32.const 12
            i32.add
            i32.load
            local.tee 4
            local.get 0
            i32.const 8
            i32.add
            i32.load
            local.tee 5
            i32.eq
            br_if 0 (;@4;)
            local.get 5
            local.get 4
            i32.store offset=12
            local.get 4
            local.get 5
            i32.store offset=8
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.load offset=1054636
          i32.const -2
          local.get 3
          i32.const 3
          i32.shr_u
          i32.rotl
          i32.and
          i32.store offset=1054636
        end
        block  ;; label = @3
          local.get 2
          call $_ZN8dlmalloc8dlmalloc5Chunk6cinuse17h7881952257593bc0E
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          local.get 2
          call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17h774ca7c76a5c5aabE
          br 2 (;@1;)
        end
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 0
            i32.load offset=1055048
            i32.eq
            br_if 0 (;@4;)
            local.get 2
            i32.const 0
            i32.load offset=1055044
            i32.ne
            br_if 1 (;@3;)
            i32.const 0
            local.get 0
            i32.store offset=1055044
            i32.const 0
            i32.const 0
            i32.load offset=1055036
            local.get 1
            i32.add
            local.tee 1
            i32.store offset=1055036
            local.get 0
            local.get 1
            call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE
            return
          end
          i32.const 0
          local.get 0
          i32.store offset=1055048
          i32.const 0
          i32.const 0
          i32.load offset=1055040
          local.get 1
          i32.add
          local.tee 1
          i32.store offset=1055040
          local.get 0
          local.get 1
          i32.const 1
          i32.or
          i32.store offset=4
          local.get 0
          i32.const 0
          i32.load offset=1055044
          i32.ne
          br_if 1 (;@2;)
          i32.const 0
          i32.const 0
          i32.store offset=1055036
          i32.const 0
          i32.const 0
          i32.store offset=1055044
          return
        end
        local.get 2
        call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
        local.tee 3
        local.get 1
        i32.add
        local.set 1
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 2
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hd87001f22d4c88d2E
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 2
            i32.const 12
            i32.add
            i32.load
            local.tee 4
            local.get 2
            i32.const 8
            i32.add
            i32.load
            local.tee 2
            i32.eq
            br_if 0 (;@4;)
            local.get 2
            local.get 4
            i32.store offset=12
            local.get 4
            local.get 2
            i32.store offset=8
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.load offset=1054636
          i32.const -2
          local.get 3
          i32.const 3
          i32.shr_u
          i32.rotl
          i32.and
          i32.store offset=1054636
        end
        local.get 0
        local.get 1
        call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE
        local.get 0
        i32.const 0
        i32.load offset=1055044
        i32.ne
        br_if 1 (;@1;)
        i32.const 0
        local.get 1
        i32.store offset=1055036
      end
      return
    end
    block  ;; label = @1
      local.get 1
      i32.const 256
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17h109ba5955a0705b3E
      return
    end
    local.get 1
    i32.const 3
    i32.shr_u
    local.tee 2
    i32.const 3
    i32.shl
    i32.const 1054644
    i32.add
    local.set 1
    block  ;; label = @1
      block  ;; label = @2
        i32.const 0
        i32.load offset=1054636
        local.tee 3
        i32.const 1
        local.get 2
        i32.shl
        local.tee 2
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load offset=8
        local.set 2
        br 1 (;@1;)
      end
      i32.const 0
      local.get 3
      local.get 2
      i32.or
      i32.store offset=1054636
      local.get 1
      local.set 2
    end
    local.get 1
    local.get 0
    i32.store offset=8
    local.get 2
    local.get 0
    i32.store offset=12
    local.get 0
    local.get 1
    i32.store offset=12
    local.get 0
    local.get 2
    i32.store offset=8)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hd87001f22d4c88d2E (type 0) (param i32)
    (local i32 i32 i32 i32 i32)
    local.get 0
    i32.load offset=24
    local.set 1
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          call $_ZN8dlmalloc8dlmalloc9TreeChunk4next17h9c173fcea4183d8fE
          local.get 0
          i32.ne
          br_if 0 (;@3;)
          local.get 0
          i32.const 20
          i32.const 16
          local.get 0
          i32.const 20
          i32.add
          local.tee 2
          i32.load
          local.tee 3
          select
          i32.add
          i32.load
          local.tee 4
          br_if 1 (;@2;)
          i32.const 0
          local.set 3
          br 2 (;@1;)
        end
        local.get 0
        call $_ZN8dlmalloc8dlmalloc9TreeChunk4prev17hde4bfcd1cf1f00a3E
        local.tee 4
        local.get 0
        call $_ZN8dlmalloc8dlmalloc9TreeChunk4next17h9c173fcea4183d8fE
        local.tee 3
        call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
        i32.store offset=12
        local.get 3
        local.get 4
        call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
        i32.store offset=8
        br 1 (;@1;)
      end
      local.get 2
      local.get 0
      i32.const 16
      i32.add
      local.get 3
      select
      local.set 2
      loop  ;; label = @2
        local.get 2
        local.set 5
        block  ;; label = @3
          local.get 4
          local.tee 3
          i32.const 20
          i32.add
          local.tee 2
          i32.load
          local.tee 4
          br_if 0 (;@3;)
          local.get 3
          i32.const 16
          i32.add
          local.set 2
          local.get 3
          i32.load offset=16
          local.set 4
        end
        local.get 4
        br_if 0 (;@2;)
      end
      local.get 5
      i32.const 0
      i32.store
    end
    block  ;; label = @1
      local.get 1
      i32.eqz
      br_if 0 (;@1;)
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          i32.load offset=28
          i32.const 2
          i32.shl
          i32.const 1054908
          i32.add
          local.tee 4
          i32.load
          local.get 0
          i32.eq
          br_if 0 (;@3;)
          local.get 1
          i32.const 16
          i32.const 20
          local.get 1
          i32.load offset=16
          local.get 0
          i32.eq
          select
          i32.add
          local.get 3
          i32.store
          local.get 3
          br_if 1 (;@2;)
          br 2 (;@1;)
        end
        local.get 4
        local.get 3
        i32.store
        local.get 3
        br_if 0 (;@2;)
        i32.const 0
        i32.const 0
        i32.load offset=1054640
        i32.const -2
        local.get 0
        i32.load offset=28
        i32.rotl
        i32.and
        i32.store offset=1054640
        return
      end
      local.get 3
      local.get 1
      i32.store offset=24
      block  ;; label = @2
        local.get 0
        i32.load offset=16
        local.tee 4
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 4
        i32.store offset=16
        local.get 4
        local.get 3
        i32.store offset=24
      end
      local.get 0
      i32.const 20
      i32.add
      i32.load
      local.tee 4
      i32.eqz
      br_if 0 (;@1;)
      local.get 3
      i32.const 20
      i32.add
      local.get 4
      i32.store
      local.get 4
      local.get 3
      i32.store offset=24
      return
    end)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17h109ba5955a0705b3E (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32)
    i32.const 0
    local.set 2
    block  ;; label = @1
      local.get 1
      i32.const 256
      i32.lt_u
      br_if 0 (;@1;)
      i32.const 31
      local.set 2
      local.get 1
      i32.const 16777215
      i32.gt_u
      br_if 0 (;@1;)
      local.get 1
      i32.const 6
      local.get 1
      i32.const 8
      i32.shr_u
      i32.clz
      local.tee 2
      i32.sub
      i32.shr_u
      i32.const 1
      i32.and
      local.get 2
      i32.const 1
      i32.shl
      i32.sub
      i32.const 62
      i32.add
      local.set 2
    end
    local.get 0
    i64.const 0
    i64.store offset=16 align=4
    local.get 0
    local.get 2
    i32.store offset=28
    local.get 2
    i32.const 2
    i32.shl
    i32.const 1054908
    i32.add
    local.set 3
    local.get 0
    call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              i32.const 0
              i32.load offset=1054640
              local.tee 5
              i32.const 1
              local.get 2
              i32.shl
              local.tee 6
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 3
              i32.load
              local.set 5
              local.get 2
              call $_ZN8dlmalloc8dlmalloc24leftshift_for_tree_index17he003bc47d801254fE
              local.set 2
              local.get 5
              call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
              call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
              local.get 1
              i32.ne
              br_if 1 (;@4;)
              local.get 5
              local.set 2
              br 2 (;@3;)
            end
            i32.const 0
            local.get 5
            local.get 6
            i32.or
            i32.store offset=1054640
            local.get 3
            local.get 0
            i32.store
            local.get 0
            local.get 3
            i32.store offset=24
            br 3 (;@1;)
          end
          local.get 1
          local.get 2
          i32.shl
          local.set 3
          loop  ;; label = @4
            local.get 5
            local.get 3
            i32.const 29
            i32.shr_u
            i32.const 4
            i32.and
            i32.add
            i32.const 16
            i32.add
            local.tee 6
            i32.load
            local.tee 2
            i32.eqz
            br_if 2 (;@2;)
            local.get 3
            i32.const 1
            i32.shl
            local.set 3
            local.get 2
            local.set 5
            local.get 2
            call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
            call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
            local.get 1
            i32.ne
            br_if 0 (;@4;)
          end
        end
        local.get 2
        call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E
        local.tee 2
        i32.load offset=8
        local.tee 3
        local.get 4
        i32.store offset=12
        local.get 2
        local.get 4
        i32.store offset=8
        local.get 4
        local.get 2
        i32.store offset=12
        local.get 4
        local.get 3
        i32.store offset=8
        local.get 0
        i32.const 0
        i32.store offset=24
        return
      end
      local.get 6
      local.get 0
      i32.store
      local.get 0
      local.get 5
      i32.store offset=24
    end
    local.get 4
    local.get 4
    i32.store offset=8
    local.get 4
    local.get 4
    i32.store offset=12)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$23release_unused_segments17h26906ec864695c40E (type 12) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      i32.const 0
      i32.load offset=1055068
      local.tee 0
      br_if 0 (;@1;)
      i32.const 0
      i32.const 4095
      i32.store offset=1055084
      i32.const 0
      return
    end
    i32.const 1055060
    local.set 1
    i32.const 0
    local.set 2
    i32.const 0
    local.set 3
    loop  ;; label = @1
      local.get 0
      local.tee 4
      i32.load offset=8
      local.set 0
      local.get 4
      i32.load offset=4
      local.set 5
      local.get 4
      i32.load
      local.set 6
      block  ;; label = @2
        block  ;; label = @3
          i32.const 1054636
          local.get 4
          i32.const 12
          i32.add
          i32.load
          i32.const 1
          i32.shr_u
          call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$16can_release_part17h7e547857d2f98700E
          i32.eqz
          br_if 0 (;@3;)
          local.get 4
          call $_ZN8dlmalloc8dlmalloc7Segment9is_extern17h512142d4de1be17dE
          br_if 0 (;@3;)
          local.get 6
          local.get 6
          call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
          local.tee 7
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.get 7
          i32.sub
          i32.add
          local.tee 7
          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
          local.set 8
          call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
          local.tee 9
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.set 10
          i32.const 20
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.set 11
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.set 12
          local.get 7
          call $_ZN8dlmalloc8dlmalloc5Chunk5inuse17hba1a84bba98ecaa7E
          br_if 0 (;@3;)
          local.get 7
          local.get 8
          i32.add
          local.get 6
          local.get 9
          local.get 5
          i32.add
          local.get 10
          local.get 11
          i32.add
          local.get 12
          i32.add
          i32.sub
          i32.add
          i32.lt_u
          br_if 0 (;@3;)
          block  ;; label = @4
            block  ;; label = @5
              i32.const 0
              i32.load offset=1055044
              local.get 7
              i32.eq
              br_if 0 (;@5;)
              local.get 7
              call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hd87001f22d4c88d2E
              br 1 (;@4;)
            end
            i32.const 0
            i32.const 0
            i32.store offset=1055036
            i32.const 0
            i32.const 0
            i32.store offset=1055044
          end
          block  ;; label = @4
            i32.const 1054636
            local.get 6
            local.get 5
            call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$4free17hdf442810a3c47608E
            br_if 0 (;@4;)
            local.get 7
            local.get 8
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17h109ba5955a0705b3E
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.load offset=1055052
          local.get 5
          i32.sub
          i32.store offset=1055052
          local.get 1
          local.get 0
          i32.store offset=8
          local.get 5
          local.get 2
          i32.add
          local.set 2
          br 1 (;@2;)
        end
        local.get 4
        local.set 1
      end
      local.get 3
      i32.const 1
      i32.add
      local.set 3
      local.get 0
      br_if 0 (;@1;)
    end
    i32.const 0
    local.get 3
    i32.const 4095
    local.get 3
    i32.const 4095
    i32.gt_u
    select
    i32.store offset=1055084
    local.get 2)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$4free17h1e7a99a832191904E (type 0) (param i32)
    (local i32 i32 i32 i32 i32 i32)
    local.get 0
    call $_ZN8dlmalloc8dlmalloc5Chunk8from_mem17h7acff0b43825b6c7E
    local.set 0
    local.get 0
    local.get 0
    call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
    local.tee 1
    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
    local.set 2
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk6pinuse17h7e78a342f64f614cE
          br_if 0 (;@3;)
          local.get 0
          i32.load
          local.set 3
          block  ;; label = @4
            block  ;; label = @5
              local.get 0
              call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17h374540cc5223b826E
              br_if 0 (;@5;)
              local.get 3
              local.get 1
              i32.add
              local.set 1
              local.get 0
              local.get 3
              call $_ZN8dlmalloc8dlmalloc5Chunk12minus_offset17hc64997bcfff16288E
              local.tee 0
              i32.const 0
              i32.load offset=1055044
              i32.ne
              br_if 1 (;@4;)
              local.get 2
              i32.load offset=4
              i32.const 3
              i32.and
              i32.const 3
              i32.ne
              br_if 2 (;@3;)
              i32.const 0
              local.get 1
              i32.store offset=1055036
              local.get 0
              local.get 1
              local.get 2
              call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17h774ca7c76a5c5aabE
              return
            end
            i32.const 1054636
            local.get 0
            local.get 3
            i32.sub
            local.get 3
            local.get 1
            i32.add
            i32.const 16
            i32.add
            local.tee 0
            call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$4free17hdf442810a3c47608E
            i32.eqz
            br_if 2 (;@2;)
            i32.const 0
            i32.const 0
            i32.load offset=1055052
            local.get 0
            i32.sub
            i32.store offset=1055052
            return
          end
          block  ;; label = @4
            local.get 3
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 0
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hd87001f22d4c88d2E
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 0
            i32.const 12
            i32.add
            i32.load
            local.tee 4
            local.get 0
            i32.const 8
            i32.add
            i32.load
            local.tee 5
            i32.eq
            br_if 0 (;@4;)
            local.get 5
            local.get 4
            i32.store offset=12
            local.get 4
            local.get 5
            i32.store offset=8
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.load offset=1054636
          i32.const -2
          local.get 3
          i32.const 3
          i32.shr_u
          i32.rotl
          i32.and
          i32.store offset=1054636
        end
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            call $_ZN8dlmalloc8dlmalloc5Chunk6cinuse17h7881952257593bc0E
            i32.eqz
            br_if 0 (;@4;)
            local.get 0
            local.get 1
            local.get 2
            call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17h774ca7c76a5c5aabE
            br 1 (;@3;)
          end
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 2
                  i32.const 0
                  i32.load offset=1055048
                  i32.eq
                  br_if 0 (;@7;)
                  local.get 2
                  i32.const 0
                  i32.load offset=1055044
                  i32.ne
                  br_if 1 (;@6;)
                  i32.const 0
                  local.get 0
                  i32.store offset=1055044
                  i32.const 0
                  i32.const 0
                  i32.load offset=1055036
                  local.get 1
                  i32.add
                  local.tee 1
                  i32.store offset=1055036
                  local.get 0
                  local.get 1
                  call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE
                  return
                end
                i32.const 0
                local.get 0
                i32.store offset=1055048
                i32.const 0
                i32.const 0
                i32.load offset=1055040
                local.get 1
                i32.add
                local.tee 1
                i32.store offset=1055040
                local.get 0
                local.get 1
                i32.const 1
                i32.or
                i32.store offset=4
                local.get 0
                i32.const 0
                i32.load offset=1055044
                i32.eq
                br_if 1 (;@5;)
                br 2 (;@4;)
              end
              local.get 2
              call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
              local.tee 3
              local.get 1
              i32.add
              local.set 1
              block  ;; label = @6
                block  ;; label = @7
                  local.get 3
                  i32.const 256
                  i32.lt_u
                  br_if 0 (;@7;)
                  local.get 2
                  call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hd87001f22d4c88d2E
                  br 1 (;@6;)
                end
                block  ;; label = @7
                  local.get 2
                  i32.const 12
                  i32.add
                  i32.load
                  local.tee 4
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.load
                  local.tee 2
                  i32.eq
                  br_if 0 (;@7;)
                  local.get 2
                  local.get 4
                  i32.store offset=12
                  local.get 4
                  local.get 2
                  i32.store offset=8
                  br 1 (;@6;)
                end
                i32.const 0
                i32.const 0
                i32.load offset=1054636
                i32.const -2
                local.get 3
                i32.const 3
                i32.shr_u
                i32.rotl
                i32.and
                i32.store offset=1054636
              end
              local.get 0
              local.get 1
              call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE
              local.get 0
              i32.const 0
              i32.load offset=1055044
              i32.ne
              br_if 2 (;@3;)
              i32.const 0
              local.get 1
              i32.store offset=1055036
              br 3 (;@2;)
            end
            i32.const 0
            i32.const 0
            i32.store offset=1055036
            i32.const 0
            i32.const 0
            i32.store offset=1055044
          end
          i32.const 0
          i32.load offset=1055076
          local.get 1
          i32.ge_u
          br_if 1 (;@2;)
          call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
          local.set 0
          local.get 0
          local.get 0
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.const 20
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.add
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.add
          i32.sub
          i32.const -65544
          i32.add
          i32.const -9
          i32.and
          i32.const -3
          i32.add
          local.tee 0
          i32.const 0
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.const 2
          i32.shl
          i32.sub
          local.tee 1
          local.get 1
          local.get 0
          i32.gt_u
          select
          i32.eqz
          br_if 1 (;@2;)
          i32.const 0
          i32.load offset=1055048
          i32.eqz
          br_if 1 (;@2;)
          call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
          local.tee 0
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.set 1
          i32.const 20
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.set 3
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.set 4
          i32.const 0
          local.set 2
          block  ;; label = @4
            i32.const 0
            i32.load offset=1055040
            local.tee 5
            local.get 4
            local.get 3
            local.get 1
            local.get 0
            i32.sub
            i32.add
            i32.add
            local.tee 0
            i32.le_u
            br_if 0 (;@4;)
            local.get 5
            local.get 0
            i32.const -1
            i32.xor
            i32.add
            i32.const -65536
            i32.and
            local.set 3
            i32.const 0
            i32.load offset=1055048
            local.set 1
            i32.const 1055060
            local.set 0
            block  ;; label = @5
              loop  ;; label = @6
                block  ;; label = @7
                  local.get 0
                  i32.load
                  local.get 1
                  i32.gt_u
                  br_if 0 (;@7;)
                  local.get 0
                  call $_ZN8dlmalloc8dlmalloc7Segment3top17h1783faaf96129d8aE
                  local.get 1
                  i32.gt_u
                  br_if 2 (;@5;)
                end
                local.get 0
                i32.load offset=8
                local.tee 0
                br_if 0 (;@6;)
              end
              i32.const 0
              local.set 0
            end
            i32.const 0
            local.set 2
            local.get 0
            call $_ZN8dlmalloc8dlmalloc7Segment9is_extern17h512142d4de1be17dE
            br_if 0 (;@4;)
            i32.const 1054636
            local.get 0
            i32.const 12
            i32.add
            i32.load
            i32.const 1
            i32.shr_u
            call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$16can_release_part17h7e547857d2f98700E
            i32.eqz
            br_if 0 (;@4;)
            local.get 0
            i32.load offset=4
            local.get 3
            i32.lt_u
            br_if 0 (;@4;)
            i32.const 1055060
            local.set 1
            loop  ;; label = @5
              local.get 0
              local.get 1
              call $_ZN8dlmalloc8dlmalloc7Segment5holds17h3b728386e6fb8f6cE
              br_if 1 (;@4;)
              local.get 1
              i32.load offset=8
              local.tee 1
              br_if 0 (;@5;)
            end
            i32.const 1054636
            local.get 0
            i32.load
            local.get 0
            i32.load offset=4
            local.tee 1
            local.get 1
            local.get 3
            i32.sub
            call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$9free_part17hc6332b0bc126b7cbE
            i32.eqz
            br_if 0 (;@4;)
            local.get 3
            i32.eqz
            br_if 0 (;@4;)
            local.get 0
            local.get 0
            i32.load offset=4
            local.get 3
            i32.sub
            i32.store offset=4
            i32.const 0
            i32.const 0
            i32.load offset=1055052
            local.get 3
            i32.sub
            i32.store offset=1055052
            i32.const 0
            i32.load offset=1055040
            local.set 1
            i32.const 0
            i32.load offset=1055048
            local.set 0
            i32.const 0
            local.get 0
            local.get 0
            call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E
            local.tee 2
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
            local.get 2
            i32.sub
            local.tee 2
            call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
            local.tee 0
            i32.store offset=1055048
            i32.const 0
            local.get 1
            local.get 3
            local.get 2
            i32.add
            i32.sub
            local.tee 1
            i32.store offset=1055040
            local.get 0
            local.get 1
            i32.const 1
            i32.or
            i32.store offset=4
            call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
            local.tee 2
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
            local.set 4
            i32.const 20
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
            local.set 5
            i32.const 16
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
            local.set 6
            local.get 0
            local.get 1
            call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
            local.get 6
            local.get 5
            local.get 4
            local.get 2
            i32.sub
            i32.add
            i32.add
            i32.store offset=4
            i32.const 0
            i32.const 2097152
            i32.store offset=1055076
            local.get 3
            local.set 2
          end
          local.get 2
          i32.const 0
          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$23release_unused_segments17h26906ec864695c40E
          i32.sub
          i32.ne
          br_if 1 (;@2;)
          i32.const 0
          i32.load offset=1055040
          i32.const 0
          i32.load offset=1055076
          i32.le_u
          br_if 1 (;@2;)
          i32.const 0
          i32.const -1
          i32.store offset=1055076
          return
        end
        local.get 1
        i32.const 256
        i32.lt_u
        br_if 1 (;@1;)
        local.get 0
        local.get 1
        call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17h109ba5955a0705b3E
        i32.const 0
        i32.const 0
        i32.load offset=1055084
        i32.const -1
        i32.add
        local.tee 0
        i32.store offset=1055084
        local.get 0
        br_if 0 (;@2;)
        call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$23release_unused_segments17h26906ec864695c40E
        drop
        return
      end
      return
    end
    local.get 1
    i32.const 3
    i32.shr_u
    local.tee 2
    i32.const 3
    i32.shl
    i32.const 1054644
    i32.add
    local.set 1
    block  ;; label = @1
      block  ;; label = @2
        i32.const 0
        i32.load offset=1054636
        local.tee 3
        i32.const 1
        local.get 2
        i32.shl
        local.tee 2
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load offset=8
        local.set 2
        br 1 (;@1;)
      end
      i32.const 0
      local.get 3
      local.get 2
      i32.or
      i32.store offset=1054636
      local.get 1
      local.set 2
    end
    local.get 1
    local.get 0
    i32.store offset=8
    local.get 2
    local.get 0
    i32.store offset=12
    local.get 0
    local.get 1
    i32.store offset=12
    local.get 0
    local.get 2
    i32.store offset=8)
  (func $_ZN3std9panicking11begin_panic17h11967a7fb7837f40E (type 8) (param i32 i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    local.get 2
    call $_ZN4core5panic8location8Location6caller17h50362f2b70bc36b9E
    i32.store offset=8
    local.get 3
    local.get 1
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    call $_ZN3std10sys_common9backtrace26__rust_end_short_backtrace17h3b68e15c55c8ecfaE
    unreachable)
  (func $_ZN58_$LT$std..io..error..Error$u20$as$u20$core..fmt..Debug$GT$3fmt17h738123dcdfa351b9E (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    call $_ZN57_$LT$std..io..error..Repr$u20$as$u20$core..fmt..Debug$GT$3fmt17hbd84633ccd67d266E)
  (func $_ZN57_$LT$std..io..error..Repr$u20$as$u20$core..fmt..Debug$GT$3fmt17hbd84633ccd67d266E (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 0
                i32.load8_u
                br_table 0 (;@6;) 1 (;@5;) 2 (;@4;) 3 (;@3;) 0 (;@6;)
              end
              local.get 2
              local.get 0
              i32.const 4
              i32.add
              i32.load
              i32.store
              local.get 2
              local.get 1
              i32.const 1050080
              i32.const 2
              call $_ZN4core3fmt9Formatter12debug_struct17h043271df4222cde7E
              i64.store offset=24
              local.get 2
              i32.const 24
              i32.add
              i32.const 1050082
              i32.const 4
              local.get 2
              i32.const 1050088
              call $_ZN4core3fmt8builders11DebugStruct5field17h644623945f1339bdE
              local.set 0
              local.get 2
              i32.const 40
              i32.store8 offset=7
              local.get 0
              i32.const 1050029
              i32.const 4
              local.get 2
              i32.const 7
              i32.add
              i32.const 1050036
              call $_ZN4core3fmt8builders11DebugStruct5field17h644623945f1339bdE
              local.set 1
              i32.const 20
              i32.const 1
              call $__rust_alloc
              local.tee 0
              i32.eqz
              br_if 4 (;@1;)
              local.get 0
              i32.const 16
              i32.add
              i32.const 0
              i32.load offset=1050943 align=1
              i32.store align=1
              local.get 0
              i32.const 8
              i32.add
              i32.const 0
              i64.load offset=1050935 align=1
              i64.store align=1
              local.get 0
              i32.const 0
              i64.load offset=1050927 align=1
              i64.store align=1
              local.get 2
              i64.const 85899345940
              i64.store offset=12 align=4
              local.get 2
              local.get 0
              i32.store offset=8
              local.get 1
              i32.const 1050052
              i32.const 7
              local.get 2
              i32.const 8
              i32.add
              i32.const 1050104
              call $_ZN4core3fmt8builders11DebugStruct5field17h644623945f1339bdE
              call $_ZN4core3fmt8builders11DebugStruct6finish17hafa48911b1175192E
              local.set 0
              local.get 2
              i32.load offset=12
              local.tee 1
              i32.eqz
              br_if 3 (;@2;)
              local.get 2
              i32.load offset=8
              local.tee 3
              i32.eqz
              br_if 3 (;@2;)
              local.get 3
              local.get 1
              i32.const 1
              call $__rust_dealloc
              br 3 (;@2;)
            end
            local.get 2
            local.get 0
            i32.load8_u offset=1
            i32.store8 offset=24
            local.get 2
            i32.const 8
            i32.add
            local.get 1
            i32.const 1050076
            i32.const 4
            call $_ZN4core3fmt9Formatter11debug_tuple17h23c48a8e1a251645E
            local.get 2
            i32.const 8
            i32.add
            local.get 2
            i32.const 24
            i32.add
            i32.const 1050036
            call $_ZN4core3fmt8builders10DebugTuple5field17h694ff79a62cd0513E
            call $_ZN4core3fmt8builders10DebugTuple6finish17h045041801e49228aE
            local.set 0
            br 2 (;@2;)
          end
          local.get 0
          i32.const 4
          i32.add
          i32.load
          local.tee 3
          i32.load
          local.set 4
          local.get 2
          local.get 3
          i32.load offset=4
          i32.store offset=28
          local.get 2
          local.get 4
          i32.store offset=24
          local.get 2
          local.get 0
          i32.load8_u offset=1
          i32.store8
          local.get 2
          local.get 1
          i32.const 1050024
          i32.const 5
          call $_ZN4core3fmt9Formatter12debug_struct17h043271df4222cde7E
          i64.store offset=8
          local.get 2
          i32.const 8
          i32.add
          i32.const 1050029
          i32.const 4
          local.get 2
          i32.const 1050036
          call $_ZN4core3fmt8builders11DebugStruct5field17h644623945f1339bdE
          i32.const 1050052
          i32.const 7
          local.get 2
          i32.const 24
          i32.add
          i32.const 1050060
          call $_ZN4core3fmt8builders11DebugStruct5field17h644623945f1339bdE
          call $_ZN4core3fmt8builders11DebugStruct6finish17hafa48911b1175192E
          local.set 0
          br 1 (;@2;)
        end
        local.get 0
        i32.const 4
        i32.add
        i32.load
        local.set 0
        local.get 2
        local.get 1
        i32.const 1050356
        i32.const 6
        call $_ZN4core3fmt9Formatter12debug_struct17h043271df4222cde7E
        i64.store offset=8
        local.get 2
        local.get 0
        i32.const 8
        i32.add
        i32.store offset=24
        local.get 2
        i32.const 8
        i32.add
        i32.const 1050029
        i32.const 4
        local.get 2
        i32.const 24
        i32.add
        i32.const 1050364
        call $_ZN4core3fmt8builders11DebugStruct5field17h644623945f1339bdE
        drop
        local.get 2
        local.get 0
        i32.store offset=24
        local.get 2
        i32.const 8
        i32.add
        i32.const 1050351
        i32.const 5
        local.get 2
        i32.const 24
        i32.add
        i32.const 1050380
        call $_ZN4core3fmt8builders11DebugStruct5field17h644623945f1339bdE
        drop
        local.get 2
        i32.const 8
        i32.add
        call $_ZN4core3fmt8builders11DebugStruct6finish17hafa48911b1175192E
        local.set 0
      end
      local.get 2
      i32.const 32
      i32.add
      global.set $__stack_pointer
      local.get 0
      return
    end
    i32.const 20
    i32.const 1
    call $_ZN5alloc5alloc18handle_alloc_error17h179b47235854889dE
    unreachable)
  (func $_ZN3std10sys_common9backtrace26__rust_end_short_backtrace17h3b68e15c55c8ecfaE (type 0) (param i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    local.get 0
    i32.load offset=8
    call $_ZN3std9panicking11begin_panic28_$u7b$$u7b$closure$u7d$$u7d$17h0a2ac8d8166919c8E
    unreachable)
  (func $_ZN3std9panicking11begin_panic28_$u7b$$u7b$closure$u7d$$u7d$17h0a2ac8d8166919c8E (type 8) (param i32 i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    local.get 1
    i32.store offset=12
    local.get 3
    local.get 0
    i32.store offset=8
    local.get 3
    i32.const 8
    i32.add
    i32.const 1050320
    i32.const 0
    local.get 2
    call $_ZN3std9panicking20rust_panic_with_hook17h47a0e203360b6c10E
    unreachable)
  (func $_ZN3std10sys_common9backtrace26__rust_end_short_backtrace17h81961607fd97e28eE (type 0) (param i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    local.get 0
    i32.load offset=8
    call $_ZN3std9panicking19begin_panic_handler28_$u7b$$u7b$closure$u7d$$u7d$17h888144d5e9a03cecE
    unreachable)
  (func $_ZN3std9panicking19begin_panic_handler28_$u7b$$u7b$closure$u7d$$u7d$17h888144d5e9a03cecE (type 8) (param i32 i32 i32)
    (local i32 i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 0
    i32.const 20
    i32.add
    i32.load
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 0
            i32.const 4
            i32.add
            i32.load
            br_table 0 (;@4;) 1 (;@3;) 3 (;@1;)
          end
          local.get 4
          br_if 2 (;@1;)
          i32.const 1049972
          local.set 0
          i32.const 0
          local.set 4
          br 1 (;@2;)
        end
        local.get 4
        br_if 1 (;@1;)
        local.get 0
        i32.load
        local.tee 0
        i32.load offset=4
        local.set 4
        local.get 0
        i32.load
        local.set 0
      end
      local.get 3
      local.get 4
      i32.store offset=4
      local.get 3
      local.get 0
      i32.store
      local.get 3
      i32.const 1050268
      local.get 1
      call $_ZN4core5panic10panic_info9PanicInfo7message17hd0be8c3f41a36954E
      local.get 2
      call $_ZN3std9panicking20rust_panic_with_hook17h47a0e203360b6c10E
      unreachable
    end
    local.get 3
    i32.const 0
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 1050248
    local.get 1
    call $_ZN4core5panic10panic_info9PanicInfo7message17hd0be8c3f41a36954E
    local.get 2
    call $_ZN3std9panicking20rust_panic_with_hook17h47a0e203360b6c10E
    unreachable)
  (func $_ZN3std5alloc24default_alloc_error_hook17h302e0694dbb851afE (type 3) (param i32 i32))
  (func $rust_oom (type 3) (param i32 i32)
    (local i32)
    local.get 0
    local.get 1
    i32.const 0
    i32.load offset=1054616
    local.tee 2
    i32.const 11
    local.get 2
    select
    call_indirect (type 3)
    unreachable
    unreachable)
  (func $__rdl_alloc (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    call $_ZN8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h51a06b01c68229b2E)
  (func $__rdl_dealloc (type 8) (param i32 i32 i32)
    local.get 0
    call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$4free17h1e7a99a832191904E)
  (func $__rdl_realloc (type 9) (param i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 9
            i32.lt_u
            br_if 0 (;@4;)
            local.get 3
            local.get 2
            call $_ZN8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h51a06b01c68229b2E
            local.tee 2
            br_if 1 (;@3;)
            i32.const 0
            return
          end
          call $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E
          local.set 1
          i32.const 0
          local.set 2
          local.get 1
          local.get 1
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.const 20
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.add
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.add
          i32.sub
          i32.const -65544
          i32.add
          i32.const -9
          i32.and
          i32.const -3
          i32.add
          local.tee 1
          i32.const 0
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.const 2
          i32.shl
          i32.sub
          local.tee 4
          local.get 4
          local.get 1
          i32.gt_u
          select
          local.get 3
          i32.le_u
          br_if 1 (;@2;)
          i32.const 16
          local.get 3
          i32.const 4
          i32.add
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          i32.const -5
          i32.add
          local.get 3
          i32.gt_u
          select
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
          local.set 4
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk8from_mem17h7acff0b43825b6c7E
          local.set 1
          local.get 1
          local.get 1
          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
          local.tee 5
          call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
          local.set 6
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 1
                          call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17h374540cc5223b826E
                          br_if 0 (;@11;)
                          local.get 5
                          local.get 4
                          i32.ge_u
                          br_if 1 (;@10;)
                          local.get 6
                          i32.const 0
                          i32.load offset=1055048
                          i32.eq
                          br_if 2 (;@9;)
                          local.get 6
                          i32.const 0
                          i32.load offset=1055044
                          i32.eq
                          br_if 3 (;@8;)
                          local.get 6
                          call $_ZN8dlmalloc8dlmalloc5Chunk6cinuse17h7881952257593bc0E
                          br_if 7 (;@4;)
                          local.get 6
                          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
                          local.tee 7
                          local.get 5
                          i32.add
                          local.tee 5
                          local.get 4
                          i32.lt_u
                          br_if 7 (;@4;)
                          local.get 5
                          local.get 4
                          i32.sub
                          local.set 8
                          local.get 7
                          i32.const 256
                          i32.lt_u
                          br_if 4 (;@7;)
                          local.get 6
                          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hd87001f22d4c88d2E
                          br 5 (;@6;)
                        end
                        local.get 1
                        call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
                        local.set 5
                        local.get 4
                        i32.const 256
                        i32.lt_u
                        br_if 6 (;@4;)
                        block  ;; label = @11
                          local.get 5
                          local.get 4
                          i32.const 4
                          i32.add
                          i32.lt_u
                          br_if 0 (;@11;)
                          local.get 5
                          local.get 4
                          i32.sub
                          i32.const 131073
                          i32.lt_u
                          br_if 6 (;@5;)
                        end
                        i32.const 1054636
                        local.get 1
                        local.get 1
                        i32.load
                        local.tee 6
                        i32.sub
                        local.get 5
                        local.get 6
                        i32.add
                        i32.const 16
                        i32.add
                        local.tee 7
                        local.get 4
                        i32.const 31
                        i32.add
                        i32.const 1054636
                        call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$9page_size17h9fbfc7fb87a8a68cE
                        call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                        local.tee 5
                        i32.const 1
                        call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$5remap17hdbe811fa2b97eddaE
                        local.tee 4
                        i32.eqz
                        br_if 6 (;@4;)
                        local.get 4
                        local.get 6
                        i32.add
                        local.tee 1
                        local.get 5
                        local.get 6
                        i32.sub
                        local.tee 3
                        i32.const -16
                        i32.add
                        local.tee 2
                        i32.store offset=4
                        call $_ZN8dlmalloc8dlmalloc5Chunk14fencepost_head17h49e62dea37f7cefeE
                        local.set 0
                        local.get 1
                        local.get 2
                        call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                        local.get 0
                        i32.store offset=4
                        local.get 1
                        local.get 3
                        i32.const -12
                        i32.add
                        call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                        i32.const 0
                        i32.store offset=4
                        i32.const 0
                        i32.const 0
                        i32.load offset=1055052
                        local.get 5
                        local.get 7
                        i32.sub
                        i32.add
                        local.tee 3
                        i32.store offset=1055052
                        i32.const 0
                        i32.const 0
                        i32.load offset=1055080
                        local.tee 2
                        local.get 4
                        local.get 4
                        local.get 2
                        i32.gt_u
                        select
                        i32.store offset=1055080
                        i32.const 0
                        i32.const 0
                        i32.load offset=1055056
                        local.tee 2
                        local.get 3
                        local.get 2
                        local.get 3
                        i32.gt_u
                        select
                        i32.store offset=1055056
                        br 9 (;@1;)
                      end
                      local.get 5
                      local.get 4
                      i32.sub
                      local.tee 5
                      i32.const 16
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      i32.lt_u
                      br_if 4 (;@5;)
                      local.get 1
                      local.get 4
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                      local.set 6
                      local.get 1
                      local.get 4
                      call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
                      local.get 6
                      local.get 5
                      call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
                      local.get 6
                      local.get 5
                      call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$13dispose_chunk17hd7d0611236d858ddE
                      br 4 (;@5;)
                    end
                    i32.const 0
                    i32.load offset=1055040
                    local.get 5
                    i32.add
                    local.tee 5
                    local.get 4
                    i32.le_u
                    br_if 4 (;@4;)
                    local.get 1
                    local.get 4
                    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                    local.set 6
                    local.get 1
                    local.get 4
                    call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
                    local.get 6
                    local.get 5
                    local.get 4
                    i32.sub
                    local.tee 4
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    i32.const 0
                    local.get 4
                    i32.store offset=1055040
                    i32.const 0
                    local.get 6
                    i32.store offset=1055048
                    br 3 (;@5;)
                  end
                  i32.const 0
                  i32.load offset=1055036
                  local.get 5
                  i32.add
                  local.tee 5
                  local.get 4
                  i32.lt_u
                  br_if 3 (;@4;)
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 5
                      local.get 4
                      i32.sub
                      local.tee 6
                      i32.const 16
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                      i32.ge_u
                      br_if 0 (;@9;)
                      local.get 1
                      local.get 5
                      call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
                      i32.const 0
                      local.set 6
                      i32.const 0
                      local.set 5
                      br 1 (;@8;)
                    end
                    local.get 1
                    local.get 4
                    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                    local.tee 5
                    local.get 6
                    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                    local.set 7
                    local.get 1
                    local.get 4
                    call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
                    local.get 5
                    local.get 6
                    call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE
                    local.get 7
                    call $_ZN8dlmalloc8dlmalloc5Chunk12clear_pinuse17he6c43f2da925fa24E
                  end
                  i32.const 0
                  local.get 5
                  i32.store offset=1055044
                  i32.const 0
                  local.get 6
                  i32.store offset=1055036
                  br 2 (;@5;)
                end
                block  ;; label = @7
                  local.get 6
                  i32.const 12
                  i32.add
                  i32.load
                  local.tee 9
                  local.get 6
                  i32.const 8
                  i32.add
                  i32.load
                  local.tee 6
                  i32.eq
                  br_if 0 (;@7;)
                  local.get 6
                  local.get 9
                  i32.store offset=12
                  local.get 9
                  local.get 6
                  i32.store offset=8
                  br 1 (;@6;)
                end
                i32.const 0
                i32.const 0
                i32.load offset=1054636
                i32.const -2
                local.get 7
                i32.const 3
                i32.shr_u
                i32.rotl
                i32.and
                i32.store offset=1054636
              end
              block  ;; label = @6
                local.get 8
                i32.const 16
                i32.const 8
                call $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E
                i32.lt_u
                br_if 0 (;@6;)
                local.get 1
                local.get 4
                call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E
                local.set 5
                local.get 1
                local.get 4
                call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
                local.get 5
                local.get 8
                call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
                local.get 5
                local.get 8
                call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$13dispose_chunk17hd7d0611236d858ddE
                br 1 (;@5;)
              end
              local.get 1
              local.get 5
              call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE
            end
            local.get 1
            br_if 3 (;@1;)
          end
          local.get 3
          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h1538d4b11d1da1beE
          local.tee 4
          i32.eqz
          br_if 1 (;@2;)
          local.get 4
          local.get 0
          local.get 3
          local.get 1
          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE
          i32.const -8
          i32.const -4
          local.get 1
          call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17h374540cc5223b826E
          select
          i32.add
          local.tee 2
          local.get 2
          local.get 3
          i32.gt_u
          select
          call $memcpy
          local.set 3
          local.get 0
          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$4free17h1e7a99a832191904E
          local.get 3
          return
        end
        local.get 2
        local.get 0
        local.get 3
        local.get 1
        local.get 1
        local.get 3
        i32.gt_u
        select
        call $memcpy
        drop
        local.get 0
        call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$4free17h1e7a99a832191904E
      end
      local.get 2
      return
    end
    local.get 1
    call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17h374540cc5223b826E
    drop
    local.get 1
    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E)
  (func $_ZN3std9panicking8set_hook17hb54d22d6dbc51511E (type 3) (param i32 i32)
    (local i32)
    block  ;; label = @1
      block  ;; label = @2
        i32.const 0
        i32.load offset=1054632
        i32.const 2147483647
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        call $_ZN3std9panicking11panic_count17is_zero_slow_path17hd1df70a45d849404E
        i32.eqz
        br_if 1 (;@1;)
      end
      i32.const 0
      i32.load offset=1054620
      local.set 2
      i32.const 0
      i32.const -1
      i32.store offset=1054620
      block  ;; label = @2
        local.get 2
        br_if 0 (;@2;)
        i32.const 0
        i32.load offset=1054628
        local.set 2
        i32.const 0
        local.get 1
        i32.store offset=1054628
        i32.const 0
        i32.load offset=1054624
        local.set 1
        i32.const 0
        local.get 0
        i32.store offset=1054624
        i32.const 0
        i32.const 0
        i32.store offset=1054620
        block  ;; label = @3
          local.get 2
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          local.get 2
          i32.load
          call_indirect (type 0)
          local.get 2
          i32.load offset=4
          local.tee 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 1
          local.get 0
          local.get 2
          i32.load offset=8
          call $__rust_dealloc
        end
        return
      end
      unreachable
      unreachable
    end
    i32.const 1050120
    i32.const 52
    i32.const 1050200
    call $_ZN3std9panicking11begin_panic17h11967a7fb7837f40E
    unreachable)
  (func $rust_begin_unwind (type 0) (param i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 1
    global.set $__stack_pointer
    local.get 0
    call $_ZN4core5panic10panic_info9PanicInfo8location17h759003817d7fe3f4E
    i32.const 1050216
    call $_ZN4core6option15Option$LT$T$GT$6unwrap17h97c6976d505cbddeE
    local.set 2
    local.get 0
    call $_ZN4core5panic10panic_info9PanicInfo7message17hd0be8c3f41a36954E
    call $_ZN4core6option15Option$LT$T$GT$6unwrap17hbc0c812220a7cf70E
    local.set 3
    local.get 1
    local.get 2
    i32.store offset=8
    local.get 1
    local.get 0
    i32.store offset=4
    local.get 1
    local.get 3
    i32.store
    local.get 1
    call $_ZN3std10sys_common9backtrace26__rust_end_short_backtrace17h81961607fd97e28eE
    unreachable)
  (func $_ZN3std9panicking20rust_panic_with_hook17h47a0e203360b6c10E (type 10) (param i32 i32 i32 i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 4
    global.set $__stack_pointer
    i32.const 1
    local.set 5
    i32.const 0
    i32.const 0
    i32.load offset=1054632
    local.tee 6
    i32.const 1
    i32.add
    i32.store offset=1054632
    block  ;; label = @1
      block  ;; label = @2
        i32.const 0
        i32.load offset=1055088
        i32.const 1
        i32.ne
        br_if 0 (;@2;)
        i32.const 0
        i32.load offset=1055092
        i32.const 1
        i32.add
        local.set 5
        br 1 (;@1;)
      end
      i32.const 0
      i32.const 1
      i32.store offset=1055088
    end
    i32.const 0
    local.get 5
    i32.store offset=1055092
    block  ;; label = @1
      block  ;; label = @2
        local.get 6
        i32.const 0
        i32.lt_s
        br_if 0 (;@2;)
        local.get 5
        i32.const 2
        i32.gt_u
        br_if 0 (;@2;)
        local.get 4
        local.get 3
        i32.store offset=28
        local.get 4
        local.get 2
        i32.store offset=24
        i32.const 0
        i32.load offset=1054620
        local.tee 6
        i32.const -1
        i32.le_s
        br_if 0 (;@2;)
        i32.const 0
        local.get 6
        i32.const 1
        i32.add
        local.tee 6
        i32.store offset=1054620
        block  ;; label = @3
          i32.const 0
          i32.load offset=1054628
          local.tee 2
          i32.eqz
          br_if 0 (;@3;)
          i32.const 0
          i32.load offset=1054624
          local.set 6
          local.get 4
          i32.const 8
          i32.add
          local.get 0
          local.get 1
          i32.load offset=16
          call_indirect (type 3)
          local.get 4
          local.get 4
          i64.load offset=8
          i64.store offset=16
          local.get 6
          local.get 4
          i32.const 16
          i32.add
          local.get 2
          i32.load offset=20
          call_indirect (type 3)
          i32.const 0
          i32.load offset=1054620
          local.set 6
        end
        i32.const 0
        local.get 6
        i32.const -1
        i32.add
        i32.store offset=1054620
        local.get 5
        i32.const 1
        i32.le_u
        br_if 1 (;@1;)
      end
      unreachable
      unreachable
    end
    local.get 0
    local.get 1
    call $rust_panic
    unreachable)
  (func $_ZN90_$LT$std..panicking..begin_panic_handler..PanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$8take_box17h7ce99a17e9564003E (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i64)
    global.get $__stack_pointer
    i32.const 48
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 1
    i32.const 4
    i32.add
    local.set 3
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.load offset=4
        i32.eqz
        br_if 0 (;@2;)
        i32.const 0
        i32.load offset=1050016
        local.set 4
        br 1 (;@1;)
      end
      local.get 1
      i32.load
      local.set 5
      local.get 2
      i64.const 0
      i64.store offset=12 align=4
      local.get 2
      i32.const 0
      i32.load offset=1050016
      local.tee 4
      i32.store offset=8
      local.get 2
      local.get 2
      i32.const 8
      i32.add
      i32.store offset=20
      local.get 2
      i32.const 24
      i32.add
      i32.const 16
      i32.add
      local.get 5
      i32.const 16
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      i32.const 24
      i32.add
      i32.const 8
      i32.add
      local.get 5
      i32.const 8
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      local.get 5
      i64.load align=4
      i64.store offset=24
      local.get 2
      i32.const 20
      i32.add
      i32.const 1049948
      local.get 2
      i32.const 24
      i32.add
      call $_ZN4core3fmt5write17h8874ba14b0fd43feE
      drop
      local.get 3
      i32.const 8
      i32.add
      local.get 2
      i32.const 8
      i32.add
      i32.const 8
      i32.add
      i32.load
      i32.store
      local.get 3
      local.get 2
      i64.load offset=8
      i64.store align=4
    end
    local.get 2
    i32.const 24
    i32.add
    i32.const 8
    i32.add
    local.tee 5
    local.get 3
    i32.const 8
    i32.add
    i32.load
    i32.store
    local.get 1
    i32.const 12
    i32.add
    i32.const 0
    i32.store
    local.get 3
    i64.load align=4
    local.set 6
    local.get 1
    i32.const 8
    i32.add
    i32.const 0
    i32.store
    local.get 1
    local.get 4
    i32.store offset=4
    local.get 2
    local.get 6
    i64.store offset=24
    block  ;; label = @1
      i32.const 12
      i32.const 4
      call $__rust_alloc
      local.tee 1
      br_if 0 (;@1;)
      i32.const 12
      i32.const 4
      call $_ZN5alloc5alloc18handle_alloc_error17h179b47235854889dE
      unreachable
    end
    local.get 1
    local.get 2
    i64.load offset=24
    i64.store align=4
    local.get 1
    i32.const 8
    i32.add
    local.get 5
    i32.load
    i32.store
    local.get 0
    i32.const 1050288
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store
    local.get 2
    i32.const 48
    i32.add
    global.set $__stack_pointer)
  (func $_ZN90_$LT$std..panicking..begin_panic_handler..PanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$3get17hbfdef6bcdd013225E (type 3) (param i32 i32)
    (local i32 i32)
    global.get $__stack_pointer
    i32.const 48
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 1
    i32.const 4
    i32.add
    local.set 3
    block  ;; label = @1
      local.get 1
      i32.load offset=4
      br_if 0 (;@1;)
      local.get 1
      i32.load
      local.set 1
      local.get 2
      i64.const 0
      i64.store offset=12 align=4
      local.get 2
      i32.const 0
      i32.load offset=1050016
      i32.store offset=8
      local.get 2
      local.get 2
      i32.const 8
      i32.add
      i32.store offset=20
      local.get 2
      i32.const 24
      i32.add
      i32.const 16
      i32.add
      local.get 1
      i32.const 16
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      i32.const 24
      i32.add
      i32.const 8
      i32.add
      local.get 1
      i32.const 8
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      local.get 1
      i64.load align=4
      i64.store offset=24
      local.get 2
      i32.const 20
      i32.add
      i32.const 1049948
      local.get 2
      i32.const 24
      i32.add
      call $_ZN4core3fmt5write17h8874ba14b0fd43feE
      drop
      local.get 3
      i32.const 8
      i32.add
      local.get 2
      i32.const 8
      i32.add
      i32.const 8
      i32.add
      i32.load
      i32.store
      local.get 3
      local.get 2
      i64.load offset=8
      i64.store align=4
    end
    local.get 0
    i32.const 1050288
    i32.store offset=4
    local.get 0
    local.get 3
    i32.store
    local.get 2
    i32.const 48
    i32.add
    global.set $__stack_pointer)
  (func $_ZN93_$LT$std..panicking..begin_panic_handler..StrPanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$8take_box17h03b9b6028fc9e0f4E (type 3) (param i32 i32)
    (local i32 i32)
    local.get 1
    i32.load offset=4
    local.set 2
    local.get 1
    i32.load
    local.set 3
    block  ;; label = @1
      i32.const 8
      i32.const 4
      call $__rust_alloc
      local.tee 1
      br_if 0 (;@1;)
      i32.const 8
      i32.const 4
      call $_ZN5alloc5alloc18handle_alloc_error17h179b47235854889dE
      unreachable
    end
    local.get 1
    local.get 2
    i32.store offset=4
    local.get 1
    local.get 3
    i32.store
    local.get 0
    i32.const 1050304
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store)
  (func $_ZN93_$LT$std..panicking..begin_panic_handler..StrPanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$3get17hfd52de6c2d25f5deE (type 3) (param i32 i32)
    local.get 0
    i32.const 1050304
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store)
  (func $_ZN91_$LT$std..panicking..begin_panic..PanicPayload$LT$A$GT$$u20$as$u20$core..panic..BoxMeUp$GT$8take_box17h64e1239c12ef2cb0E (type 3) (param i32 i32)
    (local i32 i32)
    local.get 1
    i32.load
    local.set 2
    local.get 1
    i32.const 0
    i32.store
    block  ;; label = @1
      block  ;; label = @2
        local.get 2
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load offset=4
        local.set 3
        i32.const 8
        i32.const 4
        call $__rust_alloc
        local.tee 1
        i32.eqz
        br_if 1 (;@1;)
        local.get 1
        local.get 3
        i32.store offset=4
        local.get 1
        local.get 2
        i32.store
        local.get 0
        i32.const 1050304
        i32.store offset=4
        local.get 0
        local.get 1
        i32.store
        return
      end
      unreachable
      unreachable
    end
    i32.const 8
    i32.const 4
    call $_ZN5alloc5alloc18handle_alloc_error17h179b47235854889dE
    unreachable)
  (func $_ZN91_$LT$std..panicking..begin_panic..PanicPayload$LT$A$GT$$u20$as$u20$core..panic..BoxMeUp$GT$3get17hade34ccc3d198d5dE (type 3) (param i32 i32)
    block  ;; label = @1
      local.get 1
      i32.load
      br_if 0 (;@1;)
      unreachable
      unreachable
    end
    local.get 0
    i32.const 1050304
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store)
  (func $rust_panic (type 3) (param i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 2
    local.get 1
    i32.store offset=12
    local.get 2
    local.get 0
    i32.store offset=8
    local.get 2
    i32.const 8
    i32.add
    call $__rust_start_panic
    drop
    unreachable
    unreachable)
  (func $__rust_start_panic (type 13) (param i32) (result i32)
    unreachable
    unreachable)
  (func $_ZN8dlmalloc8dlmalloc8align_up17h58684d94d4d566f4E (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.add
    i32.const -1
    i32.add
    i32.const 0
    local.get 1
    i32.sub
    i32.and)
  (func $_ZN8dlmalloc8dlmalloc9left_bits17hde52ae0f2ac5b398E (type 13) (param i32) (result i32)
    local.get 0
    i32.const 1
    i32.shl
    local.tee 0
    i32.const 0
    local.get 0
    i32.sub
    i32.or)
  (func $_ZN8dlmalloc8dlmalloc9least_bit17h06921975976171dcE (type 13) (param i32) (result i32)
    i32.const 0
    local.get 0
    i32.sub
    local.get 0
    i32.and)
  (func $_ZN8dlmalloc8dlmalloc24leftshift_for_tree_index17he003bc47d801254fE (type 13) (param i32) (result i32)
    i32.const 0
    i32.const 25
    local.get 0
    i32.const 1
    i32.shr_u
    i32.sub
    local.get 0
    i32.const 31
    i32.eq
    select)
  (func $_ZN8dlmalloc8dlmalloc5Chunk14fencepost_head17h49e62dea37f7cefeE (type 12) (result i32)
    i32.const 7)
  (func $_ZN8dlmalloc8dlmalloc5Chunk4size17hf0f2eb46d31a25eaE (type 13) (param i32) (result i32)
    local.get 0
    i32.load offset=4
    i32.const -8
    i32.and)
  (func $_ZN8dlmalloc8dlmalloc5Chunk6cinuse17h7881952257593bc0E (type 13) (param i32) (result i32)
    local.get 0
    i32.load8_u offset=4
    i32.const 2
    i32.and
    i32.const 1
    i32.shr_u)
  (func $_ZN8dlmalloc8dlmalloc5Chunk6pinuse17h7e78a342f64f614cE (type 13) (param i32) (result i32)
    local.get 0
    i32.load offset=4
    i32.const 1
    i32.and)
  (func $_ZN8dlmalloc8dlmalloc5Chunk12clear_pinuse17he6c43f2da925fa24E (type 0) (param i32)
    local.get 0
    local.get 0
    i32.load offset=4
    i32.const -2
    i32.and
    i32.store offset=4)
  (func $_ZN8dlmalloc8dlmalloc5Chunk5inuse17hba1a84bba98ecaa7E (type 13) (param i32) (result i32)
    local.get 0
    i32.load offset=4
    i32.const 3
    i32.and
    i32.const 1
    i32.ne)
  (func $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17h374540cc5223b826E (type 13) (param i32) (result i32)
    local.get 0
    i32.load8_u offset=4
    i32.const 3
    i32.and
    i32.eqz)
  (func $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17hc9de2cdba0d37dacE (type 3) (param i32 i32)
    local.get 0
    local.get 0
    i32.load offset=4
    i32.const 1
    i32.and
    local.get 1
    i32.or
    i32.const 2
    i32.or
    i32.store offset=4
    local.get 1
    local.get 0
    i32.add
    i32.const 4
    i32.add
    local.tee 0
    local.get 0
    i32.load
    i32.const 1
    i32.or
    i32.store)
  (func $_ZN8dlmalloc8dlmalloc5Chunk20set_inuse_and_pinuse17h37a663285e2dcd9dE (type 3) (param i32 i32)
    local.get 0
    local.get 1
    i32.const 3
    i32.or
    i32.store offset=4
    local.get 1
    local.get 0
    i32.add
    i32.const 4
    i32.add
    local.tee 0
    local.get 0
    i32.load
    i32.const 1
    i32.or
    i32.store)
  (func $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hbc5285bcaa5cf3fdE (type 3) (param i32 i32)
    local.get 0
    local.get 1
    i32.const 3
    i32.or
    i32.store offset=4)
  (func $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17hb87aef9eec5aae8cE (type 3) (param i32 i32)
    local.get 0
    local.get 1
    i32.const 1
    i32.or
    i32.store offset=4
    local.get 0
    local.get 1
    i32.add
    local.get 1
    i32.store)
  (func $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17h774ca7c76a5c5aabE (type 8) (param i32 i32 i32)
    local.get 2
    local.get 2
    i32.load offset=4
    i32.const -2
    i32.and
    i32.store offset=4
    local.get 0
    local.get 1
    i32.const 1
    i32.or
    i32.store offset=4
    local.get 0
    local.get 1
    i32.add
    local.get 1
    i32.store)
  (func $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h464fa8255a601fd2E (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.add)
  (func $_ZN8dlmalloc8dlmalloc5Chunk12minus_offset17hc64997bcfff16288E (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.sub)
  (func $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17hc637081340f37214E (type 13) (param i32) (result i32)
    local.get 0
    i32.const 8
    i32.add)
  (func $_ZN8dlmalloc8dlmalloc5Chunk10mem_offset17h3e1371564a9f24f9E (type 12) (result i32)
    i32.const 8)
  (func $_ZN8dlmalloc8dlmalloc5Chunk8from_mem17h7acff0b43825b6c7E (type 13) (param i32) (result i32)
    local.get 0
    i32.const -8
    i32.add)
  (func $_ZN8dlmalloc8dlmalloc9TreeChunk14leftmost_child17hf6286ca5315942e0E (type 13) (param i32) (result i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=16
      local.tee 1
      br_if 0 (;@1;)
      local.get 0
      i32.const 20
      i32.add
      i32.load
      local.set 1
    end
    local.get 1)
  (func $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h992dc95b9ebfdf60E (type 13) (param i32) (result i32)
    local.get 0)
  (func $_ZN8dlmalloc8dlmalloc9TreeChunk4next17h9c173fcea4183d8fE (type 13) (param i32) (result i32)
    local.get 0
    i32.load offset=12)
  (func $_ZN8dlmalloc8dlmalloc9TreeChunk4prev17hde4bfcd1cf1f00a3E (type 13) (param i32) (result i32)
    local.get 0
    i32.load offset=8)
  (func $_ZN8dlmalloc8dlmalloc7Segment9is_extern17h512142d4de1be17dE (type 13) (param i32) (result i32)
    local.get 0
    i32.load offset=12
    i32.const 1
    i32.and)
  (func $_ZN8dlmalloc8dlmalloc7Segment9sys_flags17h17058350dbbb4cc7E (type 13) (param i32) (result i32)
    local.get 0
    i32.load offset=12
    i32.const 1
    i32.shr_u)
  (func $_ZN8dlmalloc8dlmalloc7Segment5holds17h3b728386e6fb8f6cE (type 2) (param i32 i32) (result i32)
    (local i32 i32)
    i32.const 0
    local.set 2
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 3
      local.get 1
      i32.gt_u
      br_if 0 (;@1;)
      local.get 3
      local.get 0
      i32.load offset=4
      i32.add
      local.get 1
      i32.gt_u
      local.set 2
    end
    local.get 2)
  (func $_ZN8dlmalloc8dlmalloc7Segment3top17h1783faaf96129d8aE (type 13) (param i32) (result i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    i32.add)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$5alloc17h870b2dd71ff5fddcE (type 8) (param i32 i32 i32)
    (local i32)
    local.get 2
    i32.const 16
    i32.shr_u
    memory.grow
    local.set 3
    local.get 0
    i32.const 0
    i32.store offset=8
    local.get 0
    i32.const 0
    local.get 2
    i32.const -65536
    i32.and
    local.get 3
    i32.const -1
    i32.eq
    local.tee 2
    select
    i32.store offset=4
    local.get 0
    i32.const 0
    local.get 3
    i32.const 16
    i32.shl
    local.get 2
    select
    i32.store)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$5remap17hdbe811fa2b97eddaE (type 14) (param i32 i32 i32 i32 i32) (result i32)
    i32.const 0)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$9free_part17hc6332b0bc126b7cbE (type 9) (param i32 i32 i32 i32) (result i32)
    i32.const 0)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$4free17hdf442810a3c47608E (type 4) (param i32 i32 i32) (result i32)
    i32.const 0)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$16can_release_part17h7e547857d2f98700E (type 2) (param i32 i32) (result i32)
    i32.const 0)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$9page_size17h9fbfc7fb87a8a68cE (type 13) (param i32) (result i32)
    i32.const 65536)
  (func $_ZN4core3ptr27drop_in_place$LT$$RF$u8$GT$17h3c378e23854b1f65E (type 0) (param i32))
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$10write_char17h1282ec243aa9cf5cE (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    call $_ZN5alloc6string6String4push17h6da9af01a8c51887E
    i32.const 0)
  (func $_ZN5alloc6string6String4push17h6da9af01a8c51887E (type 3) (param i32 i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 127
        i32.gt_u
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 0
          i32.load offset=8
          local.tee 3
          local.get 0
          i32.const 4
          i32.add
          i32.load
          i32.ne
          br_if 0 (;@3;)
          local.get 0
          local.get 3
          i32.const 1
          call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17ha5db695f65d66a7aE
          local.get 0
          i32.load offset=8
          local.set 3
        end
        local.get 0
        local.get 3
        i32.const 1
        i32.add
        i32.store offset=8
        local.get 0
        i32.load
        local.get 3
        i32.add
        local.get 1
        i32.store8
        br 1 (;@1;)
      end
      local.get 2
      i32.const 0
      i32.store offset=12
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.const 2048
          i32.lt_u
          br_if 0 (;@3;)
          block  ;; label = @4
            local.get 1
            i32.const 65536
            i32.ge_u
            br_if 0 (;@4;)
            local.get 2
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=14
            local.get 2
            local.get 1
            i32.const 12
            i32.shr_u
            i32.const 224
            i32.or
            i32.store8 offset=12
            local.get 2
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=13
            i32.const 3
            local.set 1
            br 2 (;@2;)
          end
          local.get 2
          local.get 1
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=15
          local.get 2
          local.get 1
          i32.const 18
          i32.shr_u
          i32.const 240
          i32.or
          i32.store8 offset=12
          local.get 2
          local.get 1
          i32.const 6
          i32.shr_u
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=14
          local.get 2
          local.get 1
          i32.const 12
          i32.shr_u
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=13
          i32.const 4
          local.set 1
          br 1 (;@2;)
        end
        local.get 2
        local.get 1
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=13
        local.get 2
        local.get 1
        i32.const 6
        i32.shr_u
        i32.const 192
        i32.or
        i32.store8 offset=12
        i32.const 2
        local.set 1
      end
      block  ;; label = @2
        local.get 0
        i32.const 4
        i32.add
        i32.load
        local.get 0
        i32.const 8
        i32.add
        local.tee 4
        i32.load
        local.tee 3
        i32.sub
        local.get 1
        i32.ge_u
        br_if 0 (;@2;)
        local.get 0
        local.get 3
        local.get 1
        call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17ha5db695f65d66a7aE
        local.get 4
        i32.load
        local.set 3
      end
      local.get 0
      i32.load
      local.get 3
      i32.add
      local.get 2
      i32.const 12
      i32.add
      local.get 1
      call $memcpy
      drop
      local.get 4
      local.get 3
      local.get 1
      i32.add
      i32.store
    end
    local.get 2
    i32.const 16
    i32.add
    global.set $__stack_pointer)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_fmt17h8d373c03cf4ddb6fE (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 2
    local.get 0
    i32.load
    i32.store offset=4
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=8
    local.get 2
    i32.const 4
    i32.add
    i32.const 1050948
    local.get 2
    i32.const 8
    i32.add
    call $_ZN4core3fmt5write17h8874ba14b0fd43feE
    local.set 1
    local.get 2
    i32.const 32
    i32.add
    global.set $__stack_pointer
    local.get 1)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_str17h032b9e77e62a03d5E (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 3
      i32.const 4
      i32.add
      i32.load
      local.get 3
      i32.const 8
      i32.add
      local.tee 4
      i32.load
      local.tee 0
      i32.sub
      local.get 2
      i32.ge_u
      br_if 0 (;@1;)
      local.get 3
      local.get 0
      local.get 2
      call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17ha5db695f65d66a7aE
      local.get 4
      i32.load
      local.set 0
    end
    local.get 3
    i32.load
    local.get 0
    i32.add
    local.get 1
    local.get 2
    call $memcpy
    drop
    local.get 4
    local.get 0
    local.get 2
    i32.add
    i32.store
    i32.const 0)
  (func $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17ha5db695f65d66a7aE (type 8) (param i32 i32 i32)
    (local i32 i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    block  ;; label = @1
      local.get 1
      local.get 2
      i32.add
      local.tee 2
      local.get 1
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.tee 1
      i32.const 1
      i32.shl
      local.tee 4
      local.get 2
      local.get 4
      local.get 2
      i32.gt_u
      select
      local.tee 2
      i32.const 8
      local.get 2
      i32.const 8
      i32.gt_u
      select
      local.set 2
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          i32.const 16
          i32.add
          i32.const 8
          i32.add
          i32.const 1
          i32.store
          local.get 3
          local.get 1
          i32.store offset=20
          local.get 3
          local.get 0
          i32.load
          i32.store offset=16
          br 1 (;@2;)
        end
        local.get 3
        i32.const 0
        i32.store offset=16
      end
      local.get 3
      local.get 2
      i32.const 1
      local.get 3
      i32.const 16
      i32.add
      call $_ZN5alloc7raw_vec11finish_grow17hc82d353923f459cdE
      block  ;; label = @2
        local.get 3
        i32.load
        i32.const 1
        i32.ne
        br_if 0 (;@2;)
        local.get 3
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.eqz
        br_if 1 (;@1;)
        local.get 3
        i32.load offset=4
        local.get 0
        call $_ZN5alloc5alloc18handle_alloc_error17h179b47235854889dE
        unreachable
      end
      local.get 0
      local.get 3
      i64.load offset=4 align=4
      i64.store align=4
      local.get 3
      i32.const 32
      i32.add
      global.set $__stack_pointer
      return
    end
    call $_ZN5alloc7raw_vec17capacity_overflow17hada0f1b01ff34e0eE
    unreachable)
  (func $_ZN5alloc7raw_vec11finish_grow17hc82d353923f459cdE (type 10) (param i32 i32 i32 i32)
    (local i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 2
                  i32.eqz
                  br_if 0 (;@7;)
                  i32.const 1
                  local.set 4
                  local.get 1
                  i32.const 0
                  i32.lt_s
                  br_if 1 (;@6;)
                  local.get 3
                  i32.load
                  local.tee 5
                  i32.eqz
                  br_if 3 (;@4;)
                  local.get 3
                  i32.load offset=4
                  local.tee 3
                  br_if 2 (;@5;)
                  local.get 1
                  br_if 4 (;@3;)
                  local.get 2
                  local.set 3
                  br 5 (;@2;)
                end
                local.get 0
                local.get 1
                i32.store offset=4
                i32.const 1
                local.set 4
              end
              i32.const 0
              local.set 1
              br 4 (;@1;)
            end
            local.get 5
            local.get 3
            local.get 2
            local.get 1
            call $__rust_realloc
            local.set 3
            br 2 (;@2;)
          end
          local.get 1
          br_if 0 (;@3;)
          local.get 2
          local.set 3
          br 1 (;@2;)
        end
        local.get 1
        local.get 2
        call $__rust_alloc
        local.set 3
      end
      block  ;; label = @2
        local.get 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        local.get 3
        i32.store offset=4
        i32.const 0
        local.set 4
        br 1 (;@1;)
      end
      local.get 0
      local.get 1
      i32.store offset=4
      local.get 2
      local.set 1
    end
    local.get 0
    local.get 4
    i32.store
    local.get 0
    i32.const 8
    i32.add
    local.get 1
    i32.store)
  (func $_ZN5alloc5alloc18handle_alloc_error17h179b47235854889dE (type 3) (param i32 i32)
    local.get 0
    local.get 1
    call $__rust_alloc_error_handler
    unreachable)
  (func $_ZN5alloc7raw_vec17capacity_overflow17hada0f1b01ff34e0eE (type 7)
    i32.const 1051016
    i32.const 17
    i32.const 1051036
    call $_ZN4core9panicking5panic17h35778e3c18266c93E
    unreachable)
  (func $__rg_oom (type 3) (param i32 i32)
    local.get 0
    local.get 1
    call $rust_oom
    unreachable)
  (func $_ZN5alloc3fmt6format17h6ab9c6dede04b06aE (type 3) (param i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 1
    i32.const 20
    i32.add
    i32.load
    local.set 3
    local.get 1
    i32.load
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 4
        i32.add
        i32.load
        local.tee 5
        i32.const 3
        i32.shl
        local.tee 6
        br_if 0 (;@2;)
        i32.const 0
        local.set 7
        br 1 (;@1;)
      end
      local.get 6
      i32.const -8
      i32.add
      local.tee 6
      i32.const 3
      i32.shr_u
      i32.const 1
      i32.add
      local.tee 8
      i32.const 7
      i32.and
      local.set 9
      block  ;; label = @2
        block  ;; label = @3
          local.get 6
          i32.const 56
          i32.ge_u
          br_if 0 (;@3;)
          i32.const 0
          local.set 7
          local.get 4
          local.set 8
          br 1 (;@2;)
        end
        local.get 4
        i32.const 60
        i32.add
        local.set 6
        i32.const 0
        local.set 7
        i32.const 0
        local.get 8
        i32.const 1073741816
        i32.and
        i32.sub
        local.set 8
        loop  ;; label = @3
          local.get 6
          i32.load
          local.get 6
          i32.const -8
          i32.add
          i32.load
          local.get 6
          i32.const -16
          i32.add
          i32.load
          local.get 6
          i32.const -24
          i32.add
          i32.load
          local.get 6
          i32.const -32
          i32.add
          i32.load
          local.get 6
          i32.const -40
          i32.add
          i32.load
          local.get 6
          i32.const -48
          i32.add
          i32.load
          local.get 6
          i32.const -56
          i32.add
          i32.load
          local.get 7
          i32.add
          i32.add
          i32.add
          i32.add
          i32.add
          i32.add
          i32.add
          i32.add
          local.set 7
          local.get 6
          i32.const 64
          i32.add
          local.set 6
          local.get 8
          i32.const 8
          i32.add
          local.tee 8
          br_if 0 (;@3;)
        end
        local.get 6
        i32.const -60
        i32.add
        local.set 8
      end
      local.get 9
      i32.eqz
      br_if 0 (;@1;)
      i32.const 0
      local.get 9
      i32.sub
      local.set 6
      local.get 8
      i32.const 4
      i32.add
      local.set 8
      loop  ;; label = @2
        local.get 8
        i32.load
        local.get 7
        i32.add
        local.set 7
        local.get 6
        i32.const 1
        i32.add
        local.tee 9
        local.get 6
        i32.ge_u
        local.set 10
        local.get 9
        local.set 6
        local.get 8
        i32.const 8
        i32.add
        local.set 8
        local.get 10
        br_if 0 (;@2;)
      end
    end
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            br_if 0 (;@4;)
            local.get 7
            local.set 6
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 5
            i32.eqz
            br_if 0 (;@4;)
            local.get 4
            i32.load offset=4
            br_if 0 (;@4;)
            local.get 7
            i32.const 16
            i32.lt_u
            br_if 2 (;@2;)
          end
          local.get 7
          local.get 7
          i32.add
          local.tee 6
          local.get 7
          i32.lt_u
          br_if 1 (;@2;)
        end
        i32.const 0
        local.set 7
        block  ;; label = @3
          block  ;; label = @4
            local.get 6
            i32.const 0
            i32.lt_s
            br_if 0 (;@4;)
            block  ;; label = @5
              local.get 6
              br_if 0 (;@5;)
              i32.const 1
              local.set 8
              br 4 (;@1;)
            end
            local.get 6
            i32.const 1
            call $__rust_alloc
            local.tee 8
            i32.eqz
            br_if 1 (;@3;)
            local.get 6
            local.set 7
            br 3 (;@1;)
          end
          call $_ZN5alloc7raw_vec17capacity_overflow17hada0f1b01ff34e0eE
          unreachable
        end
        local.get 6
        i32.const 1
        call $_ZN5alloc5alloc18handle_alloc_error17h179b47235854889dE
        unreachable
      end
      i32.const 1
      local.set 8
      i32.const 0
      local.set 7
    end
    local.get 0
    i32.const 0
    i32.store offset=8
    local.get 0
    local.get 7
    i32.store offset=4
    local.get 0
    local.get 8
    i32.store
    local.get 2
    local.get 0
    i32.store offset=4
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=8
    block  ;; label = @1
      local.get 2
      i32.const 4
      i32.add
      i32.const 1050948
      local.get 2
      i32.const 8
      i32.add
      call $_ZN4core3fmt5write17h8874ba14b0fd43feE
      i32.eqz
      br_if 0 (;@1;)
      i32.const 1051052
      i32.const 51
      local.get 2
      i32.const 8
      i32.add
      i32.const 1050972
      i32.const 1051128
      call $_ZN4core6result13unwrap_failed17h6d050dbd00b66340E
      unreachable
    end
    local.get 2
    i32.const 32
    i32.add
    global.set $__stack_pointer)
  (func $_ZN4core3ops8function6FnOnce9call_once17h46d8ad6e7ff1079cE (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    drop
    loop (result i32)  ;; label = @1
      br 0 (;@1;)
    end)
  (func $_ZN4core3ptr102drop_in_place$LT$$RF$core..iter..adapters..copied..Copied$LT$core..slice..iter..Iter$LT$u8$GT$$GT$$GT$17haf777a974e22beeaE (type 0) (param i32))
  (func $_ZN4core9panicking18panic_bounds_check17h859d2a727fd634e5E (type 8) (param i32 i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 48
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    local.get 1
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 28
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 44
    i32.add
    i32.const 4
    i32.store
    local.get 3
    i64.const 2
    i64.store offset=12 align=4
    local.get 3
    i32.const 1051268
    i32.store offset=8
    local.get 3
    i32.const 4
    i32.store offset=36
    local.get 3
    local.get 3
    i32.const 32
    i32.add
    i32.store offset=24
    local.get 3
    local.get 3
    i32.store offset=40
    local.get 3
    local.get 3
    i32.const 4
    i32.add
    i32.store offset=32
    local.get 3
    i32.const 8
    i32.add
    local.get 2
    call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
    unreachable)
  (func $_ZN4core5slice5index26slice_start_index_len_fail17hf52653f30012ff28E (type 8) (param i32 i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 48
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    local.get 1
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 28
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 44
    i32.add
    i32.const 4
    i32.store
    local.get 3
    i64.const 2
    i64.store offset=12 align=4
    local.get 3
    i32.const 1051848
    i32.store offset=8
    local.get 3
    i32.const 4
    i32.store offset=36
    local.get 3
    local.get 3
    i32.const 32
    i32.add
    i32.store offset=24
    local.get 3
    local.get 3
    i32.const 4
    i32.add
    i32.store offset=40
    local.get 3
    local.get 3
    i32.store offset=32
    local.get 3
    i32.const 8
    i32.add
    local.get 2
    call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
    unreachable)
  (func $_ZN4core5slice5index24slice_end_index_len_fail17h9f1e36a81f2d5daaE (type 8) (param i32 i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 48
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    local.get 1
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 28
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 44
    i32.add
    i32.const 4
    i32.store
    local.get 3
    i64.const 2
    i64.store offset=12 align=4
    local.get 3
    i32.const 1051880
    i32.store offset=8
    local.get 3
    i32.const 4
    i32.store offset=36
    local.get 3
    local.get 3
    i32.const 32
    i32.add
    i32.store offset=24
    local.get 3
    local.get 3
    i32.const 4
    i32.add
    i32.store offset=40
    local.get 3
    local.get 3
    i32.store offset=32
    local.get 3
    i32.const 8
    i32.add
    local.get 2
    call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
    unreachable)
  (func $_ZN4core3fmt9Formatter3pad17hd96851719c666d38E (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32)
    local.get 0
    i32.load offset=16
    local.set 3
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 0
            i32.load offset=8
            local.tee 4
            i32.const 1
            i32.eq
            br_if 0 (;@4;)
            local.get 3
            i32.const 1
            i32.eq
            br_if 1 (;@3;)
            local.get 0
            i32.load offset=24
            local.get 1
            local.get 2
            local.get 0
            i32.const 28
            i32.add
            i32.load
            i32.load offset=12
            call_indirect (type 4)
            local.set 3
            br 3 (;@1;)
          end
          local.get 3
          i32.const 1
          i32.ne
          br_if 1 (;@2;)
        end
        local.get 1
        local.get 2
        i32.add
        local.set 5
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 0
              i32.const 20
              i32.add
              i32.load
              local.tee 6
              br_if 0 (;@5;)
              i32.const 0
              local.set 7
              local.get 1
              local.set 8
              br 1 (;@4;)
            end
            i32.const 0
            local.set 7
            local.get 1
            local.set 8
            loop  ;; label = @5
              local.get 8
              local.tee 3
              local.get 5
              i32.eq
              br_if 2 (;@3;)
              block  ;; label = @6
                block  ;; label = @7
                  local.get 3
                  i32.load8_s
                  local.tee 8
                  i32.const -1
                  i32.le_s
                  br_if 0 (;@7;)
                  local.get 3
                  i32.const 1
                  i32.add
                  local.set 8
                  br 1 (;@6;)
                end
                block  ;; label = @7
                  local.get 8
                  i32.const 255
                  i32.and
                  local.tee 8
                  i32.const 224
                  i32.ge_u
                  br_if 0 (;@7;)
                  local.get 3
                  i32.const 2
                  i32.add
                  local.set 8
                  br 1 (;@6;)
                end
                block  ;; label = @7
                  local.get 8
                  i32.const 240
                  i32.ge_u
                  br_if 0 (;@7;)
                  local.get 3
                  i32.const 3
                  i32.add
                  local.set 8
                  br 1 (;@6;)
                end
                local.get 3
                i32.load8_u offset=2
                i32.const 63
                i32.and
                i32.const 6
                i32.shl
                local.get 3
                i32.load8_u offset=1
                i32.const 63
                i32.and
                i32.const 12
                i32.shl
                i32.or
                local.get 3
                i32.load8_u offset=3
                i32.const 63
                i32.and
                i32.or
                local.get 8
                i32.const 18
                i32.shl
                i32.const 1835008
                i32.and
                i32.or
                i32.const 1114112
                i32.eq
                br_if 3 (;@3;)
                local.get 3
                i32.const 4
                i32.add
                local.set 8
              end
              local.get 7
              local.get 3
              i32.sub
              local.get 8
              i32.add
              local.set 7
              local.get 6
              i32.const -1
              i32.add
              local.tee 6
              br_if 0 (;@5;)
            end
          end
          local.get 8
          local.get 5
          i32.eq
          br_if 0 (;@3;)
          block  ;; label = @4
            local.get 8
            i32.load8_u
            local.tee 3
            i32.const 240
            i32.lt_u
            br_if 0 (;@4;)
            local.get 8
            i32.load8_u offset=2
            i32.const 63
            i32.and
            i32.const 6
            i32.shl
            local.get 8
            i32.load8_u offset=1
            i32.const 63
            i32.and
            i32.const 12
            i32.shl
            i32.or
            local.get 8
            i32.load8_u offset=3
            i32.const 63
            i32.and
            i32.or
            local.get 3
            i32.const 18
            i32.shl
            i32.const 1835008
            i32.and
            i32.or
            i32.const 1114112
            i32.eq
            br_if 1 (;@3;)
          end
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 7
                br_if 0 (;@6;)
                i32.const 0
                local.set 8
                br 1 (;@5;)
              end
              block  ;; label = @6
                local.get 7
                local.get 2
                i32.lt_u
                br_if 0 (;@6;)
                i32.const 0
                local.set 3
                local.get 2
                local.set 8
                local.get 7
                local.get 2
                i32.eq
                br_if 1 (;@5;)
                br 2 (;@4;)
              end
              i32.const 0
              local.set 3
              local.get 7
              local.set 8
              local.get 1
              local.get 7
              i32.add
              i32.load8_s
              i32.const -64
              i32.lt_s
              br_if 1 (;@4;)
            end
            local.get 8
            local.set 7
            local.get 1
            local.set 3
          end
          local.get 7
          local.get 2
          local.get 3
          select
          local.set 2
          local.get 3
          local.get 1
          local.get 3
          select
          local.set 1
        end
        local.get 4
        i32.const 1
        i32.eq
        br_if 0 (;@2;)
        local.get 0
        i32.load offset=24
        local.get 1
        local.get 2
        local.get 0
        i32.const 28
        i32.add
        i32.load
        i32.load offset=12
        call_indirect (type 4)
        return
      end
      local.get 0
      i32.const 12
      i32.add
      i32.load
      local.set 5
      block  ;; label = @2
        block  ;; label = @3
          local.get 2
          br_if 0 (;@3;)
          i32.const 0
          local.set 8
          br 1 (;@2;)
        end
        local.get 2
        i32.const 3
        i32.and
        local.set 7
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const -1
            i32.add
            i32.const 3
            i32.ge_u
            br_if 0 (;@4;)
            i32.const 0
            local.set 8
            local.get 1
            local.set 3
            br 1 (;@3;)
          end
          i32.const 0
          local.set 8
          i32.const 0
          local.get 2
          i32.const -4
          i32.and
          i32.sub
          local.set 6
          local.get 1
          local.set 3
          loop  ;; label = @4
            local.get 8
            local.get 3
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.get 3
            i32.const 1
            i32.add
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.get 3
            i32.const 2
            i32.add
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.get 3
            i32.const 3
            i32.add
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.set 8
            local.get 3
            i32.const 4
            i32.add
            local.set 3
            local.get 6
            i32.const 4
            i32.add
            local.tee 6
            br_if 0 (;@4;)
          end
        end
        local.get 7
        i32.eqz
        br_if 0 (;@2;)
        loop  ;; label = @3
          local.get 8
          local.get 3
          i32.load8_s
          i32.const -65
          i32.gt_s
          i32.add
          local.set 8
          local.get 3
          i32.const 1
          i32.add
          local.set 3
          local.get 7
          i32.const -1
          i32.add
          local.tee 7
          br_if 0 (;@3;)
        end
      end
      block  ;; label = @2
        local.get 5
        local.get 8
        i32.le_u
        br_if 0 (;@2;)
        i32.const 0
        local.set 3
        local.get 5
        local.get 8
        i32.sub
        local.tee 7
        local.set 5
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              i32.const 0
              local.get 0
              i32.load8_u offset=32
              local.tee 8
              local.get 8
              i32.const 3
              i32.eq
              select
              i32.const 3
              i32.and
              br_table 2 (;@3;) 0 (;@5;) 1 (;@4;) 2 (;@3;)
            end
            i32.const 0
            local.set 5
            local.get 7
            local.set 3
            br 1 (;@3;)
          end
          local.get 7
          i32.const 1
          i32.shr_u
          local.set 3
          local.get 7
          i32.const 1
          i32.add
          i32.const 1
          i32.shr_u
          local.set 5
        end
        local.get 3
        i32.const 1
        i32.add
        local.set 3
        local.get 0
        i32.const 28
        i32.add
        i32.load
        local.set 7
        local.get 0
        i32.load offset=4
        local.set 8
        local.get 0
        i32.load offset=24
        local.set 6
        block  ;; label = @3
          loop  ;; label = @4
            local.get 3
            i32.const -1
            i32.add
            local.tee 3
            i32.eqz
            br_if 1 (;@3;)
            local.get 6
            local.get 8
            local.get 7
            i32.load offset=16
            call_indirect (type 2)
            i32.eqz
            br_if 0 (;@4;)
          end
          i32.const 1
          return
        end
        i32.const 1
        local.set 3
        local.get 8
        i32.const 1114112
        i32.eq
        br_if 1 (;@1;)
        local.get 6
        local.get 1
        local.get 2
        local.get 7
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        i32.const 0
        local.set 3
        loop  ;; label = @3
          block  ;; label = @4
            local.get 5
            local.get 3
            i32.ne
            br_if 0 (;@4;)
            local.get 5
            local.get 5
            i32.lt_u
            return
          end
          local.get 3
          i32.const 1
          i32.add
          local.set 3
          local.get 6
          local.get 8
          local.get 7
          i32.load offset=16
          call_indirect (type 2)
          i32.eqz
          br_if 0 (;@3;)
        end
        local.get 3
        i32.const -1
        i32.add
        local.get 5
        i32.lt_u
        return
      end
      local.get 0
      i32.load offset=24
      local.get 1
      local.get 2
      local.get 0
      i32.const 28
      i32.add
      i32.load
      i32.load offset=12
      call_indirect (type 4)
      return
    end
    local.get 3)
  (func $_ZN4core9panicking5panic17h35778e3c18266c93E (type 8) (param i32 i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    i32.const 20
    i32.add
    i32.const 0
    i32.store
    local.get 3
    i32.const 1051144
    i32.store offset=16
    local.get 3
    i64.const 1
    i64.store offset=4 align=4
    local.get 3
    local.get 1
    i32.store offset=28
    local.get 3
    local.get 0
    i32.store offset=24
    local.get 3
    local.get 3
    i32.const 24
    i32.add
    i32.store
    local.get 3
    local.get 2
    call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
    unreachable)
  (func $_ZN4core5slice5index22slice_index_order_fail17hbafd09170640ee9bE (type 8) (param i32 i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 48
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    local.get 1
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 28
    i32.add
    i32.const 2
    i32.store
    local.get 3
    i32.const 44
    i32.add
    i32.const 4
    i32.store
    local.get 3
    i64.const 2
    i64.store offset=12 align=4
    local.get 3
    i32.const 1051932
    i32.store offset=8
    local.get 3
    i32.const 4
    i32.store offset=36
    local.get 3
    local.get 3
    i32.const 32
    i32.add
    i32.store offset=24
    local.get 3
    local.get 3
    i32.const 4
    i32.add
    i32.store offset=40
    local.get 3
    local.get 3
    i32.store offset=32
    local.get 3
    i32.const 8
    i32.add
    local.get 2
    call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
    unreachable)
  (func $_ZN4core3fmt3num3imp52_$LT$impl$u20$core..fmt..Display$u20$for$u20$u32$GT$3fmt17h0e0d5207039041dbE (type 2) (param i32 i32) (result i32)
    local.get 0
    i64.load32_u
    i32.const 1
    local.get 1
    call $_ZN4core3fmt3num3imp7fmt_u6417ha36fe3566051d739E)
  (func $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E (type 3) (param i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 2
    local.get 1
    i32.store offset=12
    local.get 2
    local.get 0
    i32.store offset=8
    local.get 2
    i32.const 1051200
    i32.store offset=4
    local.get 2
    i32.const 1051144
    i32.store
    local.get 2
    call $rust_begin_unwind
    unreachable)
  (func $_ZN4core3fmt5write17h8874ba14b0fd43feE (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get $__stack_pointer
    i32.const 48
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    local.get 3
    i32.const 36
    i32.add
    local.get 1
    i32.store
    local.get 3
    i32.const 3
    i32.store8 offset=40
    local.get 3
    i64.const 137438953472
    i64.store offset=8
    local.get 3
    local.get 0
    i32.store offset=32
    i32.const 0
    local.set 4
    local.get 3
    i32.const 0
    i32.store offset=24
    local.get 3
    i32.const 0
    i32.store offset=16
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.load offset=8
            local.tee 5
            br_if 0 (;@4;)
            local.get 2
            i32.const 20
            i32.add
            i32.load
            local.tee 6
            i32.eqz
            br_if 1 (;@3;)
            local.get 2
            i32.load
            local.set 1
            local.get 2
            i32.load offset=16
            local.set 0
            local.get 6
            i32.const 3
            i32.shl
            i32.const -8
            i32.add
            i32.const 3
            i32.shr_u
            i32.const 1
            i32.add
            local.tee 4
            local.set 6
            loop  ;; label = @5
              block  ;; label = @6
                local.get 1
                i32.const 4
                i32.add
                i32.load
                local.tee 7
                i32.eqz
                br_if 0 (;@6;)
                local.get 3
                i32.load offset=32
                local.get 1
                i32.load
                local.get 7
                local.get 3
                i32.load offset=36
                i32.load offset=12
                call_indirect (type 4)
                br_if 4 (;@2;)
              end
              local.get 0
              i32.load
              local.get 3
              i32.const 8
              i32.add
              local.get 0
              i32.const 4
              i32.add
              i32.load
              call_indirect (type 2)
              br_if 3 (;@2;)
              local.get 0
              i32.const 8
              i32.add
              local.set 0
              local.get 1
              i32.const 8
              i32.add
              local.set 1
              local.get 6
              i32.const -1
              i32.add
              local.tee 6
              br_if 0 (;@5;)
              br 2 (;@3;)
            end
          end
          local.get 2
          i32.const 12
          i32.add
          i32.load
          local.tee 0
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          i32.const 5
          i32.shl
          local.tee 8
          i32.const -32
          i32.add
          i32.const 5
          i32.shr_u
          i32.const 1
          i32.add
          local.set 4
          local.get 2
          i32.load
          local.set 1
          i32.const 0
          local.set 6
          loop  ;; label = @4
            block  ;; label = @5
              local.get 1
              i32.const 4
              i32.add
              i32.load
              local.tee 0
              i32.eqz
              br_if 0 (;@5;)
              local.get 3
              i32.load offset=32
              local.get 1
              i32.load
              local.get 0
              local.get 3
              i32.load offset=36
              i32.load offset=12
              call_indirect (type 4)
              br_if 3 (;@2;)
            end
            local.get 3
            local.get 5
            local.get 6
            i32.add
            local.tee 0
            i32.const 28
            i32.add
            i32.load8_u
            i32.store8 offset=40
            local.get 3
            local.get 0
            i32.const 4
            i32.add
            i64.load align=4
            i64.const 32
            i64.rotl
            i64.store offset=8
            local.get 0
            i32.const 24
            i32.add
            i32.load
            local.set 9
            local.get 2
            i32.load offset=16
            local.set 10
            i32.const 0
            local.set 11
            i32.const 0
            local.set 7
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 0
                  i32.const 20
                  i32.add
                  i32.load
                  br_table 1 (;@6;) 0 (;@7;) 2 (;@5;) 1 (;@6;)
                end
                local.get 9
                i32.const 3
                i32.shl
                local.set 12
                i32.const 0
                local.set 7
                local.get 10
                local.get 12
                i32.add
                local.tee 12
                i32.load offset=4
                i32.const 37
                i32.ne
                br_if 1 (;@5;)
                local.get 12
                i32.load
                i32.load
                local.set 9
              end
              i32.const 1
              local.set 7
            end
            local.get 3
            local.get 9
            i32.store offset=20
            local.get 3
            local.get 7
            i32.store offset=16
            local.get 0
            i32.const 16
            i32.add
            i32.load
            local.set 7
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 0
                  i32.const 12
                  i32.add
                  i32.load
                  br_table 1 (;@6;) 0 (;@7;) 2 (;@5;) 1 (;@6;)
                end
                local.get 7
                i32.const 3
                i32.shl
                local.set 9
                local.get 10
                local.get 9
                i32.add
                local.tee 9
                i32.load offset=4
                i32.const 37
                i32.ne
                br_if 1 (;@5;)
                local.get 9
                i32.load
                i32.load
                local.set 7
              end
              i32.const 1
              local.set 11
            end
            local.get 3
            local.get 7
            i32.store offset=28
            local.get 3
            local.get 11
            i32.store offset=24
            local.get 10
            local.get 0
            i32.load
            i32.const 3
            i32.shl
            i32.add
            local.tee 0
            i32.load
            local.get 3
            i32.const 8
            i32.add
            local.get 0
            i32.load offset=4
            call_indirect (type 2)
            br_if 2 (;@2;)
            local.get 1
            i32.const 8
            i32.add
            local.set 1
            local.get 8
            local.get 6
            i32.const 32
            i32.add
            local.tee 6
            i32.ne
            br_if 0 (;@4;)
          end
        end
        i32.const 0
        local.set 0
        local.get 4
        local.get 2
        i32.load offset=4
        i32.lt_u
        local.tee 1
        i32.eqz
        br_if 1 (;@1;)
        local.get 3
        i32.load offset=32
        local.get 2
        i32.load
        local.get 4
        i32.const 3
        i32.shl
        i32.add
        i32.const 0
        local.get 1
        select
        local.tee 1
        i32.load
        local.get 1
        i32.load offset=4
        local.get 3
        i32.load offset=36
        i32.load offset=12
        call_indirect (type 4)
        i32.eqz
        br_if 1 (;@1;)
      end
      i32.const 1
      local.set 0
    end
    local.get 3
    i32.const 48
    i32.add
    global.set $__stack_pointer
    local.get 0)
  (func $_ZN71_$LT$core..ops..range..Range$LT$Idx$GT$$u20$as$u20$core..fmt..Debug$GT$3fmt17h175a7f03519f2338E (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32 i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    i32.const 1
    local.set 3
    block  ;; label = @1
      local.get 0
      local.get 1
      call $_ZN4core3fmt3num52_$LT$impl$u20$core..fmt..Debug$u20$for$u20$usize$GT$3fmt17h4c7db5d789952d3dE
      br_if 0 (;@1;)
      local.get 1
      i32.const 28
      i32.add
      i32.load
      local.set 4
      local.get 1
      i32.load offset=24
      local.set 5
      local.get 2
      i32.const 28
      i32.add
      i32.const 0
      i32.store
      local.get 2
      i32.const 1051144
      i32.store offset=24
      local.get 2
      i64.const 1
      i64.store offset=12 align=4
      local.get 2
      i32.const 1051148
      i32.store offset=8
      local.get 5
      local.get 4
      local.get 2
      i32.const 8
      i32.add
      call $_ZN4core3fmt5write17h8874ba14b0fd43feE
      br_if 0 (;@1;)
      local.get 0
      i32.const 4
      i32.add
      local.get 1
      call $_ZN4core3fmt3num52_$LT$impl$u20$core..fmt..Debug$u20$for$u20$usize$GT$3fmt17h4c7db5d789952d3dE
      local.set 3
    end
    local.get 2
    i32.const 32
    i32.add
    global.set $__stack_pointer
    local.get 3)
  (func $_ZN4core3fmt3num52_$LT$impl$u20$core..fmt..Debug$u20$for$u20$usize$GT$3fmt17h4c7db5d789952d3dE (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 128
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 1
              i32.load
              local.tee 3
              i32.const 16
              i32.and
              br_if 0 (;@5;)
              local.get 3
              i32.const 32
              i32.and
              br_if 1 (;@4;)
              local.get 0
              i64.load32_u
              i32.const 1
              local.get 1
              call $_ZN4core3fmt3num3imp7fmt_u6417ha36fe3566051d739E
              local.set 0
              br 4 (;@1;)
            end
            local.get 0
            i32.load
            local.set 0
            i32.const 0
            local.set 3
            loop  ;; label = @5
              local.get 2
              local.get 3
              i32.add
              i32.const 127
              i32.add
              i32.const 48
              i32.const 87
              local.get 0
              i32.const 15
              i32.and
              local.tee 4
              i32.const 10
              i32.lt_u
              select
              local.get 4
              i32.add
              i32.store8
              local.get 3
              i32.const -1
              i32.add
              local.set 3
              local.get 0
              i32.const 15
              i32.gt_u
              local.set 4
              local.get 0
              i32.const 4
              i32.shr_u
              local.set 0
              local.get 4
              br_if 0 (;@5;)
            end
            local.get 3
            i32.const 128
            i32.add
            local.tee 0
            i32.const 129
            i32.ge_u
            br_if 1 (;@3;)
            local.get 1
            i32.const 1
            i32.const 1051460
            i32.const 2
            local.get 2
            local.get 3
            i32.add
            i32.const 128
            i32.add
            i32.const 0
            local.get 3
            i32.sub
            call $_ZN4core3fmt9Formatter12pad_integral17hc91f100cd8a143afE
            local.set 0
            br 3 (;@1;)
          end
          local.get 0
          i32.load
          local.set 0
          i32.const 0
          local.set 3
          loop  ;; label = @4
            local.get 2
            local.get 3
            i32.add
            i32.const 127
            i32.add
            i32.const 48
            i32.const 55
            local.get 0
            i32.const 15
            i32.and
            local.tee 4
            i32.const 10
            i32.lt_u
            select
            local.get 4
            i32.add
            i32.store8
            local.get 3
            i32.const -1
            i32.add
            local.set 3
            local.get 0
            i32.const 15
            i32.gt_u
            local.set 4
            local.get 0
            i32.const 4
            i32.shr_u
            local.set 0
            local.get 4
            br_if 0 (;@4;)
          end
          local.get 3
          i32.const 128
          i32.add
          local.tee 0
          i32.const 129
          i32.ge_u
          br_if 1 (;@2;)
          local.get 1
          i32.const 1
          i32.const 1051460
          i32.const 2
          local.get 2
          local.get 3
          i32.add
          i32.const 128
          i32.add
          i32.const 0
          local.get 3
          i32.sub
          call $_ZN4core3fmt9Formatter12pad_integral17hc91f100cd8a143afE
          local.set 0
          br 2 (;@1;)
        end
        local.get 0
        i32.const 128
        i32.const 1051444
        call $_ZN4core5slice5index26slice_start_index_len_fail17hf52653f30012ff28E
        unreachable
      end
      local.get 0
      i32.const 128
      i32.const 1051444
      call $_ZN4core5slice5index26slice_start_index_len_fail17hf52653f30012ff28E
      unreachable
    end
    local.get 2
    i32.const 128
    i32.add
    global.set $__stack_pointer
    local.get 0)
  (func $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17h02aaa3363841a0a8E (type 1) (param i32) (result i64)
    i64.const -5455902982710169930)
  (func $_ZN4core3fmt8builders11DebugStruct5field17h644623945f1339bdE (type 14) (param i32 i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i64 i64)
    global.get $__stack_pointer
    i32.const 64
    i32.sub
    local.tee 5
    global.set $__stack_pointer
    i32.const 1
    local.set 6
    block  ;; label = @1
      local.get 0
      i32.load8_u offset=4
      br_if 0 (;@1;)
      local.get 0
      i32.load8_u offset=5
      local.set 7
      block  ;; label = @2
        local.get 0
        i32.load
        local.tee 8
        i32.load8_u
        i32.const 4
        i32.and
        br_if 0 (;@2;)
        i32.const 1
        local.set 6
        local.get 8
        i32.load offset=24
        i32.const 1051401
        i32.const 1051403
        local.get 7
        i32.const 255
        i32.and
        local.tee 7
        select
        i32.const 2
        i32.const 3
        local.get 7
        select
        local.get 8
        i32.const 28
        i32.add
        i32.load
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        i32.const 1
        local.set 6
        local.get 8
        i32.load offset=24
        local.get 1
        local.get 2
        local.get 8
        i32.load offset=28
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        i32.const 1
        local.set 6
        local.get 8
        i32.load offset=24
        i32.const 1051285
        i32.const 2
        local.get 8
        i32.load offset=28
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        local.get 3
        local.get 8
        local.get 4
        i32.load offset=12
        call_indirect (type 2)
        local.set 6
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 7
        i32.const 255
        i32.and
        br_if 0 (;@2;)
        i32.const 1
        local.set 6
        local.get 8
        i32.load offset=24
        i32.const 1051396
        i32.const 3
        local.get 8
        i32.const 28
        i32.add
        i32.load
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
      end
      i32.const 1
      local.set 6
      local.get 5
      i32.const 1
      i32.store8 offset=23
      local.get 5
      i32.const 52
      i32.add
      i32.const 1051304
      i32.store
      local.get 5
      i32.const 16
      i32.add
      local.get 5
      i32.const 23
      i32.add
      i32.store
      local.get 5
      local.get 8
      i64.load offset=24 align=4
      i64.store offset=8
      local.get 8
      i64.load offset=8 align=4
      local.set 9
      local.get 8
      i64.load offset=16 align=4
      local.set 10
      local.get 5
      local.get 8
      i32.load8_u offset=32
      i32.store8 offset=56
      local.get 5
      local.get 10
      i64.store offset=40
      local.get 5
      local.get 9
      i64.store offset=32
      local.get 5
      local.get 8
      i64.load align=4
      i64.store offset=24
      local.get 5
      local.get 5
      i32.const 8
      i32.add
      i32.store offset=48
      local.get 5
      i32.const 8
      i32.add
      local.get 1
      local.get 2
      call $_ZN68_$LT$core..fmt..builders..PadAdapter$u20$as$u20$core..fmt..Write$GT$9write_str17h7ca5dec47fadd416E
      br_if 0 (;@1;)
      local.get 5
      i32.const 8
      i32.add
      i32.const 1051285
      i32.const 2
      call $_ZN68_$LT$core..fmt..builders..PadAdapter$u20$as$u20$core..fmt..Write$GT$9write_str17h7ca5dec47fadd416E
      br_if 0 (;@1;)
      local.get 3
      local.get 5
      i32.const 24
      i32.add
      local.get 4
      i32.load offset=12
      call_indirect (type 2)
      br_if 0 (;@1;)
      local.get 5
      i32.load offset=48
      i32.const 1051399
      i32.const 2
      local.get 5
      i32.load offset=52
      i32.load offset=12
      call_indirect (type 4)
      local.set 6
    end
    local.get 0
    i32.const 1
    i32.store8 offset=5
    local.get 0
    local.get 6
    i32.store8 offset=4
    local.get 5
    i32.const 64
    i32.add
    global.set $__stack_pointer
    local.get 0)
  (func $_ZN4core5panic8location8Location6caller17h50362f2b70bc36b9E (type 13) (param i32) (result i32)
    local.get 0)
  (func $_ZN4core5panic8location8Location4line17h24c1eca3491f6000E (type 13) (param i32) (result i32)
    local.get 0
    i32.load offset=8)
  (func $_ZN44_$LT$$RF$T$u20$as$u20$core..fmt..Display$GT$3fmt17hfb847384256de23cE (type 2) (param i32 i32) (result i32)
    local.get 1
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    call $_ZN4core3fmt9Formatter3pad17hd96851719c666d38E)
  (func $_ZN4core5panic10panic_info9PanicInfo7payload17h7ab0dcc2dcc4528eE (type 3) (param i32 i32)
    local.get 0
    local.get 1
    i64.load align=4
    i64.store)
  (func $_ZN4core5panic10panic_info9PanicInfo7message17hd0be8c3f41a36954E (type 13) (param i32) (result i32)
    local.get 0
    i32.load offset=8)
  (func $_ZN4core5panic10panic_info9PanicInfo8location17h759003817d7fe3f4E (type 13) (param i32) (result i32)
    local.get 0
    i32.load offset=12)
  (func $_ZN42_$LT$$RF$T$u20$as$u20$core..fmt..Debug$GT$3fmt17he19a0ce54ad248d3E (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    local.get 0
    i32.load offset=4
    i32.load offset=12
    call_indirect (type 2))
  (func $_ZN4core6result13unwrap_failed17h6d050dbd00b66340E (type 15) (param i32 i32 i32 i32 i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 64
    i32.sub
    local.tee 5
    global.set $__stack_pointer
    local.get 5
    local.get 1
    i32.store offset=12
    local.get 5
    local.get 0
    i32.store offset=8
    local.get 5
    local.get 3
    i32.store offset=20
    local.get 5
    local.get 2
    i32.store offset=16
    local.get 5
    i32.const 44
    i32.add
    i32.const 2
    i32.store
    local.get 5
    i32.const 60
    i32.add
    i32.const 38
    i32.store
    local.get 5
    i64.const 2
    i64.store offset=28 align=4
    local.get 5
    i32.const 1051288
    i32.store offset=24
    local.get 5
    i32.const 39
    i32.store offset=52
    local.get 5
    local.get 5
    i32.const 48
    i32.add
    i32.store offset=40
    local.get 5
    local.get 5
    i32.const 16
    i32.add
    i32.store offset=56
    local.get 5
    local.get 5
    i32.const 8
    i32.add
    i32.store offset=48
    local.get 5
    i32.const 24
    i32.add
    local.get 4
    call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
    unreachable)
  (func $_ZN68_$LT$core..fmt..builders..PadAdapter$u20$as$u20$core..fmt..Write$GT$9write_str17h7ca5dec47fadd416E (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.eqz
            br_if 0 (;@4;)
            local.get 0
            i32.load offset=4
            local.set 4
            local.get 0
            i32.load
            local.set 5
            local.get 0
            i32.load offset=8
            local.set 6
            loop  ;; label = @5
              block  ;; label = @6
                local.get 6
                i32.load8_u
                i32.eqz
                br_if 0 (;@6;)
                local.get 5
                i32.const 1051328
                i32.const 4
                local.get 4
                i32.load offset=12
                call_indirect (type 4)
                i32.eqz
                br_if 0 (;@6;)
                i32.const 1
                local.set 0
                br 3 (;@3;)
              end
              i32.const 0
              local.set 0
              local.get 2
              local.set 7
              block  ;; label = @6
                loop  ;; label = @7
                  local.get 1
                  local.get 0
                  i32.add
                  local.set 8
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 7
                      i32.const 8
                      i32.lt_u
                      br_if 0 (;@9;)
                      local.get 3
                      i32.const 8
                      i32.add
                      i32.const 10
                      local.get 8
                      local.get 7
                      call $_ZN4core5slice6memchr19memchr_general_case17h6bcaafb5e36b0dd0E
                      local.get 3
                      i32.load offset=12
                      local.set 7
                      local.get 3
                      i32.load offset=8
                      local.set 9
                      br 1 (;@8;)
                    end
                    block  ;; label = @9
                      local.get 7
                      br_if 0 (;@9;)
                      i32.const 0
                      local.set 7
                      i32.const 0
                      local.set 9
                      br 1 (;@8;)
                    end
                    i32.const 0
                    local.set 10
                    block  ;; label = @9
                      local.get 8
                      i32.load8_u
                      i32.const 10
                      i32.eq
                      br_if 0 (;@9;)
                      i32.const 0
                      local.set 9
                      local.get 7
                      i32.const 1
                      i32.eq
                      br_if 1 (;@8;)
                      i32.const 1
                      local.set 10
                      local.get 8
                      i32.load8_u offset=1
                      i32.const 10
                      i32.eq
                      br_if 0 (;@9;)
                      local.get 7
                      i32.const 2
                      i32.eq
                      br_if 1 (;@8;)
                      i32.const 2
                      local.set 10
                      local.get 8
                      i32.load8_u offset=2
                      i32.const 10
                      i32.eq
                      br_if 0 (;@9;)
                      local.get 7
                      i32.const 3
                      i32.eq
                      br_if 1 (;@8;)
                      i32.const 3
                      local.set 10
                      local.get 8
                      i32.load8_u offset=3
                      i32.const 10
                      i32.eq
                      br_if 0 (;@9;)
                      local.get 7
                      i32.const 4
                      i32.eq
                      br_if 1 (;@8;)
                      i32.const 4
                      local.set 10
                      local.get 8
                      i32.load8_u offset=4
                      i32.const 10
                      i32.eq
                      br_if 0 (;@9;)
                      local.get 7
                      i32.const 5
                      i32.eq
                      br_if 1 (;@8;)
                      i32.const 5
                      local.set 10
                      local.get 8
                      i32.load8_u offset=5
                      i32.const 10
                      i32.eq
                      br_if 0 (;@9;)
                      local.get 7
                      i32.const 6
                      i32.eq
                      br_if 1 (;@8;)
                      i32.const 6
                      local.set 10
                      local.get 8
                      i32.load8_u offset=6
                      i32.const 10
                      i32.ne
                      br_if 1 (;@8;)
                    end
                    i32.const 1
                    local.set 9
                    local.get 10
                    local.set 7
                  end
                  i32.const 0
                  local.set 8
                  block  ;; label = @8
                    local.get 9
                    i32.const 1
                    i32.eq
                    br_if 0 (;@8;)
                    local.get 2
                    local.set 0
                    br 2 (;@6;)
                  end
                  block  ;; label = @8
                    local.get 7
                    local.get 0
                    i32.add
                    local.tee 7
                    i32.const 1
                    i32.add
                    local.tee 0
                    local.get 7
                    i32.lt_u
                    br_if 0 (;@8;)
                    local.get 2
                    local.get 0
                    i32.lt_u
                    br_if 0 (;@8;)
                    local.get 1
                    local.get 7
                    i32.add
                    i32.load8_u
                    i32.const 10
                    i32.ne
                    br_if 0 (;@8;)
                    i32.const 1
                    local.set 8
                    br 2 (;@6;)
                  end
                  local.get 2
                  local.get 0
                  i32.sub
                  local.set 7
                  local.get 2
                  local.get 0
                  i32.ge_u
                  br_if 0 (;@7;)
                end
                local.get 2
                local.set 0
              end
              local.get 6
              local.get 8
              i32.store8
              block  ;; label = @6
                block  ;; label = @7
                  local.get 2
                  local.get 0
                  i32.gt_u
                  br_if 0 (;@7;)
                  local.get 2
                  local.get 0
                  i32.ne
                  br_if 5 (;@2;)
                  local.get 5
                  local.get 1
                  local.get 0
                  local.get 4
                  i32.load offset=12
                  call_indirect (type 4)
                  i32.eqz
                  br_if 1 (;@6;)
                  i32.const 1
                  local.set 0
                  br 4 (;@3;)
                end
                local.get 1
                local.get 0
                i32.add
                local.tee 7
                i32.load8_s
                i32.const -65
                i32.le_s
                br_if 4 (;@2;)
                block  ;; label = @7
                  local.get 5
                  local.get 1
                  local.get 0
                  local.get 4
                  i32.load offset=12
                  call_indirect (type 4)
                  i32.eqz
                  br_if 0 (;@7;)
                  i32.const 1
                  local.set 0
                  br 4 (;@3;)
                end
                local.get 7
                i32.load8_s
                i32.const -65
                i32.le_s
                br_if 5 (;@1;)
              end
              local.get 1
              local.get 0
              i32.add
              local.set 1
              local.get 2
              local.get 0
              i32.sub
              local.tee 2
              br_if 0 (;@5;)
            end
          end
          i32.const 0
          local.set 0
        end
        local.get 3
        i32.const 16
        i32.add
        global.set $__stack_pointer
        local.get 0
        return
      end
      local.get 1
      local.get 2
      i32.const 0
      local.get 0
      i32.const 1051364
      call $_ZN4core3str16slice_error_fail17hc8f76ed1263d932aE
      unreachable
    end
    local.get 1
    local.get 2
    local.get 0
    local.get 2
    i32.const 1051380
    call $_ZN4core3str16slice_error_fail17hc8f76ed1263d932aE
    unreachable)
  (func $_ZN4core5slice6memchr19memchr_general_case17h6bcaafb5e36b0dd0E (type 10) (param i32 i32 i32 i32)
    (local i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 3
            i32.add
            i32.const -4
            i32.and
            local.get 2
            i32.sub
            local.tee 4
            i32.eqz
            br_if 0 (;@4;)
            local.get 3
            local.get 4
            local.get 4
            local.get 3
            i32.gt_u
            select
            local.tee 4
            i32.eqz
            br_if 0 (;@4;)
            i32.const 0
            local.set 5
            local.get 1
            i32.const 255
            i32.and
            local.set 6
            i32.const 1
            local.set 7
            loop  ;; label = @5
              local.get 2
              local.get 5
              i32.add
              i32.load8_u
              local.get 6
              i32.eq
              br_if 4 (;@1;)
              local.get 4
              local.get 5
              i32.const 1
              i32.add
              local.tee 5
              i32.ne
              br_if 0 (;@5;)
            end
            local.get 4
            local.get 3
            i32.const -8
            i32.add
            local.tee 8
            i32.gt_u
            br_if 2 (;@2;)
            br 1 (;@3;)
          end
          local.get 3
          i32.const -8
          i32.add
          local.set 8
          i32.const 0
          local.set 4
        end
        local.get 1
        i32.const 255
        i32.and
        i32.const 16843009
        i32.mul
        local.set 5
        block  ;; label = @3
          loop  ;; label = @4
            local.get 2
            local.get 4
            i32.add
            local.tee 6
            i32.load
            local.get 5
            i32.xor
            local.tee 7
            i32.const -1
            i32.xor
            local.get 7
            i32.const -16843009
            i32.add
            i32.and
            local.get 6
            i32.const 4
            i32.add
            i32.load
            local.get 5
            i32.xor
            local.tee 6
            i32.const -1
            i32.xor
            local.get 6
            i32.const -16843009
            i32.add
            i32.and
            i32.or
            i32.const -2139062144
            i32.and
            br_if 1 (;@3;)
            local.get 4
            i32.const 8
            i32.add
            local.tee 4
            local.get 8
            i32.le_u
            br_if 0 (;@4;)
          end
        end
        local.get 4
        local.get 3
        i32.le_u
        br_if 0 (;@2;)
        local.get 4
        local.get 3
        i32.const 1051780
        call $_ZN4core5slice5index26slice_start_index_len_fail17hf52653f30012ff28E
        unreachable
      end
      block  ;; label = @2
        local.get 4
        local.get 3
        i32.eq
        br_if 0 (;@2;)
        local.get 3
        local.get 4
        i32.sub
        local.set 8
        local.get 2
        local.get 4
        i32.add
        local.set 6
        i32.const 0
        local.set 5
        local.get 1
        i32.const 255
        i32.and
        local.set 7
        block  ;; label = @3
          loop  ;; label = @4
            local.get 6
            local.get 5
            i32.add
            i32.load8_u
            local.get 7
            i32.eq
            br_if 1 (;@3;)
            local.get 8
            local.get 5
            i32.const 1
            i32.add
            local.tee 5
            i32.eq
            br_if 2 (;@2;)
            br 0 (;@4;)
          end
        end
        local.get 4
        local.get 5
        i32.add
        local.set 5
        i32.const 1
        local.set 7
        br 1 (;@1;)
      end
      i32.const 0
      local.set 7
    end
    local.get 0
    local.get 5
    i32.store offset=4
    local.get 0
    local.get 7
    i32.store)
  (func $_ZN4core3str16slice_error_fail17hc8f76ed1263d932aE (type 15) (param i32 i32 i32 i32 i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 112
    i32.sub
    local.tee 5
    global.set $__stack_pointer
    local.get 5
    local.get 3
    i32.store offset=12
    local.get 5
    local.get 2
    i32.store offset=8
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 1
                  i32.const 257
                  i32.lt_u
                  br_if 0 (;@7;)
                  i32.const 0
                  local.set 6
                  loop  ;; label = @8
                    block  ;; label = @9
                      local.get 0
                      local.get 6
                      i32.add
                      local.tee 7
                      i32.const 256
                      i32.add
                      i32.load8_s
                      i32.const -65
                      i32.le_s
                      br_if 0 (;@9;)
                      local.get 6
                      i32.const 256
                      i32.add
                      local.set 7
                      br 5 (;@4;)
                    end
                    block  ;; label = @9
                      local.get 7
                      i32.const 255
                      i32.add
                      i32.load8_s
                      i32.const -65
                      i32.le_s
                      br_if 0 (;@9;)
                      local.get 6
                      i32.const 255
                      i32.add
                      local.set 7
                      br 5 (;@4;)
                    end
                    local.get 7
                    i32.const 254
                    i32.add
                    i32.load8_s
                    i32.const -65
                    i32.gt_s
                    br_if 3 (;@5;)
                    local.get 7
                    i32.const 253
                    i32.add
                    i32.load8_s
                    i32.const -65
                    i32.gt_s
                    br_if 2 (;@6;)
                    local.get 6
                    i32.const -4
                    i32.add
                    local.tee 6
                    i32.const -256
                    i32.ne
                    br_if 0 (;@8;)
                  end
                  i32.const 0
                  local.set 6
                  br 4 (;@3;)
                end
                local.get 5
                local.get 1
                i32.store offset=20
                local.get 5
                local.get 0
                i32.store offset=16
                local.get 5
                i32.const 1051144
                i32.store offset=24
                i32.const 0
                local.set 7
                br 4 (;@2;)
              end
              local.get 6
              i32.const 253
              i32.add
              local.set 7
              br 1 (;@4;)
            end
            local.get 6
            i32.const 254
            i32.add
            local.set 7
          end
          block  ;; label = @4
            local.get 7
            local.get 1
            i32.lt_u
            br_if 0 (;@4;)
            local.get 1
            local.set 6
            local.get 7
            local.get 1
            i32.eq
            br_if 1 (;@3;)
            br 3 (;@1;)
          end
          local.get 0
          local.get 7
          i32.add
          i32.load8_s
          i32.const -65
          i32.le_s
          br_if 2 (;@1;)
          local.get 7
          local.set 6
        end
        local.get 5
        local.get 6
        i32.store offset=20
        local.get 5
        local.get 0
        i32.store offset=16
        local.get 5
        i32.const 1052000
        i32.store offset=24
        i32.const 5
        local.set 7
      end
      local.get 5
      local.get 7
      i32.store offset=28
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    local.get 2
                    local.get 1
                    i32.gt_u
                    local.tee 7
                    br_if 0 (;@8;)
                    local.get 3
                    local.get 1
                    i32.gt_u
                    br_if 0 (;@8;)
                    local.get 2
                    local.get 3
                    i32.gt_u
                    br_if 1 (;@7;)
                    local.get 2
                    i32.eqz
                    br_if 2 (;@6;)
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 2
                        local.get 1
                        i32.lt_u
                        br_if 0 (;@10;)
                        local.get 1
                        local.get 2
                        i32.ne
                        br_if 1 (;@9;)
                        br 4 (;@6;)
                      end
                      local.get 0
                      local.get 2
                      i32.add
                      i32.load8_s
                      i32.const -65
                      i32.gt_s
                      br_if 3 (;@6;)
                    end
                    local.get 5
                    local.get 2
                    i32.store offset=32
                    local.get 2
                    local.set 3
                    br 3 (;@5;)
                  end
                  local.get 5
                  local.get 2
                  local.get 3
                  local.get 7
                  select
                  i32.store offset=40
                  local.get 5
                  i32.const 48
                  i32.add
                  i32.const 20
                  i32.add
                  i32.const 3
                  i32.store
                  local.get 5
                  i32.const 72
                  i32.add
                  i32.const 20
                  i32.add
                  i32.const 39
                  i32.store
                  local.get 5
                  i32.const 84
                  i32.add
                  i32.const 39
                  i32.store
                  local.get 5
                  i64.const 3
                  i64.store offset=52 align=4
                  local.get 5
                  i32.const 1052040
                  i32.store offset=48
                  local.get 5
                  i32.const 4
                  i32.store offset=76
                  local.get 5
                  local.get 5
                  i32.const 72
                  i32.add
                  i32.store offset=64
                  local.get 5
                  local.get 5
                  i32.const 24
                  i32.add
                  i32.store offset=88
                  local.get 5
                  local.get 5
                  i32.const 16
                  i32.add
                  i32.store offset=80
                  local.get 5
                  local.get 5
                  i32.const 40
                  i32.add
                  i32.store offset=72
                  local.get 5
                  i32.const 48
                  i32.add
                  local.get 4
                  call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
                  unreachable
                end
                local.get 5
                i32.const 100
                i32.add
                i32.const 39
                i32.store
                local.get 5
                i32.const 72
                i32.add
                i32.const 20
                i32.add
                i32.const 39
                i32.store
                local.get 5
                i32.const 84
                i32.add
                i32.const 4
                i32.store
                local.get 5
                i32.const 48
                i32.add
                i32.const 20
                i32.add
                i32.const 4
                i32.store
                local.get 5
                i64.const 4
                i64.store offset=52 align=4
                local.get 5
                i32.const 1052100
                i32.store offset=48
                local.get 5
                i32.const 4
                i32.store offset=76
                local.get 5
                local.get 5
                i32.const 72
                i32.add
                i32.store offset=64
                local.get 5
                local.get 5
                i32.const 24
                i32.add
                i32.store offset=96
                local.get 5
                local.get 5
                i32.const 16
                i32.add
                i32.store offset=88
                local.get 5
                local.get 5
                i32.const 12
                i32.add
                i32.store offset=80
                local.get 5
                local.get 5
                i32.const 8
                i32.add
                i32.store offset=72
                local.get 5
                i32.const 48
                i32.add
                local.get 4
                call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
                unreachable
              end
              local.get 5
              local.get 3
              i32.store offset=32
              local.get 3
              i32.eqz
              br_if 1 (;@4;)
            end
            loop  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 3
                  local.get 1
                  i32.lt_u
                  local.tee 7
                  br_if 0 (;@7;)
                  local.get 1
                  local.get 3
                  i32.eq
                  br_if 5 (;@2;)
                  br 1 (;@6;)
                end
                local.get 0
                local.get 3
                i32.add
                local.tee 6
                i32.load8_s
                i32.const -64
                i32.lt_s
                br_if 0 (;@6;)
                block  ;; label = @7
                  block  ;; label = @8
                    local.get 7
                    br_if 0 (;@8;)
                    local.get 1
                    local.get 3
                    i32.ne
                    br_if 1 (;@7;)
                    br 6 (;@2;)
                  end
                  local.get 6
                  i32.load8_s
                  i32.const -65
                  i32.gt_s
                  br_if 4 (;@3;)
                end
                local.get 0
                local.get 1
                local.get 3
                local.get 1
                local.get 4
                call $_ZN4core3str16slice_error_fail17hc8f76ed1263d932aE
                unreachable
              end
              local.get 3
              i32.const -1
              i32.add
              local.tee 3
              br_if 0 (;@5;)
            end
          end
          i32.const 0
          local.set 3
        end
        local.get 3
        local.get 1
        i32.eq
        br_if 0 (;@2;)
        local.get 0
        local.get 3
        i32.add
        local.tee 0
        i32.load8_s
        local.tee 7
        i32.const 255
        i32.and
        local.set 6
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 7
                i32.const -1
                i32.gt_s
                br_if 0 (;@6;)
                local.get 0
                i32.load8_u offset=1
                i32.const 63
                i32.and
                local.set 1
                local.get 7
                i32.const 31
                i32.and
                local.set 2
                local.get 6
                i32.const 223
                i32.gt_u
                br_if 1 (;@5;)
                local.get 2
                i32.const 6
                i32.shl
                local.get 1
                i32.or
                local.set 6
                br 2 (;@4;)
              end
              local.get 5
              local.get 6
              i32.store offset=36
              i32.const 1
              local.set 7
              br 2 (;@3;)
            end
            local.get 1
            i32.const 6
            i32.shl
            local.get 0
            i32.load8_u offset=2
            i32.const 63
            i32.and
            i32.or
            local.set 6
            block  ;; label = @5
              local.get 7
              i32.const 255
              i32.and
              i32.const 240
              i32.ge_u
              br_if 0 (;@5;)
              local.get 6
              local.get 2
              i32.const 12
              i32.shl
              i32.or
              local.set 6
              br 1 (;@4;)
            end
            local.get 6
            i32.const 6
            i32.shl
            local.get 0
            i32.load8_u offset=3
            i32.const 63
            i32.and
            i32.or
            local.get 2
            i32.const 18
            i32.shl
            i32.const 1835008
            i32.and
            i32.or
            local.tee 6
            i32.const 1114112
            i32.eq
            br_if 2 (;@2;)
          end
          local.get 5
          local.get 6
          i32.store offset=36
          i32.const 1
          local.set 7
          local.get 6
          i32.const 128
          i32.lt_u
          br_if 0 (;@3;)
          i32.const 2
          local.set 7
          local.get 6
          i32.const 2048
          i32.lt_u
          br_if 0 (;@3;)
          i32.const 3
          i32.const 4
          local.get 6
          i32.const 65536
          i32.lt_u
          select
          local.set 7
        end
        local.get 5
        local.get 3
        i32.store offset=40
        local.get 5
        local.get 7
        local.get 3
        i32.add
        i32.store offset=44
        local.get 5
        i32.const 48
        i32.add
        i32.const 20
        i32.add
        i32.const 5
        i32.store
        local.get 5
        i32.const 108
        i32.add
        i32.const 39
        i32.store
        local.get 5
        i32.const 100
        i32.add
        i32.const 39
        i32.store
        local.get 5
        i32.const 72
        i32.add
        i32.const 20
        i32.add
        i32.const 40
        i32.store
        local.get 5
        i32.const 84
        i32.add
        i32.const 41
        i32.store
        local.get 5
        i64.const 5
        i64.store offset=52 align=4
        local.get 5
        i32.const 1052184
        i32.store offset=48
        local.get 5
        i32.const 4
        i32.store offset=76
        local.get 5
        local.get 5
        i32.const 72
        i32.add
        i32.store offset=64
        local.get 5
        local.get 5
        i32.const 24
        i32.add
        i32.store offset=104
        local.get 5
        local.get 5
        i32.const 16
        i32.add
        i32.store offset=96
        local.get 5
        local.get 5
        i32.const 40
        i32.add
        i32.store offset=88
        local.get 5
        local.get 5
        i32.const 36
        i32.add
        i32.store offset=80
        local.get 5
        local.get 5
        i32.const 32
        i32.add
        i32.store offset=72
        local.get 5
        i32.const 48
        i32.add
        local.get 4
        call $_ZN4core9panicking9panic_fmt17h4ca53049f45e3264E
        unreachable
      end
      i32.const 1051156
      i32.const 43
      local.get 4
      call $_ZN4core9panicking5panic17h35778e3c18266c93E
      unreachable
    end
    local.get 0
    local.get 1
    i32.const 0
    local.get 7
    i32.const 1051984
    call $_ZN4core3str16slice_error_fail17hc8f76ed1263d932aE
    unreachable)
  (func $_ZN4core3fmt8builders11DebugStruct6finish17hafa48911b1175192E (type 13) (param i32) (result i32)
    (local i32 i32)
    local.get 0
    i32.load8_u offset=4
    local.set 1
    block  ;; label = @1
      local.get 0
      i32.load8_u offset=5
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 255
      i32.and
      local.set 2
      i32.const 1
      local.set 1
      block  ;; label = @2
        local.get 2
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 0
          i32.load
          local.tee 1
          i32.load8_u
          i32.const 4
          i32.and
          br_if 0 (;@3;)
          local.get 1
          i32.load offset=24
          i32.const 1051407
          i32.const 2
          local.get 1
          i32.const 28
          i32.add
          i32.load
          i32.load offset=12
          call_indirect (type 4)
          local.set 1
          br 1 (;@2;)
        end
        local.get 1
        i32.load offset=24
        i32.const 1051406
        i32.const 1
        local.get 1
        i32.const 28
        i32.add
        i32.load
        i32.load offset=12
        call_indirect (type 4)
        local.set 1
      end
      local.get 0
      local.get 1
      i32.store8 offset=4
    end
    local.get 1
    i32.const 255
    i32.and
    i32.const 0
    i32.ne)
  (func $_ZN4core3fmt8builders10DebugTuple5field17h694ff79a62cd0513E (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i64 i64)
    global.get $__stack_pointer
    i32.const 64
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.load8_u offset=8
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load offset=4
        local.set 4
        i32.const 1
        local.set 5
        br 1 (;@1;)
      end
      local.get 0
      i32.load offset=4
      local.set 4
      block  ;; label = @2
        local.get 0
        i32.load
        local.tee 6
        i32.load8_u
        i32.const 4
        i32.and
        br_if 0 (;@2;)
        i32.const 1
        local.set 5
        local.get 6
        i32.load offset=24
        i32.const 1051401
        i32.const 1051411
        local.get 4
        select
        i32.const 2
        i32.const 1
        local.get 4
        select
        local.get 6
        i32.const 28
        i32.add
        i32.load
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        local.get 1
        local.get 6
        local.get 2
        i32.load offset=12
        call_indirect (type 2)
        local.set 5
        br 1 (;@1;)
      end
      block  ;; label = @2
        local.get 4
        br_if 0 (;@2;)
        local.get 6
        i32.load offset=24
        i32.const 1051409
        i32.const 2
        local.get 6
        i32.const 28
        i32.add
        i32.load
        i32.load offset=12
        call_indirect (type 4)
        i32.eqz
        br_if 0 (;@2;)
        i32.const 1
        local.set 5
        i32.const 0
        local.set 4
        br 1 (;@1;)
      end
      i32.const 1
      local.set 5
      local.get 3
      i32.const 1
      i32.store8 offset=23
      local.get 3
      i32.const 52
      i32.add
      i32.const 1051304
      i32.store
      local.get 3
      i32.const 16
      i32.add
      local.get 3
      i32.const 23
      i32.add
      i32.store
      local.get 3
      local.get 6
      i64.load offset=24 align=4
      i64.store offset=8
      local.get 6
      i64.load offset=8 align=4
      local.set 7
      local.get 6
      i64.load offset=16 align=4
      local.set 8
      local.get 3
      local.get 6
      i32.load8_u offset=32
      i32.store8 offset=56
      local.get 3
      local.get 8
      i64.store offset=40
      local.get 3
      local.get 7
      i64.store offset=32
      local.get 3
      local.get 6
      i64.load align=4
      i64.store offset=24
      local.get 3
      local.get 3
      i32.const 8
      i32.add
      i32.store offset=48
      local.get 1
      local.get 3
      i32.const 24
      i32.add
      local.get 2
      i32.load offset=12
      call_indirect (type 2)
      br_if 0 (;@1;)
      local.get 3
      i32.load offset=48
      i32.const 1051399
      i32.const 2
      local.get 3
      i32.load offset=52
      i32.load offset=12
      call_indirect (type 4)
      local.set 5
    end
    local.get 0
    local.get 5
    i32.store8 offset=8
    local.get 0
    local.get 4
    i32.const 1
    i32.add
    i32.store offset=4
    local.get 3
    i32.const 64
    i32.add
    global.set $__stack_pointer
    local.get 0)
  (func $_ZN4core3fmt8builders10DebugTuple6finish17h045041801e49228aE (type 13) (param i32) (result i32)
    (local i32 i32 i32)
    local.get 0
    i32.load8_u offset=8
    local.set 1
    block  ;; label = @1
      local.get 0
      i32.load offset=4
      local.tee 2
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      i32.const 255
      i32.and
      local.set 3
      i32.const 1
      local.set 1
      block  ;; label = @2
        local.get 3
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 2
          i32.const 1
          i32.ne
          br_if 0 (;@3;)
          local.get 0
          i32.load8_u offset=9
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          i32.load
          local.tee 3
          i32.load8_u
          i32.const 4
          i32.and
          br_if 0 (;@3;)
          i32.const 1
          local.set 1
          local.get 3
          i32.load offset=24
          i32.const 1051412
          i32.const 1
          local.get 3
          i32.const 28
          i32.add
          i32.load
          i32.load offset=12
          call_indirect (type 4)
          br_if 1 (;@2;)
        end
        local.get 0
        i32.load
        local.tee 1
        i32.load offset=24
        i32.const 1051413
        i32.const 1
        local.get 1
        i32.const 28
        i32.add
        i32.load
        i32.load offset=12
        call_indirect (type 4)
        local.set 1
      end
      local.get 0
      local.get 1
      i32.store8 offset=8
    end
    local.get 1
    i32.const 255
    i32.and
    i32.const 0
    i32.ne)
  (func $_ZN4core3fmt9Formatter12pad_integral17hc91f100cd8a143afE (type 16) (param i32 i32 i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.eqz
        br_if 0 (;@2;)
        i32.const 43
        i32.const 1114112
        local.get 0
        i32.load
        local.tee 6
        i32.const 1
        i32.and
        local.tee 1
        select
        local.set 7
        local.get 1
        local.get 5
        i32.add
        local.set 8
        br 1 (;@1;)
      end
      local.get 5
      i32.const 1
      i32.add
      local.set 8
      local.get 0
      i32.load
      local.set 6
      i32.const 45
      local.set 7
    end
    block  ;; label = @1
      block  ;; label = @2
        local.get 6
        i32.const 4
        i32.and
        br_if 0 (;@2;)
        i32.const 0
        local.set 2
        br 1 (;@1;)
      end
      block  ;; label = @2
        block  ;; label = @3
          local.get 3
          br_if 0 (;@3;)
          i32.const 0
          local.set 9
          br 1 (;@2;)
        end
        local.get 3
        i32.const 3
        i32.and
        local.set 10
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.const -1
            i32.add
            i32.const 3
            i32.ge_u
            br_if 0 (;@4;)
            i32.const 0
            local.set 9
            local.get 2
            local.set 1
            br 1 (;@3;)
          end
          i32.const 0
          local.set 9
          i32.const 0
          local.get 3
          i32.const -4
          i32.and
          i32.sub
          local.set 11
          local.get 2
          local.set 1
          loop  ;; label = @4
            local.get 9
            local.get 1
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.get 1
            i32.const 1
            i32.add
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.get 1
            i32.const 2
            i32.add
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.get 1
            i32.const 3
            i32.add
            i32.load8_s
            i32.const -65
            i32.gt_s
            i32.add
            local.set 9
            local.get 1
            i32.const 4
            i32.add
            local.set 1
            local.get 11
            i32.const 4
            i32.add
            local.tee 11
            br_if 0 (;@4;)
          end
        end
        local.get 10
        i32.eqz
        br_if 0 (;@2;)
        loop  ;; label = @3
          local.get 9
          local.get 1
          i32.load8_s
          i32.const -65
          i32.gt_s
          i32.add
          local.set 9
          local.get 1
          i32.const 1
          i32.add
          local.set 1
          local.get 10
          i32.const -1
          i32.add
          local.tee 10
          br_if 0 (;@3;)
        end
      end
      local.get 9
      local.get 8
      i32.add
      local.set 8
    end
    i32.const 1
    local.set 1
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i32.load offset=8
        i32.const 1
        i32.eq
        br_if 0 (;@2;)
        local.get 0
        local.get 7
        local.get 2
        local.get 3
        call $_ZN4core3fmt9Formatter12pad_integral12write_prefix17h2a3ecc823ff49259E
        br_if 1 (;@1;)
        local.get 0
        i32.load offset=24
        local.get 4
        local.get 5
        local.get 0
        i32.const 28
        i32.add
        i32.load
        i32.load offset=12
        call_indirect (type 4)
        return
      end
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 0
                i32.const 12
                i32.add
                i32.load
                local.tee 9
                local.get 8
                i32.le_u
                br_if 0 (;@6;)
                local.get 6
                i32.const 8
                i32.and
                br_if 4 (;@2;)
                i32.const 0
                local.set 1
                local.get 9
                local.get 8
                i32.sub
                local.tee 10
                local.set 8
                i32.const 1
                local.get 0
                i32.load8_u offset=32
                local.tee 9
                local.get 9
                i32.const 3
                i32.eq
                select
                i32.const 3
                i32.and
                br_table 3 (;@3;) 1 (;@5;) 2 (;@4;) 3 (;@3;)
              end
              local.get 0
              local.get 7
              local.get 2
              local.get 3
              call $_ZN4core3fmt9Formatter12pad_integral12write_prefix17h2a3ecc823ff49259E
              br_if 4 (;@1;)
              local.get 0
              i32.load offset=24
              local.get 4
              local.get 5
              local.get 0
              i32.const 28
              i32.add
              i32.load
              i32.load offset=12
              call_indirect (type 4)
              return
            end
            i32.const 0
            local.set 8
            local.get 10
            local.set 1
            br 1 (;@3;)
          end
          local.get 10
          i32.const 1
          i32.shr_u
          local.set 1
          local.get 10
          i32.const 1
          i32.add
          i32.const 1
          i32.shr_u
          local.set 8
        end
        local.get 1
        i32.const 1
        i32.add
        local.set 1
        local.get 0
        i32.const 28
        i32.add
        i32.load
        local.set 10
        local.get 0
        i32.load offset=4
        local.set 9
        local.get 0
        i32.load offset=24
        local.set 11
        block  ;; label = @3
          loop  ;; label = @4
            local.get 1
            i32.const -1
            i32.add
            local.tee 1
            i32.eqz
            br_if 1 (;@3;)
            local.get 11
            local.get 9
            local.get 10
            i32.load offset=16
            call_indirect (type 2)
            i32.eqz
            br_if 0 (;@4;)
          end
          i32.const 1
          return
        end
        i32.const 1
        local.set 1
        local.get 9
        i32.const 1114112
        i32.eq
        br_if 1 (;@1;)
        local.get 0
        local.get 7
        local.get 2
        local.get 3
        call $_ZN4core3fmt9Formatter12pad_integral12write_prefix17h2a3ecc823ff49259E
        br_if 1 (;@1;)
        local.get 0
        i32.load offset=24
        local.get 4
        local.get 5
        local.get 0
        i32.load offset=28
        i32.load offset=12
        call_indirect (type 4)
        br_if 1 (;@1;)
        local.get 0
        i32.load offset=28
        local.set 10
        local.get 0
        i32.load offset=24
        local.set 11
        i32.const 0
        local.set 1
        block  ;; label = @3
          loop  ;; label = @4
            block  ;; label = @5
              local.get 8
              local.get 1
              i32.ne
              br_if 0 (;@5;)
              local.get 8
              local.set 1
              br 2 (;@3;)
            end
            local.get 1
            i32.const 1
            i32.add
            local.set 1
            local.get 11
            local.get 9
            local.get 10
            i32.load offset=16
            call_indirect (type 2)
            i32.eqz
            br_if 0 (;@4;)
          end
          local.get 1
          i32.const -1
          i32.add
          local.set 1
        end
        local.get 1
        local.get 8
        i32.lt_u
        local.set 1
        br 1 (;@1;)
      end
      local.get 0
      i32.load offset=4
      local.set 6
      local.get 0
      i32.const 48
      i32.store offset=4
      local.get 0
      i32.load8_u offset=32
      local.set 12
      i32.const 1
      local.set 1
      local.get 0
      i32.const 1
      i32.store8 offset=32
      local.get 0
      local.get 7
      local.get 2
      local.get 3
      call $_ZN4core3fmt9Formatter12pad_integral12write_prefix17h2a3ecc823ff49259E
      br_if 0 (;@1;)
      i32.const 0
      local.set 1
      local.get 9
      local.get 8
      i32.sub
      local.tee 10
      local.set 3
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            i32.const 1
            local.get 0
            i32.load8_u offset=32
            local.tee 9
            local.get 9
            i32.const 3
            i32.eq
            select
            i32.const 3
            i32.and
            br_table 2 (;@2;) 0 (;@4;) 1 (;@3;) 2 (;@2;)
          end
          i32.const 0
          local.set 3
          local.get 10
          local.set 1
          br 1 (;@2;)
        end
        local.get 10
        i32.const 1
        i32.shr_u
        local.set 1
        local.get 10
        i32.const 1
        i32.add
        i32.const 1
        i32.shr_u
        local.set 3
      end
      local.get 1
      i32.const 1
      i32.add
      local.set 1
      local.get 0
      i32.const 28
      i32.add
      i32.load
      local.set 10
      local.get 0
      i32.load offset=4
      local.set 9
      local.get 0
      i32.load offset=24
      local.set 11
      block  ;; label = @2
        loop  ;; label = @3
          local.get 1
          i32.const -1
          i32.add
          local.tee 1
          i32.eqz
          br_if 1 (;@2;)
          local.get 11
          local.get 9
          local.get 10
          i32.load offset=16
          call_indirect (type 2)
          i32.eqz
          br_if 0 (;@3;)
        end
        i32.const 1
        return
      end
      i32.const 1
      local.set 1
      local.get 9
      i32.const 1114112
      i32.eq
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=24
      local.get 4
      local.get 5
      local.get 0
      i32.load offset=28
      i32.load offset=12
      call_indirect (type 4)
      br_if 0 (;@1;)
      local.get 0
      i32.load offset=28
      local.set 1
      local.get 0
      i32.load offset=24
      local.set 11
      i32.const 0
      local.set 10
      block  ;; label = @2
        loop  ;; label = @3
          local.get 3
          local.get 10
          i32.eq
          br_if 1 (;@2;)
          local.get 10
          i32.const 1
          i32.add
          local.set 10
          local.get 11
          local.get 9
          local.get 1
          i32.load offset=16
          call_indirect (type 2)
          i32.eqz
          br_if 0 (;@3;)
        end
        i32.const 1
        local.set 1
        local.get 10
        i32.const -1
        i32.add
        local.get 3
        i32.lt_u
        br_if 1 (;@1;)
      end
      local.get 0
      local.get 12
      i32.store8 offset=32
      local.get 0
      local.get 6
      i32.store offset=4
      i32.const 0
      return
    end
    local.get 1)
  (func $_ZN4core3fmt5Write10write_char17he7b2aa7a66144b54E (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 2
    i32.const 0
    i32.store offset=12
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.const 128
            i32.lt_u
            br_if 0 (;@4;)
            local.get 1
            i32.const 2048
            i32.lt_u
            br_if 1 (;@3;)
            local.get 1
            i32.const 65536
            i32.ge_u
            br_if 2 (;@2;)
            local.get 2
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=14
            local.get 2
            local.get 1
            i32.const 12
            i32.shr_u
            i32.const 224
            i32.or
            i32.store8 offset=12
            local.get 2
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=13
            i32.const 3
            local.set 1
            br 3 (;@1;)
          end
          local.get 2
          local.get 1
          i32.store8 offset=12
          i32.const 1
          local.set 1
          br 2 (;@1;)
        end
        local.get 2
        local.get 1
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=13
        local.get 2
        local.get 1
        i32.const 6
        i32.shr_u
        i32.const 192
        i32.or
        i32.store8 offset=12
        i32.const 2
        local.set 1
        br 1 (;@1;)
      end
      local.get 2
      local.get 1
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=15
      local.get 2
      local.get 1
      i32.const 18
      i32.shr_u
      i32.const 240
      i32.or
      i32.store8 offset=12
      local.get 2
      local.get 1
      i32.const 6
      i32.shr_u
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=14
      local.get 2
      local.get 1
      i32.const 12
      i32.shr_u
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=13
      i32.const 4
      local.set 1
    end
    local.get 0
    local.get 2
    i32.const 12
    i32.add
    local.get 1
    call $_ZN68_$LT$core..fmt..builders..PadAdapter$u20$as$u20$core..fmt..Write$GT$9write_str17h7ca5dec47fadd416E
    local.set 1
    local.get 2
    i32.const 16
    i32.add
    global.set $__stack_pointer
    local.get 1)
  (func $_ZN4core3fmt5Write9write_fmt17hb90aec2972b27b32E (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 2
    local.get 0
    i32.store offset=4
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=8
    local.get 2
    i32.const 4
    i32.add
    i32.const 1051664
    local.get 2
    i32.const 8
    i32.add
    call $_ZN4core3fmt5write17h8874ba14b0fd43feE
    local.set 1
    local.get 2
    i32.const 32
    i32.add
    global.set $__stack_pointer
    local.get 1)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_str17h47b32869691fa439E (type 4) (param i32 i32 i32) (result i32)
    local.get 0
    i32.load
    local.get 1
    local.get 2
    call $_ZN68_$LT$core..fmt..builders..PadAdapter$u20$as$u20$core..fmt..Write$GT$9write_str17h7ca5dec47fadd416E)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$10write_char17h08b6b26387744865E (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 16
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 0
    i32.load
    local.set 0
    local.get 2
    i32.const 0
    i32.store offset=12
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.const 128
            i32.lt_u
            br_if 0 (;@4;)
            local.get 1
            i32.const 2048
            i32.lt_u
            br_if 1 (;@3;)
            local.get 1
            i32.const 65536
            i32.ge_u
            br_if 2 (;@2;)
            local.get 2
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=14
            local.get 2
            local.get 1
            i32.const 12
            i32.shr_u
            i32.const 224
            i32.or
            i32.store8 offset=12
            local.get 2
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=13
            i32.const 3
            local.set 1
            br 3 (;@1;)
          end
          local.get 2
          local.get 1
          i32.store8 offset=12
          i32.const 1
          local.set 1
          br 2 (;@1;)
        end
        local.get 2
        local.get 1
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=13
        local.get 2
        local.get 1
        i32.const 6
        i32.shr_u
        i32.const 192
        i32.or
        i32.store8 offset=12
        i32.const 2
        local.set 1
        br 1 (;@1;)
      end
      local.get 2
      local.get 1
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=15
      local.get 2
      local.get 1
      i32.const 18
      i32.shr_u
      i32.const 240
      i32.or
      i32.store8 offset=12
      local.get 2
      local.get 1
      i32.const 6
      i32.shr_u
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=14
      local.get 2
      local.get 1
      i32.const 12
      i32.shr_u
      i32.const 63
      i32.and
      i32.const 128
      i32.or
      i32.store8 offset=13
      i32.const 4
      local.set 1
    end
    local.get 0
    local.get 2
    i32.const 12
    i32.add
    local.get 1
    call $_ZN68_$LT$core..fmt..builders..PadAdapter$u20$as$u20$core..fmt..Write$GT$9write_str17h7ca5dec47fadd416E
    local.set 1
    local.get 2
    i32.const 16
    i32.add
    global.set $__stack_pointer
    local.get 1)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_fmt17h508b7c51f344920cE (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get $__stack_pointer
    i32.const 32
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 2
    local.get 0
    i32.load
    i32.store offset=4
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=8
    local.get 2
    i32.const 4
    i32.add
    i32.const 1051664
    local.get 2
    i32.const 8
    i32.add
    call $_ZN4core3fmt5write17h8874ba14b0fd43feE
    local.set 1
    local.get 2
    i32.const 32
    i32.add
    global.set $__stack_pointer
    local.get 1)
  (func $_ZN4core3fmt9Formatter12pad_integral12write_prefix17h2a3ecc823ff49259E (type 9) (param i32 i32 i32 i32) (result i32)
    (local i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.const 1114112
          i32.eq
          br_if 0 (;@3;)
          i32.const 1
          local.set 4
          local.get 0
          i32.load offset=24
          local.get 1
          local.get 0
          i32.const 28
          i32.add
          i32.load
          i32.load offset=16
          call_indirect (type 2)
          br_if 1 (;@2;)
        end
        local.get 2
        br_if 1 (;@1;)
        i32.const 0
        local.set 4
      end
      local.get 4
      return
    end
    local.get 0
    i32.load offset=24
    local.get 2
    local.get 3
    local.get 0
    i32.const 28
    i32.add
    i32.load
    i32.load offset=12
    call_indirect (type 4))
  (func $_ZN4core3fmt9Formatter9write_str17h755105c46b0f09f2E (type 4) (param i32 i32 i32) (result i32)
    local.get 0
    i32.load offset=24
    local.get 1
    local.get 2
    local.get 0
    i32.const 28
    i32.add
    i32.load
    i32.load offset=12
    call_indirect (type 4))
  (func $_ZN4core3fmt9Formatter15debug_lower_hex17h4a106312d74296a5E (type 13) (param i32) (result i32)
    local.get 0
    i32.load8_u
    i32.const 16
    i32.and
    i32.const 4
    i32.shr_u)
  (func $_ZN4core3fmt9Formatter15debug_upper_hex17hf60b1b844cd134fcE (type 13) (param i32) (result i32)
    local.get 0
    i32.load8_u
    i32.const 32
    i32.and
    i32.const 5
    i32.shr_u)
  (func $_ZN4core3fmt9Formatter12debug_struct17h043271df4222cde7E (type 17) (param i32 i32 i32) (result i64)
    i64.const 4294967296
    i64.const 0
    local.get 0
    i32.load offset=24
    local.get 1
    local.get 2
    local.get 0
    i32.const 28
    i32.add
    i32.load
    i32.load offset=12
    call_indirect (type 4)
    select
    local.get 0
    i64.extend_i32_u
    i64.or)
  (func $_ZN4core3fmt9Formatter11debug_tuple17h23c48a8e1a251645E (type 10) (param i32 i32 i32 i32)
    local.get 0
    local.get 1
    i32.load offset=24
    local.get 2
    local.get 3
    local.get 1
    i32.const 28
    i32.add
    i32.load
    i32.load offset=12
    call_indirect (type 4)
    i32.store8 offset=8
    local.get 0
    local.get 1
    i32.store
    local.get 0
    local.get 3
    i32.eqz
    i32.store8 offset=9
    local.get 0
    i32.const 0
    i32.store offset=4)
  (func $_ZN40_$LT$str$u20$as$u20$core..fmt..Debug$GT$3fmt17h7f08598322370188E (type 4) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64)
    i32.const 1
    local.set 3
    block  ;; label = @1
      block  ;; label = @2
        local.get 2
        i32.load offset=24
        local.tee 4
        i32.const 34
        local.get 2
        i32.const 28
        i32.add
        i32.load
        local.tee 5
        i32.load offset=16
        local.tee 6
        call_indirect (type 2)
        br_if 0 (;@2;)
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            br_if 0 (;@4;)
            i32.const 0
            local.set 7
            br 1 (;@3;)
          end
          local.get 0
          local.get 1
          i32.add
          local.set 8
          i32.const 0
          local.set 7
          local.get 0
          local.set 9
          local.get 0
          local.set 10
          i32.const 0
          local.set 11
          block  ;; label = @4
            loop  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 10
                  i32.load8_s
                  local.tee 2
                  i32.const -1
                  i32.le_s
                  br_if 0 (;@7;)
                  local.get 10
                  i32.const 1
                  i32.add
                  local.set 10
                  local.get 2
                  i32.const 255
                  i32.and
                  local.set 12
                  br 1 (;@6;)
                end
                local.get 10
                i32.load8_u offset=1
                i32.const 63
                i32.and
                local.set 13
                local.get 2
                i32.const 31
                i32.and
                local.set 3
                block  ;; label = @7
                  local.get 2
                  i32.const 255
                  i32.and
                  local.tee 2
                  i32.const 223
                  i32.gt_u
                  br_if 0 (;@7;)
                  local.get 3
                  i32.const 6
                  i32.shl
                  local.get 13
                  i32.or
                  local.set 12
                  local.get 10
                  i32.const 2
                  i32.add
                  local.set 10
                  br 1 (;@6;)
                end
                local.get 13
                i32.const 6
                i32.shl
                local.get 10
                i32.load8_u offset=2
                i32.const 63
                i32.and
                i32.or
                local.set 13
                block  ;; label = @7
                  local.get 2
                  i32.const 240
                  i32.ge_u
                  br_if 0 (;@7;)
                  local.get 13
                  local.get 3
                  i32.const 12
                  i32.shl
                  i32.or
                  local.set 12
                  local.get 10
                  i32.const 3
                  i32.add
                  local.set 10
                  br 1 (;@6;)
                end
                local.get 13
                i32.const 6
                i32.shl
                local.get 10
                i32.load8_u offset=3
                i32.const 63
                i32.and
                i32.or
                local.get 3
                i32.const 18
                i32.shl
                i32.const 1835008
                i32.and
                i32.or
                local.tee 12
                i32.const 1114112
                i32.eq
                br_if 2 (;@4;)
                local.get 10
                i32.const 4
                i32.add
                local.set 10
              end
              i32.const 116
              local.set 14
              i32.const 2
              local.set 2
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          block  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                local.get 12
                                i32.const -9
                                i32.add
                                br_table 6 (;@8;) 3 (;@11;) 1 (;@13;) 1 (;@13;) 2 (;@12;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 1 (;@13;) 4 (;@10;) 0 (;@14;)
                              end
                              local.get 12
                              i32.const 92
                              i32.eq
                              br_if 3 (;@10;)
                            end
                            block  ;; label = @13
                              local.get 12
                              call $_ZN4core7unicode12unicode_data15grapheme_extend6lookup17hdc4e1b1965751945E
                              br_if 0 (;@13;)
                              local.get 12
                              call $_ZN4core7unicode9printable12is_printable17h811be0e8fb84ecc2E
                              br_if 6 (;@7;)
                            end
                            local.get 12
                            i32.const 1
                            i32.or
                            i32.clz
                            i32.const 2
                            i32.shr_u
                            i32.const 7
                            i32.xor
                            i64.extend_i32_u
                            i64.const 21474836480
                            i64.or
                            local.set 15
                            i32.const 3
                            local.set 2
                            local.get 12
                            local.set 14
                            br 4 (;@8;)
                          end
                          i32.const 114
                          local.set 14
                          br 2 (;@9;)
                        end
                        i32.const 110
                        local.set 14
                        br 1 (;@9;)
                      end
                      local.get 12
                      local.set 14
                    end
                  end
                  local.get 11
                  local.get 7
                  i32.lt_u
                  br_if 1 (;@6;)
                  block  ;; label = @8
                    local.get 7
                    i32.eqz
                    br_if 0 (;@8;)
                    block  ;; label = @9
                      local.get 7
                      local.get 1
                      i32.lt_u
                      br_if 0 (;@9;)
                      local.get 7
                      local.get 1
                      i32.eq
                      br_if 1 (;@8;)
                      br 3 (;@6;)
                    end
                    local.get 0
                    local.get 7
                    i32.add
                    i32.load8_s
                    i32.const -64
                    i32.lt_s
                    br_if 2 (;@6;)
                  end
                  block  ;; label = @8
                    local.get 11
                    i32.eqz
                    br_if 0 (;@8;)
                    block  ;; label = @9
                      local.get 11
                      local.get 1
                      i32.lt_u
                      br_if 0 (;@9;)
                      local.get 11
                      local.get 1
                      i32.ne
                      br_if 3 (;@6;)
                      br 1 (;@8;)
                    end
                    local.get 0
                    local.get 11
                    i32.add
                    i32.load8_s
                    i32.const -65
                    i32.le_s
                    br_if 2 (;@6;)
                  end
                  block  ;; label = @8
                    local.get 4
                    local.get 0
                    local.get 7
                    i32.add
                    local.get 11
                    local.get 7
                    i32.sub
                    local.get 5
                    i32.load offset=12
                    call_indirect (type 4)
                    i32.eqz
                    br_if 0 (;@8;)
                    i32.const 1
                    return
                  end
                  loop  ;; label = @8
                    local.get 2
                    local.set 13
                    i32.const 1
                    local.set 3
                    i32.const 92
                    local.set 7
                    i32.const 1
                    local.set 2
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          block  ;; label = @12
                            block  ;; label = @13
                              block  ;; label = @14
                                local.get 13
                                br_table 2 (;@12;) 1 (;@13;) 5 (;@9;) 0 (;@14;) 2 (;@12;)
                              end
                              block  ;; label = @14
                                block  ;; label = @15
                                  block  ;; label = @16
                                    block  ;; label = @17
                                      local.get 15
                                      i64.const 32
                                      i64.shr_u
                                      i32.wrap_i64
                                      i32.const 255
                                      i32.and
                                      br_table 5 (;@12;) 6 (;@11;) 3 (;@14;) 0 (;@17;) 1 (;@16;) 2 (;@15;) 5 (;@12;)
                                    end
                                    local.get 15
                                    i64.const -1095216660481
                                    i64.and
                                    i64.const 8589934592
                                    i64.or
                                    local.set 15
                                    i32.const 3
                                    local.set 2
                                    i32.const 123
                                    local.set 7
                                    br 7 (;@9;)
                                  end
                                  local.get 15
                                  i64.const -1095216660481
                                  i64.and
                                  i64.const 12884901888
                                  i64.or
                                  local.set 15
                                  i32.const 3
                                  local.set 2
                                  i32.const 117
                                  local.set 7
                                  br 6 (;@9;)
                                end
                                local.get 15
                                i64.const -1095216660481
                                i64.and
                                i64.const 17179869184
                                i64.or
                                local.set 15
                                i32.const 3
                                local.set 2
                                br 5 (;@9;)
                              end
                              i32.const 48
                              i32.const 87
                              local.get 14
                              local.get 15
                              i32.wrap_i64
                              local.tee 2
                              i32.const 2
                              i32.shl
                              i32.shr_u
                              i32.const 15
                              i32.and
                              local.tee 7
                              i32.const 10
                              i32.lt_u
                              select
                              local.get 7
                              i32.add
                              local.set 7
                              local.get 2
                              i32.eqz
                              br_if 3 (;@10;)
                              local.get 15
                              i64.const -1
                              i64.add
                              i64.const 4294967295
                              i64.and
                              local.get 15
                              i64.const -4294967296
                              i64.and
                              i64.or
                              local.set 15
                              i32.const 3
                              local.set 2
                              br 4 (;@9;)
                            end
                            i32.const 0
                            local.set 2
                            local.get 14
                            local.set 7
                            br 3 (;@9;)
                          end
                          i32.const 1
                          local.set 2
                          block  ;; label = @12
                            local.get 12
                            i32.const 128
                            i32.lt_u
                            br_if 0 (;@12;)
                            i32.const 2
                            local.set 2
                            local.get 12
                            i32.const 2048
                            i32.lt_u
                            br_if 0 (;@12;)
                            i32.const 3
                            i32.const 4
                            local.get 12
                            i32.const 65536
                            i32.lt_u
                            select
                            local.set 2
                          end
                          local.get 2
                          local.get 11
                          i32.add
                          local.set 7
                          br 4 (;@7;)
                        end
                        local.get 15
                        i64.const -1095216660481
                        i64.and
                        local.set 15
                        i32.const 3
                        local.set 2
                        i32.const 125
                        local.set 7
                        br 1 (;@9;)
                      end
                      local.get 15
                      i64.const -1095216660481
                      i64.and
                      i64.const 4294967296
                      i64.or
                      local.set 15
                      i32.const 3
                      local.set 2
                    end
                    local.get 4
                    local.get 7
                    local.get 6
                    call_indirect (type 2)
                    i32.eqz
                    br_if 0 (;@8;)
                    br 6 (;@2;)
                  end
                end
                local.get 11
                local.get 9
                i32.sub
                local.get 10
                i32.add
                local.set 11
                local.get 10
                local.set 9
                local.get 10
                local.get 8
                i32.ne
                br_if 1 (;@5;)
                br 2 (;@4;)
              end
            end
            local.get 0
            local.get 1
            local.get 7
            local.get 11
            i32.const 1051716
            call $_ZN4core3str16slice_error_fail17hc8f76ed1263d932aE
            unreachable
          end
          block  ;; label = @4
            local.get 7
            br_if 0 (;@4;)
            i32.const 0
            local.set 7
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 7
            local.get 1
            i32.lt_u
            br_if 0 (;@4;)
            local.get 7
            local.get 1
            i32.eq
            br_if 1 (;@3;)
            br 3 (;@1;)
          end
          local.get 0
          local.get 7
          i32.add
          i32.load8_s
          i32.const -65
          i32.le_s
          br_if 2 (;@1;)
        end
        i32.const 1
        local.set 3
        local.get 4
        local.get 0
        local.get 7
        i32.add
        local.get 1
        local.get 7
        i32.sub
        local.get 5
        i32.load offset=12
        call_indirect (type 4)
        br_if 0 (;@2;)
        local.get 4
        i32.const 34
        local.get 6
        call_indirect (type 2)
        return
      end
      local.get 3
      return
    end
    local.get 0
    local.get 1
    local.get 7
    local.get 1
    i32.const 1051732
    call $_ZN4core3str16slice_error_fail17hc8f76ed1263d932aE
    unreachable)
  (func $_ZN4core7unicode12unicode_data15grapheme_extend6lookup17hdc4e1b1965751945E (type 13) (param i32) (result i32)
    (local i32 i32 i32 i32 i32)
    local.get 0
    i32.const 11
    i32.shl
    local.set 1
    i32.const 0
    local.set 2
    i32.const 32
    local.set 3
    i32.const 32
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        loop  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              local.get 3
              i32.const 1
              i32.shr_u
              local.get 2
              i32.add
              local.tee 3
              i32.const 2
              i32.shl
              i32.const 1053780
              i32.add
              i32.load
              i32.const 11
              i32.shl
              local.tee 5
              local.get 1
              i32.lt_u
              br_if 0 (;@5;)
              local.get 5
              local.get 1
              i32.eq
              br_if 3 (;@2;)
              local.get 3
              local.set 4
              br 1 (;@4;)
            end
            local.get 3
            i32.const 1
            i32.add
            local.set 2
          end
          local.get 4
          local.get 2
          i32.sub
          local.set 3
          local.get 4
          local.get 2
          i32.gt_u
          br_if 0 (;@3;)
          br 2 (;@1;)
        end
      end
      local.get 3
      i32.const 1
      i32.add
      local.set 2
    end
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 2
          i32.const 31
          i32.gt_u
          br_if 0 (;@3;)
          local.get 2
          i32.const 2
          i32.shl
          local.set 3
          i32.const 707
          local.set 4
          block  ;; label = @4
            local.get 2
            i32.const 31
            i32.eq
            br_if 0 (;@4;)
            local.get 3
            i32.const 1053784
            i32.add
            i32.load
            i32.const 21
            i32.shr_u
            local.set 4
          end
          i32.const 0
          local.set 5
          block  ;; label = @4
            local.get 2
            i32.const -1
            i32.add
            local.tee 1
            local.get 2
            i32.gt_u
            br_if 0 (;@4;)
            local.get 1
            i32.const 32
            i32.ge_u
            br_if 2 (;@2;)
            local.get 1
            i32.const 2
            i32.shl
            i32.const 1053780
            i32.add
            i32.load
            i32.const 2097151
            i32.and
            local.set 5
          end
          block  ;; label = @4
            local.get 4
            local.get 3
            i32.const 1053780
            i32.add
            i32.load
            i32.const 21
            i32.shr_u
            local.tee 2
            i32.const 1
            i32.add
            i32.eq
            br_if 0 (;@4;)
            local.get 0
            local.get 5
            i32.sub
            local.set 1
            local.get 2
            i32.const 707
            local.get 2
            i32.const 707
            i32.gt_u
            select
            local.set 3
            local.get 4
            i32.const -1
            i32.add
            local.set 5
            i32.const 0
            local.set 4
            loop  ;; label = @5
              local.get 3
              local.get 2
              i32.eq
              br_if 4 (;@1;)
              local.get 4
              local.get 2
              i32.const 1053908
              i32.add
              i32.load8_u
              i32.add
              local.tee 4
              local.get 1
              i32.gt_u
              br_if 1 (;@4;)
              local.get 5
              local.get 2
              i32.const 1
              i32.add
              local.tee 2
              i32.ne
              br_if 0 (;@5;)
            end
            local.get 5
            local.set 2
          end
          local.get 2
          i32.const 1
          i32.and
          return
        end
        local.get 2
        i32.const 32
        i32.const 1053724
        call $_ZN4core9panicking18panic_bounds_check17h859d2a727fd634e5E
        unreachable
      end
      local.get 1
      i32.const 32
      i32.const 1053756
      call $_ZN4core9panicking18panic_bounds_check17h859d2a727fd634e5E
      unreachable
    end
    local.get 3
    i32.const 707
    i32.const 1053740
    call $_ZN4core9panicking18panic_bounds_check17h859d2a727fd634e5E
    unreachable)
  (func $_ZN4core7unicode9printable12is_printable17h811be0e8fb84ecc2E (type 13) (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 0
                        i32.const 65536
                        i32.lt_u
                        br_if 0 (;@10;)
                        local.get 0
                        i32.const 131072
                        i32.lt_u
                        br_if 1 (;@9;)
                        local.get 0
                        i32.const 2097120
                        i32.and
                        i32.const 173792
                        i32.ne
                        local.get 0
                        i32.const -177977
                        i32.add
                        i32.const 6
                        i32.gt_u
                        i32.and
                        local.get 0
                        i32.const 2097150
                        i32.and
                        i32.const 178206
                        i32.ne
                        i32.and
                        local.get 0
                        i32.const -183970
                        i32.add
                        i32.const 13
                        i32.gt_u
                        i32.and
                        local.get 0
                        i32.const -191457
                        i32.add
                        i32.const 3102
                        i32.gt_u
                        i32.and
                        local.get 0
                        i32.const -195102
                        i32.add
                        i32.const 1505
                        i32.gt_u
                        i32.and
                        local.get 0
                        i32.const -201547
                        i32.add
                        i32.const 716212
                        i32.gt_u
                        i32.and
                        local.get 0
                        i32.const 918000
                        i32.lt_u
                        i32.and
                        local.set 1
                        br 8 (;@2;)
                      end
                      i32.const 1052296
                      local.set 2
                      i32.const 0
                      local.set 3
                      local.get 0
                      i32.const 8
                      i32.shr_u
                      i32.const 255
                      i32.and
                      local.set 4
                      loop  ;; label = @10
                        local.get 2
                        i32.const 2
                        i32.add
                        local.set 5
                        local.get 3
                        local.get 2
                        i32.load8_u offset=1
                        local.tee 1
                        i32.add
                        local.set 6
                        block  ;; label = @11
                          local.get 2
                          i32.load8_u
                          local.tee 2
                          local.get 4
                          i32.eq
                          br_if 0 (;@11;)
                          local.get 2
                          local.get 4
                          i32.gt_u
                          br_if 8 (;@3;)
                          local.get 6
                          local.set 3
                          local.get 5
                          local.set 2
                          local.get 5
                          i32.const 1052376
                          i32.ne
                          br_if 1 (;@10;)
                          br 8 (;@3;)
                        end
                        local.get 6
                        local.get 3
                        i32.lt_u
                        br_if 2 (;@8;)
                        local.get 6
                        i32.const 288
                        i32.gt_u
                        br_if 3 (;@7;)
                        local.get 3
                        i32.const 1052376
                        i32.add
                        local.set 2
                        block  ;; label = @11
                          loop  ;; label = @12
                            local.get 1
                            i32.eqz
                            br_if 1 (;@11;)
                            local.get 1
                            i32.const -1
                            i32.add
                            local.set 1
                            local.get 2
                            i32.load8_u
                            local.set 3
                            local.get 2
                            i32.const 1
                            i32.add
                            local.set 2
                            local.get 3
                            local.get 0
                            i32.const 255
                            i32.and
                            i32.ne
                            br_if 0 (;@12;)
                          end
                          i32.const 0
                          local.set 1
                          br 9 (;@2;)
                        end
                        local.get 6
                        local.set 3
                        local.get 5
                        local.set 2
                        local.get 5
                        i32.const 1052376
                        i32.ne
                        br_if 0 (;@10;)
                        br 7 (;@3;)
                      end
                    end
                    i32.const 1052967
                    local.set 2
                    i32.const 0
                    local.set 3
                    local.get 0
                    i32.const 8
                    i32.shr_u
                    i32.const 255
                    i32.and
                    local.set 4
                    loop  ;; label = @9
                      local.get 2
                      i32.const 2
                      i32.add
                      local.set 5
                      local.get 3
                      local.get 2
                      i32.load8_u offset=1
                      local.tee 1
                      i32.add
                      local.set 6
                      block  ;; label = @10
                        local.get 2
                        i32.load8_u
                        local.tee 2
                        local.get 4
                        i32.eq
                        br_if 0 (;@10;)
                        local.get 2
                        local.get 4
                        i32.gt_u
                        br_if 6 (;@4;)
                        local.get 6
                        local.set 3
                        local.get 5
                        local.set 2
                        local.get 5
                        i32.const 1053051
                        i32.ne
                        br_if 1 (;@9;)
                        br 6 (;@4;)
                      end
                      local.get 6
                      local.get 3
                      i32.lt_u
                      br_if 3 (;@6;)
                      local.get 6
                      i32.const 192
                      i32.gt_u
                      br_if 4 (;@5;)
                      local.get 3
                      i32.const 1053051
                      i32.add
                      local.set 2
                      block  ;; label = @10
                        loop  ;; label = @11
                          local.get 1
                          i32.eqz
                          br_if 1 (;@10;)
                          local.get 1
                          i32.const -1
                          i32.add
                          local.set 1
                          local.get 2
                          i32.load8_u
                          local.set 3
                          local.get 2
                          i32.const 1
                          i32.add
                          local.set 2
                          local.get 3
                          local.get 0
                          i32.const 255
                          i32.and
                          i32.ne
                          br_if 0 (;@11;)
                        end
                        i32.const 0
                        local.set 1
                        br 8 (;@2;)
                      end
                      local.get 6
                      local.set 3
                      local.get 5
                      local.set 2
                      local.get 5
                      i32.const 1053051
                      i32.ne
                      br_if 0 (;@9;)
                      br 5 (;@4;)
                    end
                  end
                  local.get 3
                  local.get 6
                  i32.const 1052264
                  call $_ZN4core5slice5index22slice_index_order_fail17hbafd09170640ee9bE
                  unreachable
                end
                local.get 6
                i32.const 288
                i32.const 1052264
                call $_ZN4core5slice5index24slice_end_index_len_fail17h9f1e36a81f2d5daaE
                unreachable
              end
              local.get 3
              local.get 6
              i32.const 1052264
              call $_ZN4core5slice5index22slice_index_order_fail17hbafd09170640ee9bE
              unreachable
            end
            local.get 6
            i32.const 192
            i32.const 1052264
            call $_ZN4core5slice5index24slice_end_index_len_fail17h9f1e36a81f2d5daaE
            unreachable
          end
          local.get 0
          i32.const 65535
          i32.and
          local.set 0
          i32.const 1053243
          local.set 2
          i32.const 1
          local.set 1
          block  ;; label = @4
            loop  ;; label = @5
              local.get 2
              i32.const 1
              i32.add
              local.set 6
              block  ;; label = @6
                block  ;; label = @7
                  local.get 2
                  i32.load8_u
                  local.tee 3
                  i32.const 24
                  i32.shl
                  i32.const 24
                  i32.shr_s
                  local.tee 5
                  i32.const 0
                  i32.lt_s
                  br_if 0 (;@7;)
                  local.get 6
                  local.set 2
                  br 1 (;@6;)
                end
                local.get 6
                i32.const 1053681
                i32.eq
                br_if 2 (;@4;)
                local.get 5
                i32.const 127
                i32.and
                i32.const 8
                i32.shl
                local.get 2
                i32.load8_u offset=1
                i32.or
                local.set 3
                local.get 2
                i32.const 2
                i32.add
                local.set 2
              end
              local.get 0
              local.get 3
              i32.sub
              local.tee 0
              i32.const 0
              i32.lt_s
              br_if 3 (;@2;)
              local.get 1
              i32.const 1
              i32.xor
              local.set 1
              local.get 2
              i32.const 1053681
              i32.ne
              br_if 0 (;@5;)
              br 3 (;@2;)
            end
          end
          i32.const 1051156
          i32.const 43
          i32.const 1052280
          call $_ZN4core9panicking5panic17h35778e3c18266c93E
          unreachable
        end
        local.get 0
        i32.const 65535
        i32.and
        local.set 0
        i32.const 1052664
        local.set 2
        i32.const 1
        local.set 1
        loop  ;; label = @3
          local.get 2
          i32.const 1
          i32.add
          local.set 6
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              i32.load8_u
              local.tee 3
              i32.const 24
              i32.shl
              i32.const 24
              i32.shr_s
              local.tee 5
              i32.const 0
              i32.lt_s
              br_if 0 (;@5;)
              local.get 6
              local.set 2
              br 1 (;@4;)
            end
            local.get 6
            i32.const 1052967
            i32.eq
            br_if 3 (;@1;)
            local.get 5
            i32.const 127
            i32.and
            i32.const 8
            i32.shl
            local.get 2
            i32.load8_u offset=1
            i32.or
            local.set 3
            local.get 2
            i32.const 2
            i32.add
            local.set 2
          end
          local.get 0
          local.get 3
          i32.sub
          local.tee 0
          i32.const 0
          i32.lt_s
          br_if 1 (;@2;)
          local.get 1
          i32.const 1
          i32.xor
          local.set 1
          local.get 2
          i32.const 1052967
          i32.ne
          br_if 0 (;@3;)
        end
      end
      local.get 1
      i32.const 1
      i32.and
      return
    end
    i32.const 1051156
    i32.const 43
    i32.const 1052280
    call $_ZN4core9panicking5panic17h35778e3c18266c93E
    unreachable)
  (func $_ZN42_$LT$str$u20$as$u20$core..fmt..Display$GT$3fmt17he4f5a1a8b2979e2bE (type 4) (param i32 i32 i32) (result i32)
    local.get 2
    local.get 0
    local.get 1
    call $_ZN4core3fmt9Formatter3pad17hd96851719c666d38E)
  (func $_ZN41_$LT$char$u20$as$u20$core..fmt..Debug$GT$3fmt17hde8f11129f87ee28E (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32 i32 i64 i32)
    i32.const 1
    local.set 2
    block  ;; label = @1
      local.get 1
      i32.load offset=24
      local.tee 3
      i32.const 39
      local.get 1
      i32.const 28
      i32.add
      i32.load
      i32.load offset=16
      local.tee 4
      call_indirect (type 2)
      br_if 0 (;@1;)
      i32.const 116
      local.set 5
      i32.const 2
      local.set 1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 0
                        i32.load
                        local.tee 0
                        i32.const -9
                        i32.add
                        br_table 8 (;@2;) 3 (;@7;) 1 (;@9;) 1 (;@9;) 2 (;@8;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 1 (;@9;) 4 (;@6;) 0 (;@10;)
                      end
                      local.get 0
                      i32.const 92
                      i32.eq
                      br_if 3 (;@6;)
                    end
                    local.get 0
                    call $_ZN4core7unicode12unicode_data15grapheme_extend6lookup17hdc4e1b1965751945E
                    br_if 3 (;@5;)
                    local.get 0
                    call $_ZN4core7unicode9printable12is_printable17h811be0e8fb84ecc2E
                    i32.eqz
                    br_if 4 (;@4;)
                    i32.const 1
                    local.set 1
                    local.get 0
                    local.set 5
                    br 6 (;@2;)
                  end
                  i32.const 114
                  local.set 5
                  i32.const 2
                  local.set 1
                  br 5 (;@2;)
                end
                i32.const 110
                local.set 5
                i32.const 2
                local.set 1
                br 4 (;@2;)
              end
              i32.const 2
              local.set 1
              local.get 0
              local.set 5
              br 3 (;@2;)
            end
            local.get 0
            i32.const 1
            i32.or
            i32.clz
            i32.const 2
            i32.shr_u
            i32.const 7
            i32.xor
            i64.extend_i32_u
            i64.const 21474836480
            i64.or
            local.set 6
            br 1 (;@3;)
          end
          local.get 0
          i32.const 1
          i32.or
          i32.clz
          i32.const 2
          i32.shr_u
          i32.const 7
          i32.xor
          i64.extend_i32_u
          i64.const 21474836480
          i64.or
          local.set 6
        end
        i32.const 3
        local.set 1
        local.get 0
        local.set 5
      end
      loop  ;; label = @2
        local.get 1
        local.set 7
        i32.const 0
        local.set 1
        local.get 5
        local.set 0
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 7
                  br_table 1 (;@6;) 4 (;@3;) 2 (;@5;) 0 (;@7;) 1 (;@6;)
                end
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 6
                          i64.const 32
                          i64.shr_u
                          i32.wrap_i64
                          i32.const 255
                          i32.and
                          br_table 5 (;@6;) 0 (;@11;) 4 (;@7;) 1 (;@10;) 2 (;@9;) 3 (;@8;) 5 (;@6;)
                        end
                        local.get 6
                        i64.const -1095216660481
                        i64.and
                        local.set 6
                        i32.const 125
                        local.set 0
                        i32.const 3
                        local.set 1
                        br 7 (;@3;)
                      end
                      local.get 6
                      i64.const -1095216660481
                      i64.and
                      i64.const 8589934592
                      i64.or
                      local.set 6
                      i32.const 123
                      local.set 0
                      i32.const 3
                      local.set 1
                      br 6 (;@3;)
                    end
                    local.get 6
                    i64.const -1095216660481
                    i64.and
                    i64.const 12884901888
                    i64.or
                    local.set 6
                    i32.const 117
                    local.set 0
                    i32.const 3
                    local.set 1
                    br 5 (;@3;)
                  end
                  local.get 6
                  i64.const -1095216660481
                  i64.and
                  i64.const 17179869184
                  i64.or
                  local.set 6
                  i32.const 92
                  local.set 0
                  i32.const 3
                  local.set 1
                  br 4 (;@3;)
                end
                i32.const 48
                i32.const 87
                local.get 5
                local.get 6
                i32.wrap_i64
                local.tee 1
                i32.const 2
                i32.shl
                i32.shr_u
                i32.const 15
                i32.and
                local.tee 0
                i32.const 10
                i32.lt_u
                select
                local.get 0
                i32.add
                local.set 0
                local.get 1
                i32.eqz
                br_if 2 (;@4;)
                local.get 6
                i64.const -1
                i64.add
                i64.const 4294967295
                i64.and
                local.get 6
                i64.const -4294967296
                i64.and
                i64.or
                local.set 6
                i32.const 3
                local.set 1
                br 3 (;@3;)
              end
              local.get 3
              i32.const 39
              local.get 4
              call_indirect (type 2)
              local.set 2
              br 4 (;@1;)
            end
            i32.const 92
            local.set 0
            i32.const 1
            local.set 1
            br 1 (;@3;)
          end
          local.get 6
          i64.const -1095216660481
          i64.and
          i64.const 4294967296
          i64.or
          local.set 6
          i32.const 3
          local.set 1
        end
        local.get 3
        local.get 0
        local.get 4
        call_indirect (type 2)
        i32.eqz
        br_if 0 (;@2;)
      end
    end
    local.get 2)
  (func $_ZN4core3fmt3num3imp51_$LT$impl$u20$core..fmt..Display$u20$for$u20$u8$GT$3fmt17he9536755db9192aaE (type 2) (param i32 i32) (result i32)
    local.get 0
    i64.load8_u
    i32.const 1
    local.get 1
    call $_ZN4core3fmt3num3imp7fmt_u6417ha36fe3566051d739E)
  (func $_ZN4core3fmt3num53_$LT$impl$u20$core..fmt..LowerHex$u20$for$u20$i32$GT$3fmt17hbc57992e5da9d71eE (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 128
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 0
    i32.load
    local.set 0
    i32.const 0
    local.set 3
    loop  ;; label = @1
      local.get 2
      local.get 3
      i32.add
      i32.const 127
      i32.add
      i32.const 48
      i32.const 87
      local.get 0
      i32.const 15
      i32.and
      local.tee 4
      i32.const 10
      i32.lt_u
      select
      local.get 4
      i32.add
      i32.store8
      local.get 3
      i32.const -1
      i32.add
      local.set 3
      local.get 0
      i32.const 15
      i32.gt_u
      local.set 4
      local.get 0
      i32.const 4
      i32.shr_u
      local.set 0
      local.get 4
      br_if 0 (;@1;)
    end
    block  ;; label = @1
      local.get 3
      i32.const 128
      i32.add
      local.tee 0
      i32.const 129
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 128
      i32.const 1051444
      call $_ZN4core5slice5index26slice_start_index_len_fail17hf52653f30012ff28E
      unreachable
    end
    local.get 1
    i32.const 1
    i32.const 1051460
    i32.const 2
    local.get 2
    local.get 3
    i32.add
    i32.const 128
    i32.add
    i32.const 0
    local.get 3
    i32.sub
    call $_ZN4core3fmt9Formatter12pad_integral17hc91f100cd8a143afE
    local.set 0
    local.get 2
    i32.const 128
    i32.add
    global.set $__stack_pointer
    local.get 0)
  (func $_ZN4core3fmt3num3imp7fmt_u6417ha36fe3566051d739E (type 18) (param i64 i32 i32) (result i32)
    (local i32 i32 i64 i32 i32 i32)
    global.get $__stack_pointer
    i32.const 48
    i32.sub
    local.tee 3
    global.set $__stack_pointer
    i32.const 39
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        local.get 0
        i64.const 10000
        i64.ge_u
        br_if 0 (;@2;)
        local.get 0
        local.set 5
        br 1 (;@1;)
      end
      i32.const 39
      local.set 4
      loop  ;; label = @2
        local.get 3
        i32.const 9
        i32.add
        local.get 4
        i32.add
        local.tee 6
        i32.const -4
        i32.add
        local.get 0
        local.get 0
        i64.const 10000
        i64.div_u
        local.tee 5
        i64.const 10000
        i64.mul
        i64.sub
        i32.wrap_i64
        local.tee 7
        i32.const 65535
        i32.and
        i32.const 100
        i32.div_u
        local.tee 8
        i32.const 1
        i32.shl
        i32.const 1051462
        i32.add
        i32.load16_u align=1
        i32.store16 align=1
        local.get 6
        i32.const -2
        i32.add
        local.get 7
        local.get 8
        i32.const 100
        i32.mul
        i32.sub
        i32.const 65535
        i32.and
        i32.const 1
        i32.shl
        i32.const 1051462
        i32.add
        i32.load16_u align=1
        i32.store16 align=1
        local.get 4
        i32.const -4
        i32.add
        local.set 4
        local.get 0
        i64.const 99999999
        i64.gt_u
        local.set 6
        local.get 5
        local.set 0
        local.get 6
        br_if 0 (;@2;)
      end
    end
    block  ;; label = @1
      local.get 5
      i32.wrap_i64
      local.tee 6
      i32.const 99
      i32.le_s
      br_if 0 (;@1;)
      local.get 3
      i32.const 9
      i32.add
      local.get 4
      i32.const -2
      i32.add
      local.tee 4
      i32.add
      local.get 5
      i32.wrap_i64
      local.tee 6
      local.get 6
      i32.const 65535
      i32.and
      i32.const 100
      i32.div_u
      local.tee 6
      i32.const 100
      i32.mul
      i32.sub
      i32.const 65535
      i32.and
      i32.const 1
      i32.shl
      i32.const 1051462
      i32.add
      i32.load16_u align=1
      i32.store16 align=1
    end
    block  ;; label = @1
      block  ;; label = @2
        local.get 6
        i32.const 10
        i32.lt_s
        br_if 0 (;@2;)
        local.get 3
        i32.const 9
        i32.add
        local.get 4
        i32.const -2
        i32.add
        local.tee 4
        i32.add
        local.get 6
        i32.const 1
        i32.shl
        i32.const 1051462
        i32.add
        i32.load16_u align=1
        i32.store16 align=1
        br 1 (;@1;)
      end
      local.get 3
      i32.const 9
      i32.add
      local.get 4
      i32.const -1
      i32.add
      local.tee 4
      i32.add
      local.get 6
      i32.const 48
      i32.add
      i32.store8
    end
    local.get 2
    local.get 1
    i32.const 1051144
    i32.const 0
    local.get 3
    i32.const 9
    i32.add
    local.get 4
    i32.add
    i32.const 39
    local.get 4
    i32.sub
    call $_ZN4core3fmt9Formatter12pad_integral17hc91f100cd8a143afE
    local.set 4
    local.get 3
    i32.const 48
    i32.add
    global.set $__stack_pointer
    local.get 4)
  (func $_ZN4core3fmt3num53_$LT$impl$u20$core..fmt..UpperHex$u20$for$u20$i32$GT$3fmt17h4cba4dfa34406e54E (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get $__stack_pointer
    i32.const 128
    i32.sub
    local.tee 2
    global.set $__stack_pointer
    local.get 0
    i32.load
    local.set 0
    i32.const 0
    local.set 3
    loop  ;; label = @1
      local.get 2
      local.get 3
      i32.add
      i32.const 127
      i32.add
      i32.const 48
      i32.const 55
      local.get 0
      i32.const 15
      i32.and
      local.tee 4
      i32.const 10
      i32.lt_u
      select
      local.get 4
      i32.add
      i32.store8
      local.get 3
      i32.const -1
      i32.add
      local.set 3
      local.get 0
      i32.const 15
      i32.gt_u
      local.set 4
      local.get 0
      i32.const 4
      i32.shr_u
      local.set 0
      local.get 4
      br_if 0 (;@1;)
    end
    block  ;; label = @1
      local.get 3
      i32.const 128
      i32.add
      local.tee 0
      i32.const 129
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 128
      i32.const 1051444
      call $_ZN4core5slice5index26slice_start_index_len_fail17hf52653f30012ff28E
      unreachable
    end
    local.get 1
    i32.const 1
    i32.const 1051460
    i32.const 2
    local.get 2
    local.get 3
    i32.add
    i32.const 128
    i32.add
    i32.const 0
    local.get 3
    i32.sub
    call $_ZN4core3fmt9Formatter12pad_integral17hc91f100cd8a143afE
    local.set 0
    local.get 2
    i32.const 128
    i32.add
    global.set $__stack_pointer
    local.get 0)
  (func $_ZN4core3fmt3num3imp52_$LT$impl$u20$core..fmt..Display$u20$for$u20$i32$GT$3fmt17hf4754ab964e4babeE (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    local.tee 0
    i64.extend_i32_u
    local.get 0
    i32.const -1
    i32.xor
    i64.extend_i32_s
    i64.const 1
    i64.add
    local.get 0
    i32.const -1
    i32.gt_s
    local.tee 0
    select
    local.get 0
    local.get 1
    call $_ZN4core3fmt3num3imp7fmt_u6417ha36fe3566051d739E)
  (func $_ZN53_$LT$core..fmt..Error$u20$as$u20$core..fmt..Debug$GT$3fmt17hbeda32abaa3bca3fE (type 2) (param i32 i32) (result i32)
    local.get 1
    i32.load offset=24
    i32.const 1053772
    i32.const 5
    local.get 1
    i32.const 28
    i32.add
    i32.load
    i32.load offset=12
    call_indirect (type 4))
  (table (;0;) 50 50 funcref)
  (memory (;0;) 17)
  (global $__stack_pointer (mut i32) (i32.const 1048576))
  (global (;1;) i32 (i32.const 1049056))
  (global (;2;) i32 (i32.const 1049056))
  (global (;3;) i32 (i32.const 1055096))
  (global (;4;) i32 (i32.const 1055104))
  (export "memory" (memory 0))
  (export "init" (func $init))
  (export "__PBC_VERSION_CLIENT_3_0_0" (global 1))
  (export "action_02" (func $action_02))
  (export "__PBC_VERSION_BINDER_3_0_0" (global 2))
  (export "memcpy" (func $memcpy))
  (export "memmove" (func $memcpy))
  (export "wasm_exit" (func $wasm_exit))
  (export "__data_end" (global 3))
  (export "__heap_base" (global 4))
  (elem (;0;) (i32.const 1) func $_ZN4core3fmt3num3imp51_$LT$impl$u20$core..fmt..Display$u20$for$u20$u8$GT$3fmt17he9536755db9192aaE $_ZN4core3ptr42drop_in_place$LT$std..io..error..Error$GT$17hdff5e6040e67c8c3E $_ZN58_$LT$std..io..error..Error$u20$as$u20$core..fmt..Debug$GT$3fmt17h738123dcdfa351b9E $_ZN4core3fmt3num3imp52_$LT$impl$u20$core..fmt..Display$u20$for$u20$u32$GT$3fmt17h0e0d5207039041dbE $_ZN44_$LT$$RF$T$u20$as$u20$core..fmt..Display$GT$3fmt17h3a4134da8e0bc735E $_ZN60_$LT$alloc..string..String$u20$as$u20$core..fmt..Display$GT$3fmt17h95dd8a0b9d13f099E $_ZN4core3ptr79drop_in_place$LT$pbc_lib..exit..override_panic..$u7b$$u7b$closure$u7d$$u7d$$GT$17ha262323faf70b39dE $_ZN4core3ops8function6FnOnce40call_once$u7b$$u7b$vtable.shim$u7d$$u7d$17h1dd71637518887aaE $_ZN7pbc_lib4exit14override_panic28_$u7b$$u7b$closure$u7d$$u7d$17h76a7033cabaa5784E $_ZN4core3ptr42drop_in_place$LT$std..io..error..Error$GT$17h6226570f93ddc57dE $_ZN3std5alloc24default_alloc_error_hook17h302e0694dbb851afE $_ZN4core3ptr100drop_in_place$LT$$RF$mut$u20$std..io..Write..write_fmt..Adapter$LT$alloc..vec..Vec$LT$u8$GT$$GT$$GT$17h339b3d5f0693762fE $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_str17hb2141a7688c38d13E $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$10write_char17hf8370214c82b4737E $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_fmt17h9091e91844c7467cE $_ZN62_$LT$std..io..error..ErrorKind$u20$as$u20$core..fmt..Debug$GT$3fmt17h3c7f418217a51dfeE $_ZN42_$LT$$RF$T$u20$as$u20$core..fmt..Debug$GT$3fmt17hd5a0e5bf2fc11a8eE $_ZN4core3fmt3num50_$LT$impl$u20$core..fmt..Debug$u20$for$u20$i32$GT$3fmt17he5b0a965714491afE $_ZN4core3ptr226drop_in_place$LT$std..error..$LT$impl$u20$core..convert..From$LT$alloc..string..String$GT$$u20$for$u20$alloc..boxed..Box$LT$dyn$u20$std..error..Error$u2b$core..marker..Sync$u2b$core..marker..Send$GT$$GT$..from..StringError$GT$17h0ee9186882c5bd0bE $_ZN58_$LT$alloc..string..String$u20$as$u20$core..fmt..Debug$GT$3fmt17h97fafa98adacfdd8E $_ZN4core3ptr70drop_in_place$LT$std..panicking..begin_panic_handler..PanicPayload$GT$17h84169870e0654f03E $_ZN90_$LT$std..panicking..begin_panic_handler..PanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$8take_box17h7ce99a17e9564003E $_ZN90_$LT$std..panicking..begin_panic_handler..PanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$3get17hbfdef6bcdd013225E $_ZN93_$LT$std..panicking..begin_panic_handler..StrPanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$8take_box17h03b9b6028fc9e0f4E $_ZN93_$LT$std..panicking..begin_panic_handler..StrPanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$3get17hfd52de6c2d25f5deE $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17h652f73c7551ae1b1E $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17hd3828b2d5b220c95E $_ZN91_$LT$std..panicking..begin_panic..PanicPayload$LT$A$GT$$u20$as$u20$core..panic..BoxMeUp$GT$8take_box17h64e1239c12ef2cb0E $_ZN91_$LT$std..panicking..begin_panic..PanicPayload$LT$A$GT$$u20$as$u20$core..panic..BoxMeUp$GT$3get17hade34ccc3d198d5dE $_ZN42_$LT$$RF$T$u20$as$u20$core..fmt..Debug$GT$3fmt17h40ebbd3d0573d9c8E $_ZN42_$LT$$RF$T$u20$as$u20$core..fmt..Debug$GT$3fmt17h2e70fb27e7e00addE $_ZN4core3ptr27drop_in_place$LT$$RF$u8$GT$17h3c378e23854b1f65E $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_str17h032b9e77e62a03d5E $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$10write_char17h1282ec243aa9cf5cE $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_fmt17h8d373c03cf4ddb6fE $_ZN53_$LT$core..fmt..Error$u20$as$u20$core..fmt..Debug$GT$3fmt17hbeda32abaa3bca3fE $_ZN4core3ops8function6FnOnce9call_once17h46d8ad6e7ff1079cE $_ZN42_$LT$$RF$T$u20$as$u20$core..fmt..Debug$GT$3fmt17he19a0ce54ad248d3E $_ZN44_$LT$$RF$T$u20$as$u20$core..fmt..Display$GT$3fmt17hfb847384256de23cE $_ZN71_$LT$core..ops..range..Range$LT$Idx$GT$$u20$as$u20$core..fmt..Debug$GT$3fmt17h175a7f03519f2338E $_ZN41_$LT$char$u20$as$u20$core..fmt..Debug$GT$3fmt17hde8f11129f87ee28E $_ZN4core3ptr102drop_in_place$LT$$RF$core..iter..adapters..copied..Copied$LT$core..slice..iter..Iter$LT$u8$GT$$GT$$GT$17haf777a974e22beeaE $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17h02aaa3363841a0a8E $_ZN68_$LT$core..fmt..builders..PadAdapter$u20$as$u20$core..fmt..Write$GT$9write_str17h7ca5dec47fadd416E $_ZN4core3fmt5Write10write_char17he7b2aa7a66144b54E $_ZN4core3fmt5Write9write_fmt17hb90aec2972b27b32E $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_str17h47b32869691fa439E $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$10write_char17h08b6b26387744865E $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_fmt17h508b7c51f344920cE)
  (data $.rodata (i32.const 1048576) "No known enum value with discriminant \00\00\00\00\10\00&\00\00\00C:\5cUsers\5cJensS\5csrc\5cprivacyblockchain\5clanguage\5crust-example-testing-contract\5cdependencies\5crust-contract-sdk\5cpbc_contract_common\5csrc\5caddress.rs\00\00\000\00\10\00\8d\00\00\00\0b\00\00\00N\00\00\00failed to fill whole buffer\00\d0\00\10\00\1b\00\00\00called `Result::unwrap()` on an `Err` value\00\02\00\00\00\08\00\00\00\04\00\00\00\03\00\00\00C:\5cUsers\5cJensS\5csrc\5cprivacyblockchain\5clanguage\5crust-example-testing-contract\5cdependencies\5crust-contract-sdk\5cpbc_traits\5csrc\5cread_int.rs\00\00\000\01\10\00\85\00\00\00H\00\00\00\05\00\00\000\01\10\00\85\00\00\00\5c\00\00\00\05\00\00\00\08\00\00\00\00\00\00\00Context data too long\00\00\00\e0\01\10\00\15\00\00\00src\5clib.rs\00\00\00\02\10\00\0a\00\00\00\22\00\00\00\01\00\00\00RPC data too long\00\00\00\1c\02\10\00\11\00\00\00\00\02\10\00\0a\00\00\006\00\00\00\01\00\00\00state_reader data too long\00\00H\02\10\00\1a\00\00\00I am panicking\00\00l\02\10\00\0e\00\00\00\00\02\10\00\0a\00\00\008\00\00\00\05\00\00\00failed to fill whole buffer\00\94\02\10\00\1b\00\00\00called `Result::unwrap()` on an `Err` value\00\02\00\00\00\08\00\00\00\04\00\00\00\03\00\00\00C:\5cUsers\5cJensS\5csrc\5cprivacyblockchain\5clanguage\5crust-example-testing-contract\5cdependencies\5crust-contract-sdk\5cpbc_traits\5csrc\5creadwrite_rpc.rs\00\00\f4\02\10\00\8a\00\00\00\8a\00\00\00%\00\00\00\07\00\00\00\00\00\00\00\01\00\00\00\08\00\00\00\09\00\00\00\09\00\00\00unknown location\a8\03\10\00\10\00\00\00:\00\00\00\90\03\10\00\00\00\00\00\c0\03\10\00\01\00\00\00: \00\00\90\03\10\00\00\00\00\00\d4\03\10\00\02\00\00\00called `Result::unwrap()` on an `Err` value\00\0a\00\00\00\08\00\00\00\04\00\00\00\03\00\00\00C:\5cUsers\5cJensS\5csrc\5cprivacyblockchain\5clanguage\5crust-example-testing-contract\5cdependencies\5crust-contract-sdk\5cpbc_traits\5csrc\5creadwrite_rpc.rs\00\00$\04\10\00\8a\00\00\00+\00\00\00'\00\00\00C:\5cUsers\5cJensS\5csrc\5cprivacyblockchain\5clanguage\5crust-example-testing-contract\5cdependencies\5crust-contract-sdk\5cpbc_contract_common\5csrc\5clib.rs\00\00\00\c0\04\10\00\89\00\00\002\00\00\00\05\00\00\00\0c\00\00\00\04\00\00\00\04\00\00\00\0d\00\00\00\0e\00\00\00\0f\00\00\00called `Option::unwrap()` on a `None` value\00\01\00\00\00\00\00\00\00Errorkind\00\00\00\0c\00\00\00\01\00\00\00\01\00\00\00\10\00\00\00message\00\0c\00\00\00\08\00\00\00\04\00\00\00\11\00\00\00KindOscode\00\00\0c\00\00\00\04\00\00\00\04\00\00\00\12\00\00\00\13\00\00\00\0c\00\00\00\04\00\00\00\14\00\00\00cannot modify the panic hook from a panicking threadlibrary/std/src/panicking.rs<\06\10\00\1c\00\00\00v\00\00\00\09\00\00\00<\06\10\00\1c\00\00\00\f0\01\00\00\1f\00\00\00<\06\10\00\1c\00\00\00\f1\01\00\00\1e\00\00\00\15\00\00\00\10\00\00\00\04\00\00\00\16\00\00\00\17\00\00\00\0c\00\00\00\08\00\00\00\04\00\00\00\18\00\00\00\19\00\00\00\13\00\00\00\0c\00\00\00\04\00\00\00\1a\00\00\00\0c\00\00\00\08\00\00\00\04\00\00\00\1b\00\00\00\0c\00\00\00\08\00\00\00\04\00\00\00\1c\00\00\00\1d\00\00\00UnsupportederrorCustom\00\00\0c\00\00\00\04\00\00\00\04\00\00\00\1e\00\00\00\0c\00\00\00\04\00\00\00\04\00\00\00\1f\00\00\00UncategorizedOtherOutOfMemoryUnexpectedEofInterruptedArgumentListTooLongFilenameTooLongTooManyLinksCrossesDevicesDeadlockExecutableFileBusyResourceBusyFileTooLargeFilesystemQuotaExceededNotSeekableStorageFullWriteZeroTimedOutInvalidDataInvalidInputStaleNetworkFileHandleFilesystemLoopReadOnlyFilesystemDirectoryNotEmptyIsADirectoryNotADirectoryWouldBlockAlreadyExistsBrokenPipeNetworkDownAddrNotAvailableAddrInUseNotConnectedConnectionAbortedNetworkUnreachableHostUnreachableConnectionResetConnectionRefusedPermissionDeniedNotFoundoperation successful\00 \00\00\00\04\00\00\00\04\00\00\00!\00\00\00\22\00\00\00#\00\00\00 \00\00\00\00\00\00\00\01\00\00\00$\00\00\00library/alloc/src/raw_vec.rscapacity overflow\00\00\00l\09\10\00\1c\00\00\00\fd\01\00\00\05\00\00\00a formatting trait implementation returned an errorlibrary/alloc/src/fmt.rs\00\df\09\10\00\18\00\00\00U\02\00\00\1c\00\00\00..\00\00\08\0a\10\00\02\00\00\00called `Option::unwrap()` on a `None` value\00*\00\00\00\00\00\00\00\01\00\00\00+\00\00\00index out of bounds: the len is  but the index is \00\00P\0a\10\00 \00\00\00p\0a\10\00\12\00\00\00`: \00\08\0a\10\00\00\00\00\00\95\0a\10\00\02\00\00\00*\00\00\00\0c\00\00\00\04\00\00\00,\00\00\00-\00\00\00.\00\00\00    library/core/src/fmt/builders.rs\c4\0a\10\00 \00\00\00/\00\00\00!\00\00\00\c4\0a\10\00 \00\00\000\00\00\00\12\00\00\00 {\0a,\0a,  { } }(\0a(,)library/core/src/fmt/num.rs\00\00\00\16\0b\10\00\1b\00\00\00e\00\00\00\14\00\00\000x00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899\00\00*\00\00\00\04\00\00\00\04\00\00\00/\00\00\000\00\00\001\00\00\00library/core/src/fmt/mod.rs\00(\0c\10\00\1b\00\00\00]\08\00\00\1e\00\00\00(\0c\10\00\1b\00\00\00d\08\00\00\16\00\00\00library/core/src/slice/memchr.rsd\0c\10\00 \00\00\00[\00\00\00\05\00\00\00range start index  out of range for slice of length \94\0c\10\00\12\00\00\00\a6\0c\10\00\22\00\00\00range end index \d8\0c\10\00\10\00\00\00\a6\0c\10\00\22\00\00\00slice index starts at  but ends at \00\f8\0c\10\00\16\00\00\00\0e\0d\10\00\0d\00\00\00library/core/src/str/validations.rs\00,\0d\10\00#\00\00\00\1e\01\00\00\11\00\00\00[...]byte index  is out of bounds of `\00\00e\0d\10\00\0b\00\00\00p\0d\10\00\16\00\00\00\94\0a\10\00\01\00\00\00begin <= end ( <= ) when slicing `\00\00\a0\0d\10\00\0e\00\00\00\ae\0d\10\00\04\00\00\00\b2\0d\10\00\10\00\00\00\94\0a\10\00\01\00\00\00 is not a char boundary; it is inside  (bytes ) of `e\0d\10\00\0b\00\00\00\e4\0d\10\00&\00\00\00\0a\0e\10\00\08\00\00\00\12\0e\10\00\06\00\00\00\94\0a\10\00\01\00\00\00library/core/src/unicode/printable.rs\00\00\00@\0e\10\00%\00\00\00\0a\00\00\00\1c\00\00\00@\0e\10\00%\00\00\00\1a\00\00\006\00\00\00\00\01\03\05\05\06\06\02\07\06\08\07\09\11\0a\1c\0b\19\0c\1a\0d\10\0e\0d\0f\04\10\03\12\12\13\09\16\01\17\04\18\01\19\03\1a\07\1b\01\1c\02\1f\16 \03+\03-\0b.\010\031\022\01\a7\02\a9\02\aa\04\ab\08\fa\02\fb\05\fd\02\fe\03\ff\09\adxy\8b\8d\a20WX\8b\8c\90\1c\dd\0e\0fKL\fb\fc./?\5c]_\e2\84\8d\8e\91\92\a9\b1\ba\bb\c5\c6\c9\ca\de\e4\e5\ff\00\04\11\12)147:;=IJ]\84\8e\92\a9\b1\b4\ba\bb\c6\ca\ce\cf\e4\e5\00\04\0d\0e\11\12)14:;EFIJ^de\84\91\9b\9d\c9\ce\cf\0d\11):;EIW[\5c^_de\8d\91\a9\b4\ba\bb\c5\c9\df\e4\e5\f0\0d\11EIde\80\84\b2\bc\be\bf\d5\d7\f0\f1\83\85\8b\a4\a6\be\bf\c5\c7\ce\cf\da\dbH\98\bd\cd\c6\ce\cfINOWY^_\89\8e\8f\b1\b6\b7\bf\c1\c6\c7\d7\11\16\17[\5c\f6\f7\fe\ff\80mq\de\df\0e\1fno\1c\1d_}~\ae\af\7f\bb\bc\16\17\1e\1fFGNOXZ\5c^~\7f\b5\c5\d4\d5\dc\f0\f1\f5rs\8ftu\96&./\a7\af\b7\bf\c7\cf\d7\df\9a@\97\980\8f\1f\d2\d4\ce\ffNOZ[\07\08\0f\10'/\ee\efno7=?BE\90\91Sgu\c8\c9\d0\d1\d8\d9\e7\fe\ff\00 _\22\82\df\04\82D\08\1b\04\06\11\81\ac\0e\80\ab\05\1f\09\81\1b\03\19\08\01\04/\044\04\07\03\01\07\06\07\11\0aP\0f\12\07U\07\03\04\1c\0a\09\03\08\03\07\03\02\03\03\03\0c\04\05\03\0b\06\01\0e\15\05N\07\1b\07W\07\02\06\16\0dP\04C\03-\03\01\04\11\06\0f\0c:\04\1d%_ m\04j%\80\c8\05\82\b0\03\1a\06\82\fd\03Y\07\16\09\18\09\14\0c\14\0cj\06\0a\06\1a\06Y\07+\05F\0a,\04\0c\04\01\031\0b,\04\1a\06\0b\03\80\ac\06\0a\06/1M\03\80\a4\08<\03\0f\03<\078\08+\05\82\ff\11\18\08/\11-\03!\0f!\0f\80\8c\04\82\97\19\0b\15\88\94\05/\05;\07\02\0e\18\09\80\be\22t\0c\80\d6\1a\0c\05\80\ff\05\80\df\0c\f2\9d\037\09\81\5c\14\80\b8\08\80\cb\05\0a\18;\03\0a\068\08F\08\0c\06t\0b\1e\03Z\04Y\09\80\83\18\1c\0a\16\09L\04\80\8a\06\ab\a4\0c\17\041\a1\04\81\da&\07\0c\05\05\80\a6\10\81\f5\07\01 *\06L\04\80\8d\04\80\be\03\1b\03\0f\0d\00\06\01\01\03\01\04\02\05\07\07\02\08\08\09\02\0a\05\0b\02\0e\04\10\01\11\02\12\05\13\11\14\01\15\02\17\02\19\0d\1c\05\1d\08$\01j\04k\02\af\03\bc\02\cf\02\d1\02\d4\0c\d5\09\d6\02\d7\02\da\01\e0\05\e1\02\e7\04\e8\02\ee \f0\04\f8\02\fa\02\fb\01\0c';>NO\8f\9e\9e\9f{\8b\93\96\a2\b2\ba\86\b1\06\07\096=>V\f3\d0\d1\04\14\1867VW\7f\aa\ae\af\bd5\e0\12\87\89\8e\9e\04\0d\0e\11\12)14:EFIJNOde\5c\b6\b7\1b\1c\07\08\0a\0b\14\1769:\a8\a9\d8\d9\097\90\91\a8\07\0a;>fi\8f\92o_\bf\ee\efZb\f4\fc\ff\9a\9b./'(U\9d\a0\a1\a3\a4\a7\a8\ad\ba\bc\c4\06\0b\0c\15\1d:?EQ\a6\a7\cc\cd\a0\07\19\1a\22%>?\e7\ec\ef\ff\c5\c6\04 #%&(38:HJLPSUVXZ\5c^`cefksx}\7f\8a\a4\aa\af\b0\c0\d0\ae\afno\93^\22{\05\03\04-\03f\03\01/.\80\82\1d\031\0f\1c\04$\09\1e\05+\05D\04\0e*\80\aa\06$\04$\04(\084\0bNC\817\09\16\0a\08\18;E9\03c\08\090\16\05!\03\1b\05\01@8\04K\05/\04\0a\07\09\07@ '\04\0c\096\03:\05\1a\07\04\0c\07PI73\0d3\07.\08\0a\81&RN(\08*\16\1a&\1c\14\17\09N\04$\09D\0d\19\07\0a\06H\08'\09u\0b?A*\06;\05\0a\06Q\06\01\05\10\03\05\80\8bb\1eH\08\0a\80\a6^\22E\0b\0a\06\0d\13:\06\0a6,\04\17\80\b9<dS\0cH\09\0aFE\1bH\08S\0dI\81\07F\0a\1d\03GI7\03\0e\08\0a\069\07\0a\816\19\80\b7\01\0f2\0d\83\9bfu\0b\80\c4\8aLc\0d\84/\8f\d1\82G\a1\b9\829\07*\04\5c\06&\0aF\0a(\05\13\82\b0[eK\049\07\11@\05\0b\02\0e\97\f8\08\84\d6*\09\a2\e7\813-\03\11\04\08\81\8c\89\04k\05\0d\03\09\07\10\92`G\09t<\80\f6\0as\08p\15F\80\9a\14\0cW\09\19\80\87\81G\03\85B\0f\15\84P\1f\80\e1+\80\d5-\03\1a\04\02\81@\1f\11:\05\01\84\e0\80\f7)L\04\0a\04\02\83\11DL=\80\c2<\06\01\04U\05\1b4\02\81\0e,\04d\0cV\0a\80\ae8\1d\0d,\04\09\07\02\0e\06\80\9a\83\d8\05\10\03\0d\03t\0cY\07\0c\04\01\0f\0c\048\08\0a\06(\08\22N\81T\0c\15\03\05\03\07\09\1d\03\0b\05\06\0a\0a\06\08\08\07\09\80\cb%\0a\84\06library/core/src/unicode/unicode_data.rs\00\00\00\f1\13\10\00(\00\00\00K\00\00\00(\00\00\00\f1\13\10\00(\00\00\00W\00\00\00\16\00\00\00\f1\13\10\00(\00\00\00R\00\00\00>\00\00\00Error\00\00\00\00\03\00\00\83\04 \00\91\05`\00]\13\a0\00\12\17 \1f\0c `\1f\ef,\a0+*0 ,o\a6\e0,\02\a8`-\1e\fb`.\00\fe 6\9e\ff`6\fd\01\e16\01\0a!7$\0d\e17\ab\0ea9/\18\a190\1c\e1G\f3\1e!L\f0j\e1OOo!P\9d\bc\a1P\00\cfaQe\d1\a1Q\00\da!R\00\e0\e1S0\e1aU\ae\e2\a1V\d0\e8\e1V \00nW\f0\01\ffW\00p\00\07\00-\01\01\01\02\01\02\01\01H\0b0\15\10\01e\07\02\06\02\02\01\04#\01\1e\1b[\0b:\09\09\01\18\04\01\09\01\03\01\05+\03<\08*\18\01 7\01\01\01\04\08\04\01\03\07\0a\02\1d\01:\01\01\01\02\04\08\01\09\01\0a\02\1a\01\02\029\01\04\02\04\02\02\03\03\01\1e\02\03\01\0b\029\01\04\05\01\02\04\01\14\02\16\06\01\01:\01\01\02\01\04\08\01\07\03\0a\02\1e\01;\01\01\01\0c\01\09\01(\01\03\017\01\01\03\05\03\01\04\07\02\0b\02\1d\01:\01\02\01\02\01\03\01\05\02\07\02\0b\02\1c\029\02\01\01\02\04\08\01\09\01\0a\02\1d\01H\01\04\01\02\03\01\01\08\01Q\01\02\07\0c\08b\01\02\09\0b\06J\02\1b\01\01\01\01\017\0e\01\05\01\02\05\0b\01$\09\01f\04\01\06\01\02\02\02\19\02\04\03\10\04\0d\01\02\02\06\01\0f\01\00\03\00\03\1d\02\1e\02\1e\02@\02\01\07\08\01\02\0b\09\01-\03\01\01u\02\22\01v\03\04\02\09\01\06\03\db\02\02\01:\01\01\07\01\01\01\01\02\08\06\0a\02\010\1f1\040\07\01\01\05\01(\09\0c\02 \04\02\02\01\038\01\01\02\03\01\01\03:\08\02\02\98\03\01\0d\01\07\04\01\06\01\03\02\c6@\00\01\c3!\00\03\8d\01` \00\06i\02\00\04\01\0a \02P\02\00\01\03\01\04\01\19\02\05\01\97\02\1a\12\0d\01&\08\19\0b.\030\01\02\04\02\02'\01C\06\02\02\02\02\0c\01\08\01/\013\01\01\03\02\02\05\02\01\01*\02\08\01\ee\01\02\01\04\01\00\01\00\10\10\10\00\02\00\01\e2\01\95\05\00\03\01\02\05\04(\03\04\01\a5\02\00\04\00\02\99\0b1\04{\016\0f)\01\02\02\0a\031\04\02\02\07\01=\03$\05\01\08>\01\0c\024\09\0a\04\02\01_\03\02\01\01\02\06\01\a0\01\03\08\15\029\02\01\01\01\01\16\01\0e\07\03\05\c3\08\02\03\01\01\17\01Q\01\02\06\01\01\02\01\01\02\01\02\eb\01\02\04\06\02\01\02\1b\02U\08\02\01\01\02j\01\01\01\02\06\01\01e\03\02\04\01\05\00\09\01\02\f5\01\0a\02\01\01\04\01\90\04\02\02\04\01 \0a(\06\02\04\08\01\09\06\02\03.\0d\01\02\00\07\01\06\01\01R\16\02\07\01\02\01\02z\06\03\01\01\02\01\07\01\01H\02\03\01\01\01\00\02\00\05;\07\00\01?\04Q\01\00\02\00.\02\17\00\01\01\03\04\05\08\08\02\07\1e\04\94\03\007\042\08\01\0e\01\16\05\01\0f\00\07\01\11\02\07\01\02\01\05\00\07\00\01=\04\00\07m\07\00`\80\f0\00"))
