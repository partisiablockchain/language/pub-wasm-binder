(module
  (import "pbc" "memmove" (func (param i32 i32 i32) (result i32)))
  (memory 1)
  (data 0 (i32.const 0x05) "Hello, World!")
  (data 0 (i32.const 0x00) "\01") ;; Section id: state
  (data 0 (i32.const 0x01) "\00\00\00\0d") ;; Section length
  (func $callmemmove (param) (result i64)
    i32.const 0x0c ;; Src (0x05 + 7)
    i32.const 0x05 ;; Dest (0x05)
    i32.const 5    ;; Length: |Hello|
    call 0  ;;Memory should now be: "Hello, Hello!"
    drop
    i64.const 0x0000_0012_0000_0000
  )
  (global $binder_exported i32 i32.const 0)
  (export "__PBC_VERSION_BINDER_5_0_0" (global $binder_exported))
  (export "call_memmove" (func $callmemmove ))
)

