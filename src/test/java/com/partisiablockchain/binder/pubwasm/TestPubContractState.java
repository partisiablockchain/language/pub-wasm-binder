package com.partisiablockchain.binder.pubwasm;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Contract state for testing {@link PublicWasmContractBinder}. */
public final class TestPubContractState {

  int createCount;
  int invokeCount;
  byte[] lastContext;
  byte[] lastRpc;

  /**
   * Create new {@link TestPubContractState}.
   *
   * @param createCount Count of creation
   * @param invokeCount Count of invocations.
   * @param lastContext Serialization of the last context.
   * @param lastRpc The latest RPC given to the contract.
   */
  public TestPubContractState(
      int createCount, int invokeCount, byte[] lastContext, byte[] lastRpc) {
    this.createCount = createCount;
    this.invokeCount = invokeCount;
    this.lastContext = lastContext;
    this.lastRpc = lastRpc;
  }

  TestPubWasmContext lastContext() {
    return TestPubWasmContext.readFrom(SafeDataInputStream.createFromBytes(lastContext));
  }

  byte[] asByteArray() {
    return SafeDataOutputStream.serialize(
        out -> {
          out.writeInt(createCount);
          out.writeInt(invokeCount);
          out.writeDynamicBytes(lastContext);
          out.writeDynamicBytes(lastRpc);
        });
  }

  static TestPubContractState readFrom(SafeDataInputStream stream) {
    return new TestPubContractState(
        stream.readInt(), stream.readInt(), stream.readDynamicBytes(), stream.readDynamicBytes());
  }
}
