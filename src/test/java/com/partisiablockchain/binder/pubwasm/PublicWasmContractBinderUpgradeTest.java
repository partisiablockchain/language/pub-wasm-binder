package com.partisiablockchain.binder.pubwasm;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.binder.ContractIdentifiers;
import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.contract.SerializationChecker;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.wasminvoker.WasmState;
import com.partisiablockchain.serialization.StateAccessor;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test upgrade logic of {@link PublicWasmContractBinder}. */
public final class PublicWasmContractBinderUpgradeTest {

  private PublicWasmContractBinder binder;
  private PubBinderContextStub pubBinderContext;
  private static final byte[] DEFAULT_EVENTS = WasmSerializedEventGroupsTest.EVENT_GROUP_AS_HEX;

  private static final ContractIdentifiers CONTRACT_IDENTIFIERS_OLD =
      new ContractIdentifiers(
          Hash.fromString("1111111111111111111111111111111111111111111111111111111111111111"),
          Hash.fromString("2222222222222222222222222222222222222222222222222222222222222222"),
          Hash.fromString("3333333333333333333333333333333333333333333333333333333333333333"));

  private static final ContractIdentifiers CONTRACT_IDENTIFIERS_NEW =
      new ContractIdentifiers(
          Hash.fromString("4444444444444444444444444444444444444444444444444444444444444444"),
          Hash.fromString("5555555555555555555555555555555555555555555555555555555555555555"),
          Hash.fromString("6666666666666666666666666666666666666666666666666666666666666666"));

  /** Setup mocks and contract. */
  @BeforeEach
  public void setUp() {
    TestWasmInvoker invoker = new TestWasmInvoker(null, DEFAULT_EVENTS.clone());
    binder = new PublicWasmContractBinder(invoker);
    pubBinderContext = new PubBinderContextStub();
  }

  private static final byte[] EXPECTED_PERMIT_BYTES =
      new byte[] {
        // WASM contract upgrade permit (Dynamic bytes)
        0,
        0,
        0,
        32,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        // Gas remaining for use in upgrade state
        7,
        39,
        10,
        24
      };

  private static final byte[] PERMIT_BYTES_NO_GAS =
      new byte[] {
        // WASM contract upgrade permit (Dynamic bytes)
        0,
        0,
        0,
        32,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        // Gas remaining for use in upgrade state
        0,
        0,
        0,
        0
      };

  private static final byte[] RPC = new byte[] {1, 3, 3, 7};

  /**
   * When an event tries to acquire the permit to upgrade a contract that implements the upgrade
   * logic, a valid permit is returned. Additionally, gas is consumed when calling the contract to
   * upgrade.
   */
  @Test
  public void acquireUpgradePermitForUpgradableContract() {
    WasmInvokerUpgradeStub invoker = new WasmInvokerUpgradeStub(true, true);
    binder = new PublicWasmContractBinder(invoker);

    ContractUpgradePermit permit =
        binder.acquireUpgradePermit(
            pubBinderContext,
            PublicWasmContractBinderTest.wasmState(0),
            CONTRACT_IDENTIFIERS_OLD,
            CONTRACT_IDENTIFIERS_NEW,
            RPC);

    assertThat(pubBinderContext.getGasUsed()).isEqualTo(TestWasmInvoker.DEFAULT_GAS_USED);

    assertThat(permit.oldContractData()).containsExactly(EXPECTED_PERMIT_BYTES);
  }

  /** Contract can be upgraded. */
  @Test
  public void upgradeStateHappyPath() {
    WasmInvokerUpgradeStub invoker = new WasmInvokerUpgradeStub(true, true);
    binder = new PublicWasmContractBinder(invoker);

    final WasmState updatedWasmState =
        binder.upgradeState(
            pubBinderContext,
            StateAccessor.create(PublicWasmContractBinderTest.wasmState(0)),
            new ContractUpgradePermit(EXPECTED_PERMIT_BYTES),
            RPC);

    assertThat(updatedWasmState).isNotNull();
    assertThat(pubBinderContext.getGasUsed()).isEqualTo(TestWasmInvoker.DEFAULT_GAS_USED);
  }

  /** Contract upgrade can fail due to too little gas. */
  @Test
  public void upgradeStateFailDueToTooLittleGas() {
    WasmInvokerUpgradeStub invoker = new WasmInvokerUpgradeStub(true, true);
    binder = new PublicWasmContractBinder(invoker);

    assertThatThrownBy(
            () ->
                binder.upgradeState(
                    pubBinderContext,
                    StateAccessor.create(PublicWasmContractBinderTest.wasmState(0)),
                    new ContractUpgradePermit(PERMIT_BYTES_NO_GAS),
                    RPC))
        .hasMessage("No gas for WASM!");
  }

  /**
   * When an event tries to acquire the permit to upgrade a contract that is not allowed to be
   * upgraded, an exception is thrown. Additionally, gas is consumed when calling the contract to
   * upgrade despite not acquiring permit.
   */
  @Test
  public void cannotAcquireUpgradePermitForNonUpgradableContract() {
    WasmInvokerUpgradeStub invoker = new WasmInvokerUpgradeStub(true, false);
    binder = new PublicWasmContractBinder(invoker);

    assertThatThrownBy(
            () ->
                binder.acquireUpgradePermit(
                    pubBinderContext,
                    PublicWasmContractBinderTest.wasmState(0),
                    CONTRACT_IDENTIFIERS_OLD,
                    CONTRACT_IDENTIFIERS_NEW,
                    RPC))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Contract did not allow this upgrade");

    assertThat(pubBinderContext.getGasUsed()).isEqualTo(TestWasmInvoker.DEFAULT_GAS_USED);
  }

  /**
   * When an event tries to acquire the permit to upgrade a contract that does not implement the
   * upgrade logic, an exception is thrown. Additionally, no gas is consumed, since no function to
   * upgrade is called.
   */
  @Test
  public void cannotAcquireUpgradePermitWhenUpgradeIsNotSupportedByContract() {
    WasmInvokerUpgradeStub invoker = new WasmInvokerUpgradeStub(false, false);
    binder = new PublicWasmContractBinder(invoker);

    assertThatThrownBy(
            () ->
                binder.acquireUpgradePermit(
                    pubBinderContext,
                    PublicWasmContractBinderTest.wasmState(0),
                    CONTRACT_IDENTIFIERS_OLD,
                    CONTRACT_IDENTIFIERS_NEW,
                    RPC))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage(
            "Contract does not implement \"upgrade_is_allowed\", and defaults to not upgradable");

    assertThat(pubBinderContext.getGasUsed()).isEqualTo(0L);
  }

  /**
   * An error is thrown when the upgraded-to contract doesn't implement the required hook for
   * updating the state.
   */
  @Test
  public void upgradeStateUnsupported() {
    WasmInvokerUpgradeStub invoker = new WasmInvokerUpgradeStub(false, false);
    binder = new PublicWasmContractBinder(invoker);

    assertThatThrownBy(
            () ->
                binder.upgradeState(
                    pubBinderContext,
                    StateAccessor.create(PublicWasmContractBinderTest.wasmState(0)),
                    new ContractUpgradePermit(new byte[32]),
                    RPC))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Contract does not implement \"upgrade\", and can thus not be upgraded to");

    assertThat(pubBinderContext.getGasUsed()).isEqualTo(0L);
  }

  private static final List<PubBinderUpgradePermit> PERMIT_EXAMPLES =
      List.of(
          new PubBinderUpgradePermit(new byte[0], 0),
          new PubBinderUpgradePermit(new byte[] {1, 2, 3, 4, 5}, 100010));

  /** Permits can be serialized and deserialized. */
  @Test
  public void serializePubBinderUpgradePermit() {
    SerializationChecker.assertSerializeDeserialize(PubBinderUpgradePermit::read, PERMIT_EXAMPLES);
  }

  /**
   * Contract upgrade is denied if the contract returns anything other than a single non-zero byte.
   */
  @Test
  public void didContractAllowThisUpgradeEdgeCases() {
    assertThat(AbstractPublicWasmContractBinder.didContractAllowThisUpgrade(null)).isFalse();
    assertThat(AbstractPublicWasmContractBinder.didContractAllowThisUpgrade(new byte[0])).isFalse();
    assertThat(AbstractPublicWasmContractBinder.didContractAllowThisUpgrade(new byte[2])).isFalse();
    assertThat(AbstractPublicWasmContractBinder.didContractAllowThisUpgrade(new byte[99]))
        .isFalse();
    assertThat(AbstractPublicWasmContractBinder.didContractAllowThisUpgrade(new byte[] {0}))
        .isFalse();
    assertThat(AbstractPublicWasmContractBinder.didContractAllowThisUpgrade(new byte[] {1}))
        .isTrue();
    assertThat(AbstractPublicWasmContractBinder.didContractAllowThisUpgrade(new byte[] {2}))
        .isTrue();
    assertThat(AbstractPublicWasmContractBinder.didContractAllowThisUpgrade(new byte[] {99}))
        .isTrue();
  }
}
