package com.partisiablockchain.binder.pubwasm;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.language.wasminvoker.SemVer;
import com.partisiablockchain.language.wasminvoker.WasmState;
import com.partisiablockchain.serialization.LargeByteArray;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test invocation logic of {@link PublicWasmContractBinder}. */
public final class PublicWasmContractBinderTest {

  private PublicWasmContractBinder binder;
  private PubBinderContextStub pubBinderContext;
  private static final byte[] DEFAULT_EVENTS = WasmSerializedEventGroupsTest.EVENT_GROUP_AS_HEX;

  /** Setup mocks and contract. */
  @BeforeEach
  public void setUp() {
    TestWasmInvoker invoker = new TestWasmInvoker(null, DEFAULT_EVENTS.clone());
    binder = new PublicWasmContractBinder(invoker);
    pubBinderContext = new PubBinderContextStub();
  }

  @Test
  public void getStateClass() {
    assertThat(binder.getStateClass()).isEqualTo(WasmState.class);
  }

  @Test
  public void create() {
    BinderResult<WasmState, BinderInteraction> result =
        binder.create(pubBinderContext, new byte[] {-1, -1, -1, -1, 0x0F, 33});
    verifyBinderEvents(result);
    WasmState rawState = result.getState();

    byte[] data = rawState.getState();
    TestPubContractState state =
        TestPubContractState.readFrom(SafeDataInputStream.createFromBytes(data));

    assertThat(state.createCount).isEqualTo(1);
    assertThat(state.invokeCount).isEqualTo(0);

    assertContext(state.lastContext());

    assertThat(state.lastRpc).hasSize(1);
    assertThat(state.lastRpc[0]).isEqualTo((byte) 33);

    assertThat(pubBinderContext.getGasUsed()).isEqualTo(TestWasmInvoker.DEFAULT_GAS_USED);
  }

  @Test
  public void invoke() {
    WasmState previousState = wasmState(1);
    TestWasmInvoker invoker =
        new TestWasmInvoker(
            new TestPubContractState(0, 1, new byte[0], new byte[0]), DEFAULT_EVENTS.clone());
    binder = new PublicWasmContractBinder(invoker);

    byte[] bytes =
        SafeDataOutputStream.serialize(
            rpc -> {
              rpc.write(Hex.decode("E58E26"));
              rpc.writeByte(34);
            });

    BinderResult<WasmState, BinderInteraction> result =
        binder.invoke(pubBinderContext, previousState, bytes);
    verifyBinderEvents(result);

    TestPubContractState actualState =
        TestPubContractState.readFrom(
            SafeDataInputStream.createFromBytes(result.getState().getState()));
    assertThat(actualState.createCount).isEqualTo(0);
    assertThat(actualState.invokeCount).isEqualTo(2);
    assertThat(actualState.lastRpc).hasSize(1);
    assertThat(actualState.lastRpc[0]).isEqualTo((byte) 34);
    assertThat(pubBinderContext.getGasUsed()).isEqualTo(TestWasmInvoker.DEFAULT_GAS_USED);
  }

  @Test
  public void invoke_empty() {
    WasmState previousState = wasmState(1);
    TestWasmInvoker invoker =
        new TestWasmInvoker(
            new TestPubContractState(0, 0, new byte[0], new byte[0]), DEFAULT_EVENTS.clone());
    binder = new PublicWasmContractBinder(invoker);

    byte[] bytes = new byte[0];

    BinderResult<WasmState, BinderInteraction> invocationResult =
        binder.invoke(pubBinderContext, previousState, bytes);
    assertThat(invocationResult.getInvocations()).isEmpty();

    WasmState wasmState = invocationResult.getState();
    assertThat(wasmState.getState()).isEqualTo(previousState.getState());
    assertThat(pubBinderContext.getGasUsed()).isEqualTo(0);
  }

  @Test
  public void callbackNonEmptyRpc() {
    TestWasmInvoker invoker =
        new TestWasmInvoker(
            new TestPubContractState(0, 1, new byte[0], new byte[0]), new byte[] {0, 0, 0, 0});
    binder = new PublicWasmContractBinder(invoker);

    var state = wasmState(1);

    var rpc = new byte[] {13, 1, 2, 3};
    BinderResult<WasmState, BinderInteraction> binderResult =
        binder.callback(pubBinderContext, state, createCallbackContext(), rpc);

    assertThat(binderResult.getInvocations()).isEmpty();

    assertThat(pubBinderContext.getGasUsed()).isEqualTo(TestWasmInvoker.DEFAULT_GAS_USED);
  }

  private CallbackContext createCallbackContext(boolean... results) {
    FixedList<CallbackContext.ExecutionResult> list = FixedList.create();
    for (boolean result : results) {
      list =
          list.addElement(
              CallbackContext.createResult(
                  PubBinderContextStub.ORIGINAL_TRANSACTION_HASH,
                  result,
                  SafeDataInputStream.createFromBytes(new byte[] {0, 1, 2, 3, 4, 5})));
    }
    return CallbackContext.create(list);
  }

  @Test
  public void initWithActualWasm() {
    PublicWasmContractBinder binder = new PublicWasmContractBinder(load("wasm-invoker-test.wasm"));
    WasmState result =
        binder.create(pubBinderContext, new byte[] {-1, -1, -1, -1, 0x0f}).getState();
    byte[] state = result.getState();
    assertThat(state).contains("Hello World WASM".getBytes(StandardCharsets.UTF_8));
  }

  @Test
  public void initWithoutHeaderBytes() {
    PublicWasmContractBinder binder = new PublicWasmContractBinder(load("wasm-invoker-test.wasm"));
    assertThatThrownBy(() -> binder.create(pubBinderContext, new byte[0]))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Contract initialization bytes should start with 0xFFFFFFFF0F");
  }

  @Test
  public void initWithWrongHeaderBytes() {
    PublicWasmContractBinder binder = new PublicWasmContractBinder(load("wasm-invoker-test.wasm"));
    assertThatThrownBy(() -> binder.create(pubBinderContext, new byte[] {0, 0, 0, 0, 1}))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Contract initialization bytes should start with 0xFFFFFFFF0F");
  }

  @Test
  public void contractWithBadVersion() {
    assertThatThrownBy(
            () -> new PublicWasmContractBinder(load("wasm-binder-version-255.100.50.wasm")))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Unsupported binder ABI version 255.100.50 in WASM. Supported version is 10.4.0");
  }

  @Test
  public void isBinderSupported() {
    assertThat(PublicWasmContractBinder.isSupported(new SemVer(10, 4, 0))).isTrue();
    assertThat(PublicWasmContractBinder.isSupported(new SemVer(10, 4, 3))).isTrue();
    assertThat(PublicWasmContractBinder.isSupported(new SemVer(9, 1, 0))).isFalse();
    assertThat(PublicWasmContractBinder.isSupported(new SemVer(10, 0, 0))).isFalse();
  }

  @SuppressWarnings("unchecked")
  private void verifyBinderEvents(BinderResult<WasmState, BinderInteraction> result) {
    assertThat(result.getInvocations()).hasSize(0);

    assertThat(result.getCallResult()).isInstanceOf(BinderEventGroup.class);
    BinderEventGroup<BinderInteraction> eventGroup =
        (BinderEventGroup<BinderInteraction>) result.getCallResult();

    long expectedGasToDistribute =
        PubBinderContextStub.AVAILABLE_GAS - TestWasmInvoker.DEFAULT_GAS_USED;
    long expectedAllocatedCost = expectedGasToDistribute / 2;

    assertThat(eventGroup.getCallbackRpc()).isEqualTo(Hex.decode("AFBFCFDF"));
    assertThat(eventGroup.getCallbackCost()).isEqualTo(expectedAllocatedCost);
    assertThat(eventGroup.isCallbackCostFromContract()).isFalse();

    List<?> invocations = eventGroup.getEvents();
    assertThat(invocations).hasSize(1);
    BinderInteraction invocation = (BinderInteraction) invocations.get(0);
    assertThat(invocation.contract.writeAsString())
        .isEqualTo(WasmSerializedEventGroupsTest.ADDRESS);
    assertThat(invocation.rpc).isEqualTo(Hex.decode("4242CAFE"));
    assertThat(invocation.effectiveCost).isEqualTo(expectedAllocatedCost);
    assertThat(invocation.costFromContract).isFalse();
  }

  private void assertContext(TestPubWasmContext context) {
    assertThat(context.blockProductionTime).isEqualTo(PubBinderContextStub.PRODUCTION_TIME);
    assertThat(context.blockTime).isEqualTo(PubBinderContextStub.BLOCK_TIME);
    assertThat(context.contractAddress).isEqualTo(PubBinderContextStub.CONTRACT_ADDRESS);
    assertThat(context.from).isEqualTo(PubBinderContextStub.TRANSACTION_SENDER);
    assertThat(context.currentTransactionHash)
        .isEqualTo(PubBinderContextStub.CURRENT_TRANSACTION_HASH);
    assertThat(context.originalTransactionHash)
        .isEqualTo(PubBinderContextStub.ORIGINAL_TRANSACTION_HASH);
  }

  static byte[] load(final String name) {
    final InputStream stream =
        requireNonNull(PublicWasmContractBinderTest.class.getResourceAsStream(name));
    try {
      return stream.readAllBytes();
    } catch (final java.io.IOException e) {
      throw new RuntimeException(e);
    }
  }

  static WasmState wasmState(int invocationCount) {
    final TestPubContractState state =
        new TestPubContractState(0, invocationCount, new byte[0], new byte[0]);
    return new WasmState(
        new LargeByteArray(state.asByteArray()), WasmInvokerUpgradeStub.exampleAvlTrees());
  }
}
