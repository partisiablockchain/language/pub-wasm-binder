package com.partisiablockchain.binder.pubwasm;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.bouncycastle.util.encoders.Hex;

final class WasmSerializedEventGroupsTest {

  static final String ADDRESS = "000101010101010101010101010101010101010101";
  static final String EVENT_GROUP_AS_HEX_RAW_TEXT =
      ""
          + "00000001" // One group
          + "01" // Non-null callback
          + "00000004" // Four bytes callback
          + "AFBFCFDF" // Callback contents
          + "00" // Null callback cost
          + "00000001" // One interaction
          + interactionAsString()
          + "00"; // Null return data

  static final byte[] EVENT_GROUP_AS_HEX = Hex.decode(EVENT_GROUP_AS_HEX_RAW_TEXT);

  private static String interactionAsString() {
    String base =
        ADDRESS
            + "00000004" // RPC length as BE
            + "4242CAFE" // RPC
            + "00"; // Send from contract
    return base + "00"; // Null cost
  }
}
