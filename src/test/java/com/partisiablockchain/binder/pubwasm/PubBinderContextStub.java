package com.partisiablockchain.binder.pubwasm;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.pub.PubBinderContext;
import com.partisiablockchain.crypto.Hash;

/** Stub of {@link PubBinderContext} for testing {@link PublicWasmContractBinder}. */
public final class PubBinderContextStub implements PubBinderContext {

  static final BlockchainAddress TRANSACTION_SENDER =
      BlockchainAddress.fromString("000070000000000000000000000000000000000001");
  static final BlockchainAddress CONTRACT_ADDRESS =
      BlockchainAddress.fromString("010000000000000000000000000000000000000001");

  static final long PRODUCTION_TIME = 11L;
  static final long BLOCK_TIME = 77L;
  static final Hash CURRENT_TRANSACTION_HASH = Hash.create(stream -> stream.writeInt(42));
  static final Hash ORIGINAL_TRANSACTION_HASH = Hash.create(stream -> stream.writeInt(17));
  static final long AVAILABLE_GAS = 120_000_000L;

  private long gasUsed = 0;

  public long getGasUsed() {
    return gasUsed;
  }

  @Override
  public long availableGas() {
    return AVAILABLE_GAS;
  }

  @Override
  public void registerCpuFee(long gas) {
    gasUsed += gas;
  }

  @Override
  public BlockchainAddress getContractAddress() {
    return CONTRACT_ADDRESS;
  }

  @Override
  public long getBlockTime() {
    return BLOCK_TIME;
  }

  @Override
  public long getBlockProductionTime() {
    return PRODUCTION_TIME;
  }

  @Override
  public BlockchainAddress getFrom() {
    return TRANSACTION_SENDER;
  }

  @Override
  public Hash getCurrentTransactionHash() {
    return CURRENT_TRANSACTION_HASH;
  }

  @Override
  public Hash getOriginalTransactionHash() {
    return ORIGINAL_TRANSACTION_HASH;
  }
}
