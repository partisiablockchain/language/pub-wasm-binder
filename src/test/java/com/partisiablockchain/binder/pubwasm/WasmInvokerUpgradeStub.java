package com.partisiablockchain.binder.pubwasm;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.SharedContext;
import com.partisiablockchain.language.wasminvoker.WasmInvoker;
import com.partisiablockchain.language.wasminvoker.WasmState;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTreeWrapper;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTrees;
import com.partisiablockchain.language.wasminvoker.avltrees.ComparableByteArray;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;

/** {@link WasmInvoker} stub for testing contract upgrade. */
@SuppressWarnings("unused")
public final class WasmInvokerUpgradeStub implements WasmInvoker {

  private final boolean doesFunctionExistResponse;
  private final boolean isUpgradeAllowed;

  /**
   * Constructor for {@link WasmInvokerUpgradeStub}.
   *
   * @param doesFunctionExistResponse Response to all {@link doesFunctionExist} calls.
   * @param isUpgradeAllowed Status response to queries for {@code upgrade_is_allowed}.
   */
  public WasmInvokerUpgradeStub(boolean doesFunctionExistResponse, boolean isUpgradeAllowed) {
    this.doesFunctionExistResponse = doesFunctionExistResponse;
    this.isUpgradeAllowed = isUpgradeAllowed;
  }

  @Override
  public boolean doesFunctionExist(String functionName) {
    return doesFunctionExistResponse;
  }

  @Override
  public WasmResult executeWasm(
      long availableWasmGas,
      String functionName,
      SharedContext context,
      CallbackContext callbackContext,
      AvlTrees avlTrees,
      byte[]... params) {
    if (availableWasmGas <= 0) {
      throw new RuntimeException("No gas for WASM!");
    }

    if ("upgrade_is_allowed".equals(functionName)) {
      return executeWasmUpgradeIsAllowed(
          availableWasmGas, functionName, context, callbackContext, avlTrees, params);
    } else {
      return executeWasmUpgradeState(
          availableWasmGas, functionName, context, callbackContext, avlTrees, params);
    }
  }

  private WasmResult executeWasmUpgradeIsAllowed(
      long availableWasmGas,
      String functionName,
      SharedContext context,
      CallbackContext callbackContext,
      AvlTrees avlTrees,
      byte[]... params) {

    Assertions.assertThat(functionName).as("function name").isEqualTo("upgrade_is_allowed");

    Assertions.assertThat(params[0]).as("state").hasSize(16);
    Assertions.assertThat(Hex.toHexString(params[1]))
        .as("current contract hash")
        .isEqualTo("1111111111111111111111111111111111111111111111111111111111111111");
    Assertions.assertThat(Hex.toHexString(params[2]))
        .as("current binder hash")
        .isEqualTo("2222222222222222222222222222222222222222222222222222222222222222");
    Assertions.assertThat(Hex.toHexString(params[3]))
        .as("current abi hash")
        .isEqualTo("3333333333333333333333333333333333333333333333333333333333333333");
    Assertions.assertThat(Hex.toHexString(params[4]))
        .as("upgraded contract hash")
        .isEqualTo("4444444444444444444444444444444444444444444444444444444444444444");
    Assertions.assertThat(Hex.toHexString(params[5]))
        .as("upgraded binder hash")
        .isEqualTo("5555555555555555555555555555555555555555555555555555555555555555");
    Assertions.assertThat(Hex.toHexString(params[6]))
        .as("upgraded abi hash")
        .isEqualTo("6666666666666666666666666666666666666666666666666666666666666666");
    Assertions.assertThat(Hex.toHexString(params[7])).as("rpc").isEqualTo("0000000401030307");

    final TestPubContractState contractState =
        new TestPubContractState(0, 1, serializePubContext(context), params[7]);

    final WasmResultSection sectionUpgradeAllowed =
        new WasmResultSection(
            AbstractPublicWasmContractBinder.RETURN_SECTION_UPGRADE_ALLOWED,
            SafeDataOutputStream.serialize(s -> s.writeBoolean(isUpgradeAllowed)));

    final WasmResultSection sectionStateTypeWitness =
        new WasmResultSection(
            AbstractPublicWasmContractBinder.RETURN_SECTION_STATE_TYPE_WITNESS, new byte[32]);

    return new WasmResult(
        updateContractState(context, params[params.length - 1]),
        null,
        1_000L,
        List.of(sectionUpgradeAllowed, sectionStateTypeWitness));
  }

  private WasmResult executeWasmUpgradeState(
      long availableWasmGas,
      String functionName,
      SharedContext context,
      CallbackContext callbackContext,
      AvlTrees avlTrees,
      byte[]... params) {

    // Check expected parameters
    Assertions.assertThat(params.length).isEqualTo(3);
    Assertions.assertThat(params[0]).hasSize(32); // State type witness
    Assertions.assertThat(params[1]).hasSize(16); // State
    Assertions.assertThat(params[2]).hasSize(4); // RPC
    Assertions.assertThat(avlTrees.avlTrees().getValue(1).size()).isEqualTo(1);

    return new WasmResult(
        updateContractState(context, params[params.length - 1]), null, 1_000L, List.of());
  }

  private static WasmState updateContractState(SharedContext context, byte[] rpc) {
    final TestPubContractState state =
        new TestPubContractState(0, 1, serializePubContext(context), rpc);

    return new WasmState(new LargeByteArray(state.asByteArray()), exampleAvlTrees());
  }

  static AvlTree<Integer, AvlTreeWrapper> exampleAvlTrees() {
    final var exampleTree =
        AvlTree.<ComparableByteArray, LargeByteArray>create()
            .set(new ComparableByteArray(new byte[32]), new LargeByteArray(new byte[32]));
    return AvlTree.<Integer, AvlTreeWrapper>create().set(1, new AvlTreeWrapper(exampleTree));
  }

  private static byte[] serializePubContext(SharedContext context) {
    return SafeDataOutputStream.serialize(
        out -> {
          context.getContractAddress().write(out);
          context.getFrom().write(out);
          out.writeLong(context.getBlockTime());
          out.writeLong(context.getBlockProductionTime());
          context.getCurrentTransactionHash().write(out);
          context.getOriginalTransactionHash().write(out);
        });
  }
}
