package com.partisiablockchain.binder.pubwasm;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.SharedContext;
import com.partisiablockchain.language.wasminvoker.Configuration;
import com.partisiablockchain.language.wasminvoker.WasmInvoker;
import com.partisiablockchain.language.wasminvoker.WasmState;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTrees;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import com.partisiablockchain.language.wasminvoker.events.SerializedEventResult;
import com.partisiablockchain.serialization.LargeByteArray;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;

/** {@link WasmInvoker} testing object for testing general binder behaviour. */
public final class TestWasmInvoker implements WasmInvoker {
  static final long DEFAULT_GAS_USED = 1_000L;

  /** Latest tracked state of the contract. */
  TestPubContractState lastState;

  private final EventResult eventResult;

  /**
   * Constructor for {@link TestWasmInvoker}.
   *
   * @param initialState The initial state of the contract.
   * @param serializedEventGroups Serialized {@link EventResult} included in {@link executeWasm}
   *     responses.
   */
  public TestWasmInvoker(TestPubContractState initialState, byte[] serializedEventGroups) {
    this.lastState = initialState;
    this.eventResult =
        SerializedEventResult.readEventResultFromStream(
            new Configuration(true, false),
            SafeDataInputStream.createFromBytes(serializedEventGroups));
  }

  @Override
  public boolean doesFunctionExist(final String functionName) {
    return false;
  }

  @Override
  public WasmResult executeWasm(
      long remainingGas,
      String functionName,
      SharedContext context,
      CallbackContext callbackContext,
      AvlTrees avlTrees,
      byte[]... params) {
    if ("init".equals(functionName)) {
      lastState = new TestPubContractState(1, 0, serializePubContext(context), params[0]);
    } else if ("action_e58e26".equals(functionName) || "action_02".equals(functionName)) {
      lastState =
          new TestPubContractState(
              lastState.createCount,
              lastState.invokeCount + 1,
              serializePubContext(context),
              params[1]);
    }

    return new WasmResult(
        new WasmState(new LargeByteArray(lastState.asByteArray())),
        eventResult,
        DEFAULT_GAS_USED,
        List.of());
  }

  private static byte[] serializePubContext(SharedContext context) {
    return SafeDataOutputStream.serialize(
        out -> {
          context.getContractAddress().write(out);
          context.getFrom().write(out);
          out.writeLong(context.getBlockTime());
          out.writeLong(context.getBlockProductionTime());
          context.getCurrentTransactionHash().write(out);
          context.getOriginalTransactionHash().write(out);
        });
  }
}
